<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ProductTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i=0; $i < 3000; $i++) {
            App\Model\Product\Product::create([
                'secure_id' => str_random(8),
                'title' => $faker->sentence($nbWords = 6, $variableNbWords = true) ,
                'description' => $faker->randomHtml(),
                'img_small' => '/common/images/placeholder/placeholder.png',
                'img_hd' => '/common/images/placeholder/placeholder.png',
                'primary_group' => 'p',
                'primary_group_id' => '1',
                'secondary_group' => 's',
                'secondary_group_id' => '1',
                'featured' => $faker->numberBetween($min = 0, $max = 1),
                'active' => $faker->numberBetween($min = 0, $max = 1),
            ]);
        }
    }
}
