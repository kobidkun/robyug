<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('txt_id');
            $table->text('secure_id');
            $table->text('taxable_value')->nullable();
            $table->text('tax_amount');
            $table->text('cgst_amt')->nullable();
            $table->text('sgst_amt')->nullable();
            $table->text('igst_amt')->nullable();
            $table->text('total')->nullable();
            $table->text('customer_id');
            $table->text('billing_address');
            $table->text('delivery_address');
            $table->text('shipping_type');
            $table->text('payment_type');
            $table->text('user_ip');
            $table->text('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
