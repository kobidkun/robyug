<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_to_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('product_id');
            $table->text('primary_group_id')->nullable();
            $table->text('primary_group')->nullable();
            $table->text('secondary_group_id')->nullable();
            $table->text('secondary_group')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_to_groups');
    }
}
