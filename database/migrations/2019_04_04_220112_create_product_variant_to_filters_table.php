<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariantToFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variant_to_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('product_variant_id');
            $table->text('product_id');
            $table->text('filter_type_id');
            $table->text('filter_name');
            $table->text('filter_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variant_to_filters');
    }
}
