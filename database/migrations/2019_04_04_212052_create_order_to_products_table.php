<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_to_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('order_id');
            $table->text('secure_id');
            $table->text('product_id');
            $table->text('product_variant_id');
            $table->text('variant_type');
            $table->text('variant_value');
            $table->text('title');
            $table->text('slug')->nullable();
            $table->text('hsn')->nullable();
            $table->text('code_name')->nullable();
            $table->text('type')->nullable();
            $table->text('unit')->nullable();
            $table->text('value')->nullable();
            $table->text('discount')->nullable();
            $table->text('discount_per')->nullable();
            $table->text('cgst')->nullable();
            $table->text('sgst')->nullable();
            $table->text('igst')->nullable();
            $table->text('cgst_per')->nullable();
            $table->text('sgst_per')->nullable();
            $table->text('igst_per')->nullable();
            $table->text('taxper')->nullable();
            $table->text('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_to_products');
    }
}
