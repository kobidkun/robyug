<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('product_id');
            $table->text('variant_type');
            $table->text('variant_value');
            $table->text('secure_id');
            $table->text('title');
            $table->longText('description');
            $table->text('img')->nullable();
            $table->text('img_small')->nullable();
            $table->text('img_hd')->nullable();
            $table->text('meta')->nullable();
            $table->text('slug')->nullable();
            $table->text('hsn')->nullable();
            $table->text('primary_group');
            $table->text('primary_group_id');
            $table->text('secondary_group');
            $table->text('secondary_group_id');
            $table->text('code_name')->nullable();
            $table->text('type')->nullable();
            $table->text('unit')->nullable();
            $table->text('value')->nullable();
            $table->text('discount')->nullable();
            $table->text('discount_per')->nullable();
            $table->text('after_discount_amt')->nullable();
            $table->text('cgst')->nullable();
            $table->text('sgst')->nullable();
            $table->text('igst')->nullable();
            $table->text('cgst_per')->nullable();
            $table->text('sgst_per')->nullable();
            $table->text('igst_per')->nullable();
            $table->text('taxper')->nullable();
            $table->text('tax_amt')->nullable();
            $table->text('total')->nullable();
            $table->string('featured')->default('0');
            $table->boolean('active')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
