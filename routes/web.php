<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/', 'Front\ManagePage@HomePagetwo')->name('components.welcome');
Route::get('/category/primary/{category}', 'Front\ManagePage@ProductsbyPrimaryCategory')->name('front.category.primary');
Route::get('contact', 'Front\ManagePage@ContactPage')->name('components.contact');
Route::get('/product-details/{slug}', 'Front\ManagePage@ProductDetails')->name('front.productdetails');
Route::get('shop', 'Front\ManagePage@ShopPage')->name('components.shop');
Route::get('about-us', 'Front\ManagePage@AboutPage')->name('components.about-us');
Route::get('blog', 'Front\ManagePage@HomePage')->name('components.blog');
Route::get('login', 'Front\ManagePage@HomePage')->name('components.login');
Route::get('cart', 'Front\ManagePage@HomePage')->name('components.cart');
Route::get('checkout', 'Front\ManagePage@HomePage')->name('components.checkout');
Route::get('orderform', 'Front\ManagePage@HomePage')->name('components.orderform');
Route::get('profile', 'Front\ManagePage@HomePage')->name('components.profile');
Route::get('wishlist', 'Front\ManagePage@HomePage')->name('components.wishlist');
Route::get('forgetpassword', 'Front\ManagePage@HomePage')->name('components.forgetpassword');
Route::get('confirmation', 'Front\ManagePage@HomePage')->name('components.confirmation');
Route::get('logout', 'Front\ManagePage@HomePage')->name('components.logout');
Route::get('orderfailure', 'Front\ManagePage@HomePage')->name('components.orderfailure');
Route::get('ordertracking', 'Front\ManagePage@HomePage')->name('components.ordertracking');
Route::get('myorder', 'Front\ManagePage@HomePage')->name('components.myorder');
Route::get('mail-message', 'Front\ManagePage@HomePage')->name('components.mail-message');




/////// admin

Route::get('admin/dashboard', 'Admin\DashboardController@Index')->name('admin.dashboard');


//group

Route::get('admin/group/primary/create', 'Admin\Master\Group\ManageGroup@PrimaryGroupView')->name('admin.group.primary.create.view');
Route::post('admin/group/primary/create', 'Admin\Master\Group\ManageGroup@PrimaryGroupSave')->name('admin.group.primary.create.save');
Route::get('admin/group/primary/delete/{id}', 'Admin\Master\Group\ManageGroup@PrimaryGroupDelete')->name('admin.group.primary.create.delete');
Route::get('admin/group/primary/edit/{id}', 'Admin\Master\Group\ManageGroup@EditPrimary')
    ->name('admin.group.primary.edit');
Route::post('admin/group/primary/update', 'Admin\Master\Group\ManageGroup@UpdatePrimary')
    ->name('admin.group.primary.update');
Route::get('admin/group/primary', 'Admin\Master\Group\ManageGroup@PrimaryGroupAll')->name('admin.group.primary.all');
Route::get('admin/group/secondary/get/{id}', 'Admin\Master\Group\ManageGroup@GetSecondaryGroup')
    ->name('admin.group.secondary.get');


Route::get('admin/group/secondary/create', 'Admin\Master\Group\ManageGroup@SecondaryGroupView')->name('admin.group.secondary.create.view');
Route::post('admin/group/secondary/create', 'Admin\Master\Group\ManageGroup@SecondaryGroupSave')->name('admin.group.secondary.create.save');
Route::get('admin/group/secondary/delete/{id}', 'Admin\Master\Group\ManageGroup@SecondaryGroupDelete')->name('admin.group.secondary.create.delete');
Route::get('admin/group/secondary', 'Admin\Master\Group\ManageGroup@SecondaryGroupAll')->name('admin.group.secondary.all');
Route::get('admin/group/secondary/edit/{id}', 'Admin\Master\Group\ManageGroup@EditSecondary')
    ->name('admin.group.secondary.edit');
Route::post('admin/group/secondary/update', 'Admin\Master\Group\ManageGroup@UpdateSecondary')
    ->name('admin.group.secondary.update');

//// filter
///
///

Route::get('admin/filter/create', 'Admin\Master\Filter\ManageFilter@FilterView')->name('admin.filter.create.view');
Route::post('admin/filter/create', 'Admin\Master\Filter\ManageFilter@FilterSave')->name('admin.filter.create.save');
Route::get('admin/filter/delete/{id}', 'Admin\Master\Filter\ManageFilter@FilterDelete')->name('admin.filter.create.delete');
Route::get('admin/filter', 'Admin\Master\Filter\ManageFilter@FilterAll')->name('admin.filter.all');
Route::get('admin/filter/addvalue/{id}', 'Admin\Master\Filter\ManageFilter@AddValues')
    ->name('admin.filter.values.add');
Route::post('admin/filter/addvalue/save', 'Admin\Master\Filter\ManageFilter@SaveFilterValues')
    ->name('admin.filter.types.values.save');



// product


Route::get('admin/product/create', 'Admin\Product\ManageProduct@CreateProductView')
    ->name('admin.product.create.view');

Route::get('admin/product', 'Admin\Product\ManageProduct@AllProducts')
    ->name('admin.product');

Route::get('admin/product/details/{id}', 'Admin\Product\ManageProduct@ProductDetails')
    ->name('admin.product.details');


Route::get('admin/product/update/image/{id}', 'Admin\Product\ManageProduct@updateImage')
    ->name('admin.product.update.image');

Route::get('admin/product/details/edit/{id}', 'Admin\Product\ManageProduct@Productedit')
    ->name('admin.product.edit.details');



Route::get('admin/product/details/filter/{id}', 'Admin\Product\ManageProduct@Productedit')
    ->name('admin.product.edit.details');

Route::post('admin/product/create', 'Admin\Product\ManageProduct@SaveProduct')
    ->name('admin.product.create.save');

Route::get('admin/product/add/filter/{id}', 'Admin\Product\ManageProduct@AddFilter')
    ->name('admin.product.add.filter');

Route::post('admin/product/add/filter/two', 'Admin\Product\ManageProduct@AddFiltersec')
    ->name('admin.product.add.filter.two');


Route::post('admin/product/add/filter/two/save', 'Admin\Product\ManageProduct@AddFilterSave')
    ->name('admin.product.add.filter.save');

Route::get('admin/product/gallery/{id}', 'Admin\Product\ManageProduct@ProductGallery')
    ->name('admin.product.create.gallery');

Route::get('admin/product/gallery/delete/{id}', 'Admin\Product\ManageProduct@DeleteGallery')
    ->name('admin.product.delete.gallery');

Route::post('admin/product/gallery/upload', 'Admin\Product\ManageProduct@UploadGallery')
    ->name('admin.product.create.gallery.upload');

Route::get('admin/product/api', 'Admin\Product\ManageProduct@DatatableApi')
    ->name('admin.product.datatable.api');

Route::post('admin/product/details/update/{id}', 'Admin\Product\ManageProduct@SaveProductupdateedit')
    ->name('admin.product.edit.update.save');


Route::get('admin/product/extra/feature/activate/{id}', 'Admin\Product\ManageProduct@CreateFeatureProduct')
    ->name('admin.product.feature.activate');

Route::get('admin/product/extra/feature/deactivate/{id}', 'Admin\Product\ManageProduct@CreateFeaturenonProduct')
    ->name('admin.product.feature.deactivate');

// group

Route::get('admin/product/add/group/{id}', 'Admin\Product\ManageProduct@AddGroup')
    ->name('admin.product.add.group');

Route::post('admin/product/add/group/two', 'Admin\Product\ManageProduct@AddGroupsec')
    ->name('admin.product.add.group.two');

Route::post('admin/product/add/group/two/save', 'Admin\Product\ManageProduct@AddGroupSave')
    ->name('admin.product.add.group.save');

Route::get('admin/product/delete/group/primary/{id}', 'Admin\Product\ManageProduct@DeletePrimary')
    ->name('admin.product.delete.group.primary');


Route::get('admin/product/delete/group/secondary/{id}', 'Admin\Product\ManageProduct@DeleteSecondaty')
    ->name('admin.product.delete.group.secondary');


Route::post('admin/product/update/hd/image', 'Admin\Product\ManageProduct@UpdatePrimaryhdimg')
    ->name('admin.product.update.hd.image');

Route::post('admin/product/update/small/image', 'Admin\Product\ManageProduct@UpdatePrimarysmimg')
    ->name('admin.product.update.small.image');



// add multiple group


Route::post('admin/product/add/primary/group', 'Admin\Product\ManageProduct@AddPrimaryGroup')
    ->name('admin.product.add.primary.group.post');

Route::get('admin/product/delete/primary/group/{id}', 'Admin\Product\ManageProduct@DeletePrimaryGroup')
    ->name('admin.product.delete.group.primary');


Route::get('admin/product/add/secondary/group/{id}', 'Admin\Product\ManageProduct@AddGroupSecondary')
    ->name('admin.product.add.group.secondary');



Route::post('admin/product/add/secondary/group', 'Admin\Product\ManageProduct@AddSecondaryGroup')
    ->name('admin.product.add.secondary.group.post');

Route::get('admin/product/delete/secondary/group/{id}', 'Admin\Product\ManageProduct@DeleteSecondaryGroup')
    ->name('admin.product.delete.group.secondary');

//// variant


Route::get('admin/product/variant/create/{id}', 'Admin\Product\ProductVariant\ManageProductVariant@CreateProductView')
    ->name('admin.product.variant.create.view');

Route::get('admin/product/variant', 'Admin\Product\ProductVariant\ManageProductVariant@AllProducts')
    ->name('admin.product.variant');

Route::get('admin/product/variant/details/{id}', 'Admin\Product\ProductVariant\ManageProductVariant@ProductDetails')
    ->name('admin.product.variant.details');

Route::post('admin/product/variant/create', 'Admin\Product\ProductVariant\ManageProductVariant@SaveProduct')
    ->name('admin.product.variant.create.save');

Route::get('admin/product/variant/api', 'Admin\Product\ProductVariant\ManageProductVariant@DatatableApi')
    ->name('admin.product.variant.datatable.api');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
