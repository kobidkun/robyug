@extends('base')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('{{asset('/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')}}')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            @if ($customer->customer_to_profile_pictures !== null )

                                                <img src="{{asset('/storage/'.$customer->customer_to_profile_pictures->path)}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @else


                                                <img src="{{asset('dist/img/avatar12.jpg')}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @endif
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$customer->fname}} {{$customer->lname}}</div>
                                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Phone</span><span class="mr-5">{{$customer->phone}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Mobile</span><span class="mr-5">{{$customer->mobile}}</span></span><span> <br>

                                                <span class="font-weight-500 pr-5">Email</span> <span>{{$customer->email}}</span>
                                        <br> <span class="font-weight-500 pr-5">Hand</span> <span>{{$customer->hand}}</span></span></div>
                                        <br>

                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="bg-white shadow-bottom">
                    <div class="container">
                        <ul class="nav nav-light nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="{{route('admin.customer.details',$customer->id)}}" class="d-flex h-60p align-items-center nav-link ">Tree View</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.profile',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.password',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Password</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.files',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Files</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                                <div class="row">


                                    <div class="col-xl-12">
                                        <section class="hk-sec-wrapper">


                                            <!--begin: Search Form -->


                                            <div class="row">

                                                <div class="col-12" style="text-align: center">

                                                    <h5>Primary Member</h5>

                                                    <button class="btn btn-primary primary-member">

                                                    </button>



                                                </div>





                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer" style="text-align: center">
                                                    <br>
                                                    <br>

                                                    <div class="row">
                                                        <div class="col-md-6 mb-10">
                                                            <div class="" id="downlineleft">

                                                            </div>
                                                        </div>


                                                        <div class="col-md-6 mb-10">



                                                            <div class="" id="downlineright">

                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer2" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup2"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline2">



                                                    </div>
                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer3" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup3"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline3">

                                                    </div>
                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer4" style="text-align: center">
                                                    <br>
                                                    <div id="memup4"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline4">

                                                    </div>
                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer5" style="text-align: center">
                                                    <br>
                                                    <div id="memup5"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline5">

                                                    </div>
                                                </div>
                                                {{--downlines--}}


                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer6" style="text-align: center">
                                                    <br>
                                                    <div id="memup6"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline6">

                                                    </div>
                                                </div>
                                                {{--downlines--}}


                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer7" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup7"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline7">

                                                    </div>
                                                </div>
                                                {{--downlines--}}


                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer8" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup8"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline8">

                                                    </div>
                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer9" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup9"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline9">

                                                    </div>
                                                </div>
                                                {{--downlines--}}



                                                {{--downlines--}}

                                                <div  class="col-12" id="downlinecontainer10" style="text-align: center">
                                                    <br>
                                                    <div style="font-weight: 500;font-size: 24px" id="memup10"></div>
                                                    <br>
                                                    <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline10">

                                                    </div>
                                                </div>
                                                {{--downlines--}}








                                            </div>



                                        </section>
                                    </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@endsection


@section('footer')


    <script>






        var downline = [];

        $.ajax({
            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
            url:"{{route('customer.tree.view.id',$customer->id)}}",
            //  async: true,
            dataType: 'json',

            success: function(obj){
                // var json = $.parseJSON(obj);

                $(".primary-member").replaceWith('<div style="text-align: center"><i style="font-size: 128px; color: #e12500!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br> ' +
                    '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> </div>')







                $("#downlineleft").append(
                    '<div class="col-md-6 mb-10 fdownline">' +
                    '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                    '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                    ' <br> IC No:  '+obj.left.ic_number+
                    ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                    '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+



                    '</div>'


                );

                $("#downlineright").append(
                    '<div class="col-md-6 mb-10 fdownline">' +
                    '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                    '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                    ' <br> IC No:  '+obj.right.ic_number+
                    ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                    '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+



                    '</div> '


                )


            },
            error: function(error){
                alert(error);
            }
        })



        //   href="/dashboard/admin/tree-view/view/'+v.id+'"

        $(document).on("click", ".dwnlinemembers" , function(){

            $("#downline2").html('');
            $("#memup2").html('');


            $("#downline3").html('');
            $("#memup3").html('');

            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');



            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);

                    $("#memup2").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number

                    )


                    if(obj.left === null){

                        $("#downline2").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline2").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline2").append('No Downline')
                    }



                    else{
                        $("#downline2").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }




                },
                error: function(error){
                    alert(error);
                }
            })




        });

        // next

        $(document).on("click", ".dwnlinemembers3" , function(){



            $("#downline3").html('');
            $("#memup3").html('');

            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup3").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers4">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline3").append('No Downline')
                    }



                    else{
                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                },
                error: function(error){
                    alert(error);
                }
            })




        });



        $(document).on("click", ".dwnlinemembers4" , function(){



            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);






            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup4").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                    //  $.each(obj.downline,function(k,v){


                    if(obj.left === null){

                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers5">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline4").append('No Downline')
                    }



                    else{
                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                    //   });
                },
                error: function(error){
                    alert(error);
                }
            })




        });

        // five

        $(document).on("click", ".dwnlinemembers5" , function(){


            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup5").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                    //  $.each(obj.downline,function(k,v){



                    if(obj.left === null){

                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline5").append('No Downline')
                    }



                    else{
                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    //  console.log(v.fname);
                    //  });
                },
                error: function(error){
                    alert(error);
                }
            })




        });


        $(document).on("click", ".dwnlinemembers6" , function(){

            $("#downline6").html('');
            $("#memup6").html('');


            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup6").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                    //  $.each(obj.downline,function(k,v){



                    if(obj.left === null){

                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline6").append('No Downline')
                    }



                    else{
                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    //  console.log(v.fname);
                    //  });
                },
                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers7" , function(){



            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup7").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)






                    if(obj.left === null){

                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline7").append('No Downline')
                    }



                    else{
                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                },
                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers8" , function(){




            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj) {
                    // var json = $.parseJSON(obj);
                    $("#memup8").html(obj.customer.fname + ' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)


                    if(obj.left === null){

                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline8").append('No Downline')
                    }



                    else{
                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }

                },



                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers9" , function(){



            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');

            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup9").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline9").append('No Downline')
                    }



                    else{
                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                },
                error: function(error){
                    alert(error);
                }
            })




        });


        $(document).on("click", ".dwnlinemembers10" , function(){



            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
                url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup10").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline10").append('No Downline')
                    }



                    else{
                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                },
                error: function(error){
                    alert(error);
                }
            })




        });







    </script>





    @endsection