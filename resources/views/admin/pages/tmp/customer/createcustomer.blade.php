@extends('base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Customer</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Create Customer</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Create Customer</h5>
                   <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" method="post"  action="{{route('admin.customer.create.save')}}">


@csrf
                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">First name</label>
                                        <input type="text" class="form-control"
                                               placeholder="First name"  name="fname" required>

                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom02">Last name</label>
                                        <input type="text" class="form-control"
                                               placeholder="Last Name"
                                               name="lname"

                                               required>


                                    </div>



                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Email</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Email"
                                                name="email"
                                                autocomplete="off"
                                                required
                                        >



                                    </div>




                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">mobile</label>
                                        <input
                                                type="tel"
                                                class="form-control"
                                                placeholder="Mobile"
                                                name="mobile"
                                                autocomplete="off"

                                        >



                                    </div>



                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Referral Number</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Referral Number"
                                                name="ref"
                                                autocomplete="off"

                                        >



                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Gurdian's Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Gurdian's Name"
                                                name="g_name"
                                                autocomplete="off"

                                        >



                                    </div>



                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Date of Birth</label>
                                        <input
                                                type="date"
                                                class="form-control"
                                                placeholder="Date of Birth"
                                                id="m_datepicker_1"
                                                name="dob"
                                                autocomplete="off"

                                        >



                                    </div>

                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Pan Number</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Pan Number"
                                                name="pan"
                                                autocomplete="off"

                                        >



                                    </div>

                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Aadhar Number</label>
                                        <input
                                                data-mask="9999 9999 9999 9999"
                                                class="form-control"
                                                type="text"
                                                placeholder="Aadhar Number"
                                                name="aadhar"
                                                autocomplete="off"

                                        >



                                    </div>

                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Billing Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Billing Name"
                                                name="billing_name"
                                                autocomplete="off"

                                        >



                                    </div>

                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">GSTIN Number</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="GSTIN Number"
                                                name="gstin"
                                                autocomplete="off"

                                        >



                                    </div>

                                     <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Select State</label>
                                         <select  name="gstin_state" class="form-control m-input">
                                             <option selected>Select State</option>
                                             <option value="35">Andaman and Nicobar</option>
                                             <option value="37">Andhra Pradesh</option>
                                             <option value="12">Arunachal Pradesh</option>
                                             <option value="18">Assam</option>
                                             <option value="10">Bihar</option>
                                             <!-- <option value="Chandigarh">Chandigarh</option> -->
                                             <option value="04">Chhattisgarh</option>
                                             <option value="26">Dadra and Nagar Haveli</option>
                                             <option value="25">Daman and Diu</option>
                                             <option value="07">Delhi</option>
                                             <!-- <option value="Foreign">Foreign</option> -->
                                             <option value="30">Goa</option>
                                             <option value="24">Gujarat</option>
                                             <option value="06">Haryana</option>
                                             <option value="02">Himachal Pradesh</option>
                                             <option value="01">Jammu and Kashmir</option>
                                             <option value="20">Jharkhand</option>
                                             <option value="29">Karnataka</option>
                                             <option value="32">Kerala</option>
                                             <option value="31">Lakshadweep</option>
                                             <option value="23">Madhya Pradesh</option>
                                             <option value="27">Maharastra</option>
                                             <option value="14">Manipur</option>
                                             <option value="17">Meghalaya</option>
                                             <option value="15">Mizoram</option>
                                             <option value="13">Nagaland</option>
                                             <option value="21">Orissa</option>
                                             <option value="34">Puducherry</option>
                                             <option value="03">Punjab</option>
                                             <option value="08">Rajasthan</option>
                                             <option value="11">Sikkim</option>
                                             <option value="33">Tamil Nadu</option>
                                             <option value="36">Telangana</option>
                                             <option value="16">Tripura</option>
                                             <option value="09">Uttar Pradesh</option>
                                             <option value="05">Uttarakhand</option>
                                             <option value="19">West Bengal</option>

                                         </select>



                                    </div>




                                </div>

                                <div class="form-row">




                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Password</label>
                                        <input
                                                type="password"
                                                class="form-control"
                                                placeholder="Password"
                                                name="password"

                                                required
                                        >

                                    </div>

                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Rank</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Rank"
                                                name="commission"

                                        >

                                    </div>

                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Street Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Street Name"
                                                name="street"

                                        >

                                    </div>

                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Locality</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Locality"
                                                name="locality"

                                        >

                                    </div>

                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">City / Village</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="City / Village"
                                                name="city"

                                        >

                                    </div>

                                     <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">State</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="State"
                                                name="state"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Country</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Country"
                                                name="country"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Pincode</label>
                                        <input
                                                type="number"
                                                class="form-control"
                                                placeholder="Pincode"
                                                name="pin"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Nominee Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Nominee Name"
                                                name="nominee_name"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Relation with Applicant</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Relation with Applicant"
                                                name="nominee_relation"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">DOB</label>
                                        <input
                                                type="date"
                                                class="form-control"
                                                placeholder="DOB"
                                                id="m_datepicker_1"
                                                name="nominee_dob"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Bank Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Bank Name"
                                                name="bank_name"

                                        >

                                    </div>

                                      <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Bank Branch Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Bank Branch Name"
                                                name="bank_branch"

                                        >

                                    </div>

                                         <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">IFSC Code</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="IFSC Code"
                                                name="bank_ifsc"

                                        >

                                    </div>

                                         <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Account Holder Name</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Account Holder Name"
                                                name="bank_account_holder_name"

                                        >

                                    </div>

                                         <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Account Number</label>
                                        <input
                                                type="text"
                                                class="form-control"
                                                placeholder="Account Number"
                                                name="bank_account_number"

                                        >

                                    </div>









                                </div>

                                <hr>
                                <div class="form-row">






                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Sponcer id</label>
                                        <input

                                                type="text"
                                                class="form-control"
                                                id="sponcerid"
                                                name="sponser_id"

                                        >

                                    </div>


                                    <div class="col-md-6 mb-10">

                                        <input class="custom-control-label" type="radio" name="hand" value="left" checked > Left<br>



                                    </div>


                                    <div class="col-md-6 mb-10">

                                        <input class="custom-control-label" type="radio" name="hand" value="right" > Right<br>



                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="form-check custom-control custom-checkbox">
                                        <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                        <label class="form-check-label custom-control-label" for="invalidCheck">
                                            Agree to terms and conditions
                                        </label>
                                        <div class="invalid-feedback">
                                            You must agree before submitting.
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-primary registercustomer"  type="submit">Save Customer</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')



    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.theme.min.css"/>
    <script src="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.js" type="text/javascript"></script>


    <script>
        $(document).ready(function () {


            var src = '{{route('admin.customer.create.search')}}';
            $("#sponcerid").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.ic_number);
                    //  $('.sponser--id').val(ui.item.sponser_id);

                    if(ui.item.id >= 1){

                        $(".registercustomer").removeAttr("disabled");

                    } else {
                        alert('Sponcer id not Available')
                    }










                }

            });
        });
    </script>



    @endsection