@extends('base')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('{{asset('/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')}}')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            @if ($customer->customer_to_profile_pictures !== null )

                                                <img src="{{asset('/storage/'.$customer->customer_to_profile_pictures->path)}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @else


                                                <img src="{{asset('dist/img/avatar12.jpg')}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @endif
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$customer->fname}} {{$customer->lname}}</div>
                                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Phone</span><span class="mr-5">{{$customer->phone}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Mobile</span><span class="mr-5">{{$customer->mobile}}</span></span><span> <br>

                                                <span class="font-weight-500 pr-5">Email</span> <span>{{$customer->email}}</span>
                                        <br> <span class="font-weight-500 pr-5">Hand</span> <span>{{$customer->hand}}</span></span></div>
                                        <br>

                                    <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="bg-white shadow-bottom">
                    <div class="container">
                        <ul class="nav nav-light nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="{{route('admin.customer.details',$customer->id)}}" class="d-flex h-60p align-items-center nav-link ">Tree View</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.profile',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.password',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Password</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.files',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Files</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                                <div class="row">


                                    <div class="col-xl-12">
                                        <section class="hk-sec-wrapper" style="margin: 15px">


                                            <!--begin: Search Form -->



                                            <div class="row">
                                                <div class="col-sm">
                                                    <form class="needs-validation" method="post"
                                                          action="{{route('admin.customer.profile.update', $customer->id)}}">


                                                        @csrf
                                                        <div class="form-row">
                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom01">First name</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="First name"  name="fname"
                                                                       value="{{$customer->fname}}"
                                                                       required>

                                                            </div>
                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom02">Last name</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="Last Name"
                                                                       name="lname"
                                                                       value="{{$customer->lname}}"

                                                                       required>


                                                            </div>



                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Email</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Email"
                                                                        name="email"
                                                                        autocomplete="off"
                                                                        required
                                                                        value="{{$customer->email}}"
                                                                >



                                                            </div>




                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">mobile</label>
                                                                <input
                                                                        type="tel"
                                                                        class="form-control"
                                                                        placeholder="Mobile"
                                                                        name="mobile"
                                                                        autocomplete="off"
                                                                        value="{{$customer->mobile}}"

                                                                >



                                                            </div>






                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Gurdian's Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Gurdian's Name"
                                                                        name="g_name"
                                                                        autocomplete="off"
                                                                        value="{{$customer->g_name}}"

                                                                >



                                                            </div>



                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Date of Birth</label>
                                                                <input
                                                                        type="date"
                                                                        class="form-control"
                                                                        placeholder="Date of Birth"
                                                                        id="m_datepicker_1"
                                                                        name="dob"
                                                                        autocomplete="off"
                                                                        value="{{$customer->dob}}"

                                                                >



                                                            </div>

                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Pan Number</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Pan Number"
                                                                        name="pan"
                                                                        autocomplete="off"
                                                                        value="{{$customer->pan}}"

                                                                >



                                                            </div>

                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Aadhar Number</label>
                                                                <input
                                                                        data-mask="9999 9999 9999 9999"
                                                                        class="form-control"
                                                                        type="text"
                                                                        placeholder="Aadhar Number"
                                                                        name="aadhar"
                                                                        autocomplete="off"
                                                                        value="{{$customer->aadhar}}"

                                                                >



                                                            </div>

                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Billing Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Billing Name"
                                                                        name="billing_name"
                                                                        autocomplete="off"
                                                                        value="{{$customer->billing_name}}"

                                                                >



                                                            </div>

                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">GSTIN Number</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="GSTIN Number"
                                                                        name="gstin"
                                                                        autocomplete="off"
                                                                        value="{{$customer->gstin}}"

                                                                >



                                                            </div>

                                                            <div class="col-md-3 mb-10">
                                                                <label for="validationCustom03">Select State</label>
                                                                <select  name="gstin_state" class="form-control m-input">
                                                                    <option selected>Select State</option>
                                                                    <option
                                                                            value="{{$customer->gstin_state}}">{{$customer->gstin_state}}</option>
                                                                    <option value="35">Andaman and Nicobar</option>
                                                                    <option value="37">Andhra Pradesh</option>
                                                                    <option value="12">Arunachal Pradesh</option>
                                                                    <option value="18">Assam</option>
                                                                    <option value="10">Bihar</option>
                                                                    <!-- <option value="Chandigarh">Chandigarh</option> -->
                                                                    <option value="04">Chhattisgarh</option>
                                                                    <option value="26">Dadra and Nagar Haveli</option>
                                                                    <option value="25">Daman and Diu</option>
                                                                    <option value="07">Delhi</option>
                                                                    <!-- <option value="Foreign">Foreign</option> -->
                                                                    <option value="30">Goa</option>
                                                                    <option value="24">Gujarat</option>
                                                                    <option value="06">Haryana</option>
                                                                    <option value="02">Himachal Pradesh</option>
                                                                    <option value="01">Jammu and Kashmir</option>
                                                                    <option value="20">Jharkhand</option>
                                                                    <option value="29">Karnataka</option>
                                                                    <option value="32">Kerala</option>
                                                                    <option value="31">Lakshadweep</option>
                                                                    <option value="23">Madhya Pradesh</option>
                                                                    <option value="27">Maharastra</option>
                                                                    <option value="14">Manipur</option>
                                                                    <option value="17">Meghalaya</option>
                                                                    <option value="15">Mizoram</option>
                                                                    <option value="13">Nagaland</option>
                                                                    <option value="21">Orissa</option>
                                                                    <option value="34">Puducherry</option>
                                                                    <option value="03">Punjab</option>
                                                                    <option value="08">Rajasthan</option>
                                                                    <option value="11">Sikkim</option>
                                                                    <option value="33">Tamil Nadu</option>
                                                                    <option value="36">Telangana</option>
                                                                    <option value="16">Tripura</option>
                                                                    <option value="09">Uttar Pradesh</option>
                                                                    <option value="05">Uttarakhand</option>
                                                                    <option value="19">West Bengal</option>

                                                                </select>



                                                            </div>




                                                        </div>

                                                        <div class="form-row">






                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Rank</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Rank"
                                                                        name="commission"

                                                                        value="{{$customer->commission}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Street Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Street Name"
                                                                        name="street"
                                                                        value="{{$customer->street}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Locality</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Locality"
                                                                        name="locality"
                                                                        value="{{$customer->locality}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">City / Village</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="City / Village"
                                                                        name="city"
                                                                        value="{{$customer->city}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">State</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="State"
                                                                        name="state"
                                                                        value="{{$customer->state}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Country</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Country"
                                                                        name="country"
                                                                        value="{{$customer->country}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Pincode</label>
                                                                <input
                                                                        type="number"
                                                                        class="form-control"
                                                                        placeholder="Pincode"
                                                                        name="pin"
                                                                        value="{{$customer->pin}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Nominee Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Nominee Name"
                                                                        name="nominee_name"
                                                                        value="{{$customer->nominee_name}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Relation with Applicant</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Relation with Applicant"
                                                                        name="nominee_relation"
                                                                        value="{{$customer->nominee_relation}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">DOB</label>
                                                                <input
                                                                        type="date"
                                                                        class="form-control"
                                                                        placeholder="DOB"
                                                                        id="m_datepicker_1"
                                                                        name="nominee_dob"
                                                                        value="{{$customer->nominee_dob}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Bank Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Bank Name"
                                                                        name="bank_name"
                                                                        value="{{$customer->bank_name}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Bank Branch Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Bank Branch Name"
                                                                        name="bank_branch"
                                                                        value="{{$customer->bank_branch}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">IFSC Code</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="IFSC Code"
                                                                        name="bank_ifsc"
                                                                        value="{{$customer->bank_ifsc}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Account Holder Name</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Account Holder Name"
                                                                        name="bank_account_holder_name"
                                                                        value="{{$customer->bank_account_holder_name}}"

                                                                >

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Account Number</label>
                                                                <input
                                                                        type="text"
                                                                        class="form-control"
                                                                        placeholder="Account Number"
                                                                        name="bank_account_number"
                                                                        value="{{$customer->bank_account_number}}"

                                                                >

                                                            </div>









                                                        </div>

                                                        <hr>
                                                        <div class="form-row">






                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom03">Sponcer id</label>
                                                                <input readonly

                                                                        type="text"
                                                                        class="form-control"
                                                                        id="sponcerid"
                                                                        name="sponser_id"
                                                                       value="{{$customer->sponser_id}}"

                                                                >

                                                            </div>



                                                        </div>







                                                        <div class="form-group">
                                                            <div class="form-check custom-control custom-checkbox">
                                                                <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                                                <label class="form-check-label custom-control-label" for="invalidCheck">
                                                                    Agree to terms and conditions
                                                                </label>
                                                                <div class="invalid-feedback">
                                                                    You must agree before submitting.
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <button class="btn btn-primary registercustomer"  type="submit">Update Customer</button>
                                                    </form>
                                                </div>
                                            </div>







                                        </section>



                                        <section class="hk-sec-wrapper "  style="margin: 15px">




                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                        @endif


                                                    <!--begin: Search Form -->



                                                    <div class="row" >
                                                        <div class="col-sm">
                                                            <form
                                                                    enctype="multipart/form-data"
                                                                    class="needs-validation" method="post"  action="{{route('admin.customer.profile.img.update')}}">


                                                                @csrf
                                                                <div class="form-row">
                                                                    <div class="col-md-3 mb-10">
                                                                        <label for="validationCustom01">Update Profile 500px x 500px</label>
                                                                        <input type="file" class="form-control"
                                                                               placeholder="Image"  name="img" required>

                                                                        <input type="hidden" class="form-control"
                                                                               placeholder="Image" value="{{$customer->id}}" name="customer_id" required>


                                                                    </div>
                                                                </div>


                                                                <button class="btn btn-primary registercustomer"  type="submit">Update Profile</button>

                                                            </form>



                                                        </div>
                                                    </div>









                                        </section>


                                    </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@endsection


@section('footer')








    @endsection