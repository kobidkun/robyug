@extends('base')

@section('content')
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('{{asset('/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')}}')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            @if ($customer->customer_to_profile_pictures !== null )

                                                <img src="{{asset('/storage/'.$customer->customer_to_profile_pictures->path)}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @else


                                                <img src="{{asset('dist/img/avatar12.jpg')}}"
                                                     alt="user" class="avatar-img rounded-circle">

                                            @endif
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$customer->fname}} {{$customer->lname}}</div>
                                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Phone</span><span class="mr-5">{{$customer->phone}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Mobile</span><span class="mr-5">{{$customer->mobile}}</span></span><span> <br>

                                                <span class="font-weight-500 pr-5">Email</span> <span>{{$customer->email}}</span>
                                        <br> <span class="font-weight-500 pr-5">Hand</span> <span>{{$customer->hand}}</span></span></div>
                                        <br>

                                        <br>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="bg-white shadow-bottom">
                    <div class="container">
                        <ul class="nav nav-light nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="{{route('admin.customer.details',$customer->id)}}" class="d-flex h-60p align-items-center nav-link ">Tree View</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.profile',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.password',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Password</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.customer.files',$customer->id)}}" class="d-flex h-60p align-items-center nav-link">Files</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                                <div class="row">


                                    <div class="col-xl-12">
                                        <section class="hk-sec-wrapper">


                                            <!--begin: Search Form -->



                                            <div class="row">
                                                <div class="col-sm">

                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif


                                                    <form class="needs-validation"
                                                          method="post"
                                                          enctype="multipart/form-data"
                                                          action="{{route('admin.customer.files.files.update',$customer->id)}}">


                                                        @csrf
                                                        <div class="form-row">
                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom01"> Name</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="First name"  name="file_name" required>

                                                            </div>

                                                            <div class="col-md-6 mb-10">
                                                                <label for="validationCustom01">Update Profile 500px x 500px</label>
                                                                <input type="file" class="form-control"
                                                                       placeholder="Image"  name="img" required>

                                                                <input type="hidden" class="form-control"
                                                                       placeholder="Image" value="{{$customer->id}}" name="customer_id" required>

                                                            </div>

                                                        </div>

                                                        <button class="btn btn-primary registercustomer"  type="submit">Update Profile</button>


                                                    </form>



                                                </div>
                                            </div>



                                        </section>
                                    </div>


                        </div>



                                <div class="row">


                                    <div class="col-xl-12">
                                        <section class="hk-sec-wrapper">


                                            <!--begin: Search Form -->



                                            <div class="row">
                                                <div class="col-sm">

                                                    @foreach($customer->customer_to_files as $images)
                                                    <div class="col-xl-4">
                                                        <a data-fancybox="gallery"
                                                           href="{{asset('/storage/'.$images->path)}}">
                                                            <img width="350px" src="{{asset('/storage/'.$images->path)}}">
                                                        </a>
                                                    </div>

                                                        @endforeach



                                                </div>
                                            </div>



                                        </section>
                                    </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>


@endsection


@section('footer')


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>






    @endsection