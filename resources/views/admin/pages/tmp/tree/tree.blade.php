@extends('base')

@section('content')

    <link rel="stylesheet" href="{{asset('/js/vis/dist/vis.min.css') }}" />
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Tree View
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">

                        </div>
                    </div>



                    <div class="col-sm">
                        <!--begin: Search Form -->


                        <div class="row">

                            <div class="col-12" style="text-align: center">

                                <h5>Primary Member</h5>

                                <button class="btn btn-primary primary-member">

                                </button>



                            </div>





                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer" style="text-align: center">
                                <br>
                                <br>

                              <div class="row">
                                  <div class="col-md-6 mb-10">
                                      <div class="" id="downlineleft">

                                      </div>
                                  </div>


                                  <div class="col-md-6 mb-10">



                                      <div class="" id="downlineright">

                                      </div>
                                  </div>
                              </div>




                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer2" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup2"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline2">



                                </div>
                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer3" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup3"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline3">

                                </div>
                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer4" style="text-align: center">
                                <br>
                                <div id="memup4"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline4">

                                </div>
                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer5" style="text-align: center">
                                <br>
                                <div id="memup5"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline5">

                                </div>
                            </div>
                            {{--downlines--}}


                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer6" style="text-align: center">
                                <br>
                                <div id="memup6"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline6">

                                </div>
                            </div>
                            {{--downlines--}}


                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer7" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup7"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline7">

                                </div>
                            </div>
                            {{--downlines--}}


                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer8" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup8"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline8">

                                </div>
                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer9" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup9"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline9">

                                </div>
                            </div>
                            {{--downlines--}}



                            {{--downlines--}}

                            <div  class="col-12" id="downlinecontainer10" style="text-align: center">
                                <br>
                                <div style="font-weight: 500;font-size: 24px" id="memup10"></div>
                                <br>
                                <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline10">

                                </div>
                            </div>
                            {{--downlines--}}








                        </div>







                    </div>
                    <!--end::Section-->

                </div>


            </div>
        </div>
    </div>

@endsection



@section('footer')


    <style>
        .treeviewbutton{
            margin-top: 5px;
        }

        .fdownline{
            text-align: center;
            msrgin-left: 500px;
        }
    </style>

    <script>






        var downline = [];

        $.ajax({
            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
            url:"{{route('admin.tree.view.by.id.admin')}}",
            //  async: true,
            dataType: 'json',

            success: function(obj){
                // var json = $.parseJSON(obj);

                $(".primary-member").replaceWith('<div style="text-align: center"><i style="font-size: 128px; color: #e12500!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br> ' +
                    '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> </div>')







                    $("#downlineleft").append(
                        '<div class="col-md-6 mb-10 fdownline">' +
                        '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                        '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                        ' <br> IC No:  '+obj.left.ic_number+
                        ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+



                        '</div>'


                    );

                $("#downlineright").append(
                        '<div class="col-md-6 mb-10 fdownline">' +
                        '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                        '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                        ' <br> IC No:  '+obj.right.ic_number+
                        ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+



                        '</div> '


                    )


            },
            error: function(error){
                alert(error);
            }
        })



        //   href="/dashboard/admin/tree-view/view/'+v.id+'"

        $(document).on("click", ".dwnlinemembers" , function(){

            $("#downline2").html('');
            $("#memup2").html('');


            $("#downline3").html('');
            $("#memup3").html('');

            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');



            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);

                    $("#memup2").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number

                    )


                    if(obj.left === null){

                         $("#downline2").append(

                        '<div class="col-md-6 mb-10 fdownline">' +
                        '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                        '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                        ' <br> IC No:  ' + obj.right.ic_number +
                        ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                        '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                         )

                    }


                    if(obj.right === null){
                                       $("#downline2").append(

                         '<div class="col-md-6 mb-10 fdownline">' +
                         '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.left.fname+ ' '+obj.left.lname +
                         ' <br> IC No:  '+obj.left.ic_number+
                         ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                                       )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline2").append('No Downline')
                    }



                    else{
                        $("#downline2").append(

                        '<div class="col-md-6 mb-10 fdownline">' +
                        '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                        '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.left.fname+ ' '+obj.left.lname +
                        ' <br> IC No:  '+obj.left.ic_number+
                        ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                        '<div class="col-md-6 mb-10 fdownline">' +
                        '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                        '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers3">'+obj.right.fname+ ' '+obj.right.lname +
                        ' <br> IC No:  '+obj.right.ic_number+
                        ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }




                },
                error: function(error){
                    alert(error);
                }
            })




        });

        // next

        $(document).on("click", ".dwnlinemembers3" , function(){



            $("#downline3").html('');
            $("#memup3").html('');

            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup3").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers4">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline3").append('No Downline')
                    }



                    else{
                        $("#downline3").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers4">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                },
                error: function(error){
                    alert(error);
                }
            })




        });



        $(document).on("click", ".dwnlinemembers4" , function(){



            $("#downline4").html('');
            $("#memup4").html('');

            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);






            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup4").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                  //  $.each(obj.downline,function(k,v){


                    if(obj.left === null){

                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers5">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline4").append('No Downline')
                    }



                    else{
                        $("#downline4").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers5">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                 //   });
                },
                error: function(error){
                    alert(error);
                }
            })




        });

        // five

        $(document).on("click", ".dwnlinemembers5" , function(){


            $("#downline5").html('');
            $("#memup5").html('');

            $("#downline6").html('');
            $("#memup6").html('');

            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup5").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                  //  $.each(obj.downline,function(k,v){



                    if(obj.left === null){

                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline5").append('No Downline')
                    }



                    else{
                        $("#downline5").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                      //  console.log(v.fname);
                  //  });
                },
                error: function(error){
                    alert(error);
                }
            })




        });


        $(document).on("click", ".dwnlinemembers6" , function(){

            $("#downline6").html('');
            $("#memup6").html('');


            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup6").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                  //  $.each(obj.downline,function(k,v){



                    if(obj.left === null){

                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline6").append('No Downline')
                    }



                    else{
                        $("#downline6").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                      //  console.log(v.fname);
                  //  });
                },
                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers7" , function(){



            $("#downline7").html('');
            $("#memup7").html('');


            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup7").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)






                    if(obj.left === null){

                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline7").append('No Downline')
                    }



                    else{
                        $("#downline7").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }



                },
                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers8" , function(){




            $("#downline8").html('');
            $("#memup8").html('');


            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj) {
                    // var json = $.parseJSON(obj);
                    $("#memup8").html(obj.customer.fname + ' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)


                    if(obj.left === null){

                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline8").append('No Downline')
                    }



                    else{
                        $("#downline8").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }

                },



                error: function(error){
                    alert(error);
                }
            })




        });

        $(document).on("click", ".dwnlinemembers9" , function(){



            $("#downline9").html('');
            $("#memup9").html('');


            $("#downline10").html('');
            $("#memup10").html('');

            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup9").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline9").append('No Downline')
                    }



                    else{
                        $("#downline9").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                },
                error: function(error){
                    alert(error);
                }
            })




        });


        $(document).on("click", ".dwnlinemembers10" , function(){



            $("#downline10").html('');
            $("#memup10").html('');


            var USERID = $(this).val()
            alert ( USERID);



            var downline = [];

            $.ajax({
                /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
               url:'{{url('/admin/dashboard/admin/tree-view/by-id/down-line/')}}'+'/'+USERID,
                //  async: true,
                dataType: 'json',

                success: function(obj){
                    // var json = $.parseJSON(obj);
                    $("#memup10").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)







                    if(obj.left === null){

                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="' + obj.right.id + '" class="btn btn-success treeviewbutton dwnlinemembers">' + obj.right.fname + ' ' + obj.right.lname +
                            ' <br> IC No:  ' + obj.right.ic_number +
                            ' <br> Sponcer ID:  ' + obj.right.sponser_id + '</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/' + obj.right.id + '"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                    if(obj.right === null){
                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'

                        )

                    }

                    else if(obj.right === null && obj.left === null) {

                        $("#downline10").append('No Downline')
                    }



                    else{
                        $("#downline10").append(

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.left.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.left.fname+ ' '+obj.left.lname +
                            ' <br> IC No:  '+obj.left.ic_number+
                            ' <br> Sponcer ID:  '+obj.left.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.left.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a></div>'+

                            '<div class="col-md-6 mb-10 fdownline">' +
                            '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                            '<button  value="'+obj.right.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+obj.right.fname+ ' '+obj.right.lname +
                            ' <br> IC No:  '+obj.right.ic_number+
                            ' <br> Sponcer ID:  '+obj.right.sponser_id+'</button> <br><br>' +
                            '<a href="/dashboard/admin/tree-view/view/'+obj.right.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'
                        )

                    }


                },
                error: function(error){
                    alert(error);
                }
            })




        });







    </script>

@endsection


