@extends('base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">All Customer</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->

        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">All Customer</h5>
                   <div class="row">
                        <div class="col-sm">

                            <table class="table table-striped table-bordered" id="user-table" style="width: 100%">
                                <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>F Name</th>
                                    <th>L NAME</th>
                                    <th>EMAIL</th>
                                    <th>MOBILE</th>
                                    <th>SPONCER</th>
                                    <th>Hand</th>
                                    <th>Verified</th>
                                    <th>ACTION</th>



                                </tr>
                                </thead>
                            </table>



                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')


    <link rel="stylesheet" type="text/css" href="{{asset('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}"/>

    <script type="text/javascript" src="{{asset('/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/vendors/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.temp.dt')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'sponser_id', name: 'sponser_id' },
                    { data: 'hand', name: 'hand' },
                    { data: 'is_mobile_verified', name: 'is_mobile_verified' },



                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>





    @endsection