@extends('base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">All Customer</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->

        <!-- /Title -->

        <style>
            .top-title{
                color: red;
                margin: 0;
                padding: 10px 0;
                text-align: center;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                background-color: white;
                font-weight: 400;
                box-shadow: 0px 1px 4px rgba(0,0,0,0.2);
            }

            .container{
                display: flex;
                position: relative;
                align-items: center;
                padding: 12px 20px;
                background-color: #ffffff;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                box-shadow: 0px 2px 4px rgba(0,0,0,0.2);

            }

            .left-content img{
                border-radius: 50%;
                max-width: 80%;
            }

            .right-content{
                margin-top: 5px;
                margin-right: 10px;
            }

            .greeting{
                background: tomato;
                color: white;
                padding: 6px 12px;
                letter-spacing: 1.2px;
                text-transform: uppercase;
                user-select: none;
                position: relative;
            }

            .greeting:before{
                content: '';
                width:0;
                height:0;
                border: 6px solid;
                position: absolute;
                top: 100%;
                left:0;
                border-color: transparent;
                border-top-color: #5f4cff;
                border-left-color: #8c89ff;
            }

            .my-name{
                font-weight: 200;
                margin-bottom: 0;
            }

            .my-name span{
                font-weight: 400;
            }

            .my-website a{
                font-weight: 300;
                font-size: 14px;
                color: rgb(15,49,82);
            }

            .detail-infor{
                display: flex;
                margin-top: 1rem;
            }

            .labels{
                font-weight: 600;
                margin-right: 2rem;
            }

            .bottom-content{
                position: absolute;
                top: 95%;
                left: 50%;
                transform: translateX(-50%);
                background: #585cff;
                width: 100%;
                padding: 7px 0;
                display: flex;
                justify-content: center;
                align-items: center;
                box-shadow: 0px 2px 4px rgba(0,0,0,0.2);
                border-bottom-left-radius: 4px;
                border-bottom-right-radius: 4px;
            }

            .bottom-content a{
                color: black;
                text-decoration: none;
                opacity: 0.8;
                transition: 200ms;
                font-size: 24px;
                margin:0  10px;
            }

            .bottom-content a:hover{
                opacity: 1;
            }
        </style>
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">

                   <div class="row">


                            <div class="container">
                                <div class="left-content">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7Lvm8NKK495L4AkQYR30rmuZlzRX4RmhBg4AXyggdw-INYJwP" alt="avatar">
                                </div>
                                <div class="right-content">

                                    <h2 class="my-name">
                                        <span>{{$customer->fname}} {{$customer->lname}}</span>
                                    </h2>
                                    <div class="detail-infor">
                                        <div class="labels">
                                            <p>Name:</p>
                                            <p>Email:</p>
                                            <p>Mobile:</p>
                                            <p>Phone:</p>
                                            <p>Sponcer ID:</p>
                                            <p>Hand:</p>
                                            <p>Verified:</p>
                                        </div>
                                        <div class="infor">
                                            <p>{{$customer->fname}} {{$customer->lname}}</p>
                                            <p>{{$customer->email}}</p>
                                            <p>{{$customer->phone}}</p>
                                            <p>{{$customer->mobile}}</p>
                                            <p>{{$customer->sponser_id}}</p>
                                            <p>{{$customer->hand}}</p>
                                            <p>{{$customer->is_mobile_verified}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-content">
                                   <a href="{{route('admin.customer.temp.convert.process',$customer->id)}}" style="background-color: #585cff;border-color: #585cff" class="btn btn-block btn-primary">Confirm</a>
                                </div>
                            </div>


                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')


    <link rel="stylesheet" type="text/css" href="{{asset('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}"/>

    <script type="text/javascript" src="{{asset('/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/vendors/datatables.net-responsive/js/dataTables.responsive.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.temp.dt')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'sponser_id', name: 'sponser_id' },
                    { data: 'hand', name: 'hand' },
                    { data: 'is_mobile_verified', name: 'is_mobile_verified' },



                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>





    @endsection