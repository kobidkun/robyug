@extends('admin.base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Group</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Create Group</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Create Group</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" method="post"  action="{{route('admin.group.primary.create.save')}}">


                                @csrf
                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">Title</label>
                                        <input type="text" class="form-control title"
                                               placeholder="Title"  name="title" required>

                                    </div>
                                    <div class="col-md-9 mb-10">
                                        <label for="validationCustom02">Description</label>
                                        <input type="text" class="form-control"
                                               placeholder="Description"
                                               name="description"

                                               >


                                    </div>



                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Slug</label>
                                        <input
                                                type="text"
                                                class="form-control slug"
                                                placeholder="Slug"
                                                name="slug"
                                                autocomplete="off"
                                                required
                                                readonly
                                        >



                                    </div>




                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Featured</label>
                                         <select class="form-control" name="featured" id="">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>



                                    </div>










                                </div>














                                <div class="form-group">
                                    <div class="form-check custom-control custom-checkbox">
                                        <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                        <label class="form-check-label custom-control-label" for="invalidCheck">
                                            Agree to terms and conditions
                                        </label>
                                        <div class="invalid-feedback">
                                            You must agree before submitting.
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-primary registercustomer"  type="submit">Save </button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')



    <script>



        $(document).ready(function() {
            $(".title").keyup(function () {

                var cat_name_val = $(this).val();
                var actualSlug = cat_name_val.replace(/ /g, '-').toLowerCase()

                $(".slug").val(actualSlug);


            });

        });
</script>



@endsection