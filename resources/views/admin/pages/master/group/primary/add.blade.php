@extends('admin.base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Group</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Group</h4>

            <a class="btn btn-gradient-success" href="{{route('admin.group.primary.create.view')}}">Create</a>


        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">



                <section class="hk-sec-wrapper">

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0 ">
                                        <thead class="thead-primary">
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Slug</th>
                                            <th>Secondary Groups</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        @foreach ($groups as $group)

                                            <tr>
                                                <th scope="row">{{$group->id}}</th>
                                                <td>{{$group->title}}</td>
                                                <td class="peity-gradient">{{$group->slug}}</td>
                                                <td class="peity-gradient">

                                                    @foreach($group->secondary_groups as $sec)

                                                    <button class="btn btn-gradient-warning">
                                                        {{$sec->title}}
                                                    </button>  <br>

                                                        @endforeach

                                                </td>
                                                <td>
                                                    <a href="{{route('admin.group.primary.edit',$group->id)}}"
                                                       class="btn btn-info">Edit</a>
                                                </td>

                                                <td>
                                                    <a href="{{route('admin.group.primary.create.delete',$group->id)}}"
                                                       class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>

                                        @endforeach



                                        {{ $groups->links() }}



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>








            </div>
        </div>
    </div>


@endsection


@section('footer')



    \



@endsection