@extends('admin.base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Group</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Group</h4>

            <a class="btn btn-gradient-success" href="{{route('admin.group.primary.create.view')}}">Create</a>


        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">



                <section class="hk-sec-wrapper">

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">



                                <form class="needs-validation" method="post"
                                      action="{{route('admin.filter.types.values.save')}}">


                                    @csrf
                                    <div class="form-row">
                                        <div class="col-md-3 mb-10">
                                            <label for="validationCustom01">Filter</label>
                                            <input type="text" class="form-control title"
                                                   readonly value="{{$filter->title}}"
                                                   placeholder="Title"  name="filter_types_title" required>

                                            <input name="filter_type_id" type="hidden" value="{{$filter->id}}">

                                        </div>
                                        <div class="col-md-9 mb-10">
                                            <label for="validationCustom02">Value</label>
                                            <input type="text" class="form-control"
                                                   placeholder="Value"
                                                   name="value"

                                                   required>


                                        </div>











                                    </div>














                                    <div class="form-group">
                                        <div class="form-check custom-control custom-checkbox">
                                            <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                            <label class="form-check-label custom-control-label" for="invalidCheck">
                                                Agree to terms and conditions
                                            </label>
                                            <div class="invalid-feedback">
                                                You must agree before submitting.
                                            </div>
                                        </div>
                                    </div>


                                    <button class="btn btn-primary registercustomer"  type="submit">Save</button>
                                </form>





                            </div>
                        </div>
                    </div>
                </section>








            </div>
        </div>
    </div>


@endsection


@section('footer')






@endsection