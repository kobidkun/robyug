@extends('admin.base')

@section('content')



        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                @include('admin.pages.product.menu', ['prs' => $prs])
                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                        <div class="row">


                            <div class="col-xl-12">
                                <section class="hk-sec-wrapper" style="">


                                    <!--begin: Search Form -->

                                    <a class="btn btn-gradient-success" href="{{route('admin.product.variant.create.view',$prs->id)}}">
                                        Create Variant</a>


                                    <br>
                                    <br>

                                    <div class="row">
                                        <div class="col-sm">





                                            <form method="POST" action="{{route('admin.product.create.gallery.upload')}}"
                                                  class="dropzone"

                                            id="mDropzoneTwoPrimary"
                                            >

                                           @csrf
                                            <input name="product_id" value="{{$prs->id}}" type="hidden">
                                            <input name="product_name" value="{{$prs->title}}" type="hidden">
                                            <div class="m-dropzone__msg dz-message needsclick">




                                                <h3 class="m-dropzone__msg-title">
                                                    Please click the image

                                                </h3>
                                            </div>
                                            </form>








                                        </div>
                                    </div>




                                    <div class="row">
                                        <div class="col-sm">

<div class="row">

    @foreach($prs->product_to_images as $img)

        <div class="col-md-4">
            <img src="{{asset('/storage/'.$img->path)}}" width="250px" alt="">
        </div>
        <br>
        <center>
            <a class=" btn btn-danger" href="{{route('admin.product.delete.gallery',$img->id)}}">Delete</a>

        </center>
    @endforeach
</div>


                                        </div>
                                    </div>




                                </section>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection

@section('footer')
    <script src="{{asset('dist/js/tooltip-data.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>


    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload

                Dropzone.options.mDropzoneTwoPrimary  = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
                    init: function () {
                        this.on("maxfilesexceeded", function (file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        })

                    }
                };
                Dropzone.options.mDropzoneTwo = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 5, // MB
                    maxFiles: 20,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };


            };

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>

@endsection