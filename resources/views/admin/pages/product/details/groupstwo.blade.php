@extends('admin.base')

@section('content')



    <!-- Row -->
    <div class="row">
        <div class="col-xl-12 pa-0">
            @include('admin.pages.product.menu', ['prs' => $prs])
            <div class="tab-content">
                <div class="tab-pane fade show active" role="tabpanel">

                    <div class="row">






                        <div class="col-xl-12">
                            <section class="hk-sec-wrapper" style="">


                                <!--begin: Search Form -->


                                <h5>Add Secondary Group</h5>


                                <br>
                                <br>




                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <form  method="post"
                                               enctype="multipart/form-data"
                                               action="{{route('admin.product.add.secondary.group.post')}}">


                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-3 mb-10">









                                                    <select class="searchable" multiple="multiple"
                                                            name="secondary_group_id[]">
                                                        @foreach ($sgs as $filter)
                                                            <option value="{{$filter->id}}">{{$filter->title}}</option>
                                                        @endforeach

                                                    </select>



                                                </div>


                                                <input type="hidden" name="product_id" value="{{$prs->id}}">




                                            </div>




                                            <button class="btn btn-primary registercustomer"  type="submit">Save</button>
                                        </form>
                                    </div>


                                    <div class="col-sm-6">
                                        <table class="table table-hover mb-0 ">
                                            <thead class="thead-primary">
                                            <tr>
                                                <th>#</th>
                                                <th>secondary Group</th>
                                                <th>Delete Group</th>


                                            </tr>
                                            </thead>
                                            <tbody>


                                            @foreach ($prs->product_to_secondary_groups as $key => $product)

                                                <tr>
                                                    <th scope="row">{{$key++ +1}}</th>
                                                    <td>{{$product->secondary_group}} <br>

                                                    </td>

                                                    <td>
                                                        <a class="btn btn-gradient-danger" href="{{route('admin.product.delete.group.secondary',$product->id)}}">
                                                            Delete
                                                        </a>
                                                    </td>


                                                </tr>

                                            @endforeach







                                            </tbody>
                                        </table>
                                    </div>


                                </div>



                            </section>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('footer')



    <script src="{{asset('dist/js/tooltip-data.js')}}"></script>

    <link href="{{asset('/vendors/lou-multi-select-57fb8d3/css/multi-select.dist.css')}}" media="screen" rel="stylesheet" type="text/css">
    <script src="{{asset('/vendors/lou-multi-select-57fb8d3/js/jquery.multi-select.js')}}" type="text/javascript"></script>

    <script>
        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try Searching'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try Searching'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });


    </script>


@endsection