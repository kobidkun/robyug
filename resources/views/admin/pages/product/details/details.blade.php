@extends('admin.base')

@section('content')



        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">



               @include('admin.pages.product.menu', ['prs' => $prs])






                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                        <div class="row">


                            <div class="col-xl-12">
                                <section class="hk-sec-wrapper" style="">


                                    <!--begin: Search Form -->

                                    <a class="btn btn-gradient-success"
                                       href="{{route('admin.product.variant.create.view',$prs->id)}}">
                                        Create Variant</a>


                                    @if ($prs->featured === '0')

                                        <a class="btn btn-gradient-info"
                                           data-toggle="tooltip-info" data-placement="top"

                                           title="This Product will be listed in Top Positions"

                                           href="{{route('admin.product.feature.activate',$prs->id)}}">
                                            Make it Feature Product</a>


                                        @else

                                        <a class="btn btn-gradient-danger"
                                           data-toggle="tooltip-info" data-placement="top"

                                           title="This Product will be de listed in Top Positions"

                                           href="{{route('admin.product.feature.deactivate',$prs->id)}}">
                                            Make it non Feature Product</a>

                                    @endif




                                    <br>
                                    <br>

                                    <div class="row">
                                        <div class="col-sm">

                                            <table class="table table-hover mb-0 ">
                                                <thead class="thead-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Title</th>
                                                    <th>Slug</th>
                                                    <th>Variant Types</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @foreach ($prs->product_variants as $product)

                                                    <tr>
                                                        <th scope="row">{{$product->id}}</th>
                                                        <td>{{$product->title}}</td>
                                                        <td class="peity-gradient">{{$product->slug}}</td>
                                                        <td class="peity-gradient">{{$product->variant_type}} : {{$product->variant_value}}</td>

                                                        <td>
                                                            <a href="#"
                                                               class="btn btn-danger">Details</a>
                                                        </td>
                                                    </tr>

                                                @endforeach







                                                </tbody>
                                            </table>









                                        </div>
                                    </div>



                                </section>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection


@section('footer')

    <script src="{{asset('dist/js/tooltip-data.js')}}"></script>

@stop