@extends('admin.base')

@section('content')



        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">
                @include('admin.pages.product.menu', ['prs' => $prs])
                <div class="tab-content">
                    <div class="tab-pane fade show active" role="tabpanel">

                        <div class="row">


                            <div class="col-xl-12">
                                <section class="hk-sec-wrapper" style="">


                                    <!--begin: Search Form -->

                                    <a class="btn btn-gradient-success" href="{{route('admin.product.variant.create.view',$prs->id)}}">
                                        Create Variant</a>


                                    <br>
                                    <br>

                                    <div class="row">
                                        <div class="col-sm">

                                            <table class="table table-hover mb-0 ">
                                                <thead class="thead-primary">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Filter</th>
                                                    <th>Value</th>

                                                </tr>
                                                </thead>
                                                <tbody>


                                                @foreach ($prs->product_to_filters as $key => $product)

                                                    <tr>
                                                        <th scope="row">{{$key++ +1}}</th>
                                                        <td>{{$product->filter_name}}</td>
                                                        <td class="peity-gradient">{{$product->filter_slug}}</td>

                                                    </tr>

                                                @endforeach







                                                </tbody>
                                            </table>









                                        </div>
                                    </div>


                                    <br>
                                    <br>

                                    <div class="row">
                                        <div class="col-sm">
                                            <form class="needs-validation" method="post"
                                                  enctype="multipart/form-data"
                                                  action="{{route('admin.product.add.filter.two')}}">


                                                @csrf
                                                <div class="form-row">
                                                    <div class="col-md-3 mb-10">
                                                        <label for="validationCustom01">Select Filter</label>

                                                        <select class="form-control" name="filter">

                                                            @foreach ($filter as $filter)
                                                                <option value="{{$filter->id}}">{{$filter->title}}</option>
                                                            @endforeach


                                                        </select>



                                                    </div>


                                                    <input type="hidden" name="product_id" value="{{$product->id}}">




                                                </div>






















                                                <div class="form-group">
                                                    <div class="form-check custom-control custom-checkbox">
                                                        <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                                        <label class="form-check-label custom-control-label" for="invalidCheck">
                                                            Agree to terms and conditions
                                                        </label>
                                                        <div class="invalid-feedback">
                                                            You must agree before submitting.
                                                        </div>
                                                    </div>
                                                </div>


                                                <button class="btn btn-primary registercustomer"  type="submit">Save</button>
                                            </form>
                                        </div>
                                    </div>



                                </section>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection


@section('footer')

    <script src="{{asset('dist/js/tooltip-data.js')}}"></script>


    <script>



        $(document).ready(function(){
            $(".product-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".product-slug").val(actualSlug);
                $(".product-meta").val(cat_name_val);



            });

            $(".product-base-price").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".product-discount").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".product-tax-percentage").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".primcat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                updatehiddenpc();
            }));



            $(".seccat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                updatehiddensc();
            }));

            function updatehiddenpc() {

                var pccnam =  $(".primcat option:selected").text();

                $(".primarycatname").val(pccnam);

            }

            function updatehiddensc() {

                var pccnam =  $(".seccat option:selected").text();

                $(".secondarycatname").val(pccnam);

            }








            //    var CGST =  $(".product-cgst").val();
            //    var SGST =  $(".product-sgst").val();
            //   var IGST =  $(".product-igst").val();
            // var TOTAL =  $(".product-total").val();
            //   var TOTALTAX =  $(".product-tax-amt").val();



            function onPriceupdate() {
                var BASEPRICE =  $(".product-base-price").val();
                var TAXPERCENTAGE =  $(".product-tax-percentage option:selected").val();
                var DISCOUNT =  $(".product-discount").val();



                //calcul;ate actual price

                var AFTERDISC = BASEPRICE - DISCOUNT ;

                var DISC_PERCENTAGE = (DISCOUNT/BASEPRICE )*100

                $(".product-after-discount").val(AFTERDISC.toFixed(2));
                $(".product-discount-percentage").val(DISC_PERCENTAGE.toFixed(2));


                //cal gst
                //
                //
                //

                var TocalculatePrice =  $(".product-after-discount").val();


                var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                $(".product-cgst").val(CGST.toFixed(2));
                $(".product-sgst").val(SGST.toFixed(2));
                $(".product-igst").val(IGST.toFixed(2));
                $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".product-total-amt").val((Number(TocalculatePrice) + Number(TOTALTAX)).toFixed(2));




                //  $(".product-after-discount").val(AFTERDISC);

                //  console.log(BASEPRICE);









            }





            /// cal change


            $( ".m_selectpicker" )
                .change(


                    function () {
                        // preventDefault();
                        var abr = '<option value="">==select Primary categories==</option>';
                        $("#secondarycatplaceholder").html(abr);


                        var str = $( "#primarycatplaceholder option:selected" ).val();


                        // console.log(str)
                        $.ajax({
                            type: "GET",
                            url: "/admin/category/sec/searchforproducy/"+str,

                            success: function( resp) {
                                //   resp.preventDefault();
                                // $("#msg").prepend(resp);
                                console.log(resp.cat2.length);
                                //  var str = '<option value="0">Select Secondary Category</option>';
                                if (resp.cat2.length > 0 ) {
                                    $.each(resp.cat2, function (index, value) {
                                        str = str+'<option  value="'+value.id+'">'+value.name+'</option>';
                                    });
                                    $("#secondarycatplaceholder").html(str);
                                } else {
                                    //  var abr = '';
                                    $("#secondarycatplaceholder").html('<option value="0">No Secondary categories</option>');
                                }

                                //turtiary

                                var abr2 = '<option value="">==select Secondary categories==</option>';
                                $("#turtiaryycatplaceholder").html(abr2);
                                var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                                $.ajax({
                                    type: "GET",
                                    url: "/admin/category/tur/searchforproducy/"+str2,
                                    success: function( resp2) {
                                        console.log(resp2.cat3.length);
                                        if (resp2.cat3.length > 0 ) {
                                            $.each(resp2.cat3, function (index, value) {
                                                str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                            });
                                            $("#turtiaryycatplaceholder").html(str2);
                                        } else {
                                            $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                                        }
                                    }
                                });



                            }
                        });




                        // console.log(str)
                    }



                )
                .change();


















        });






    </script>


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>




@endsection