@extends('admin.base')

@section('content')



    <!-- Row -->
    <div class="row">
        <div class="col-xl-12 pa-0">
            @include('admin.pages.product.menu', ['prs' => $prs])
            <div class="tab-content">
                <div class="tab-pane fade show active" role="tabpanel">

                    <div class="row">


                        <div class="col-xl-12">
                            <section class="hk-sec-wrapper" style="">


                                <!--begin: Search Form -->

                                <a class="btn btn-gradient-success" href="{{route('admin.product.variant.create.view',$prs->id)}}">
                                    Create Variant</a>


                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-sm">
                                        <form class="needs-validation" method="post"
                                              enctype="multipart/form-data"
                                              action="{{route('admin.product.add.filter.save')}}">


                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-3 mb-10">
                                                    <label for="validationCustom01">Select Filter</label>
.
                                                    <input type="text" readonly class="form-control" value="{{$filter->title}}">



                                                </div>


                                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                                <input type="hidden" name="filter_type_id" value="{{$filter->id}}">

                                                <div class="col-md-3 mb-10">
                                                    <label for="validationCustom01">Select Filter Values</label>

                                                    <select class="form-control" name="filter">

                                                        @foreach ($filtervalues as $filtervalue)
                                                            <option value="{{$filtervalue->id}}">{{$filtervalue->value}}</option>
                                                        @endforeach


                                                    </select>



                                                </div>


                                            </div>

























                                            <div class="form-group">
                                                <div class="form-check custom-control custom-checkbox">
                                                    <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                                    <label class="form-check-label custom-control-label" for="invalidCheck">
                                                        Agree to terms and conditions
                                                    </label>
                                                    <div class="invalid-feedback">
                                                        You must agree before submitting.
                                                    </div>
                                                </div>
                                            </div>


                                            <button class="btn btn-primary registercustomer"  type="submit">Save</button>
                                        </form>
                                    </div>
                                </div>



                            </section>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('footer')




    <script src="{{asset('dist/js/tooltip-data.js')}}"></script>


@endsection