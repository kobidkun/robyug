<div class="profile-cover-wrap overlay-wrap" >
    <div class="profile-cover-img" style="background-image:url('http://multimarktix.com/img/profile/bba5cf5acb1e03045d81555821b986c7461ca64c.jpg')"></div>
    <div class="bg-overlay bg-trans-dark-60"></div>
    <div class="container profile-cover-content py-50">
        <div class="hk-row">
            <div class="col-lg-6">
                <div class="media align-items-center">
                    <div class="media-img-wrap  d-flex">
                        <div class="">



                            <img src="data:image/png;base64,
                                                 {{ base64_encode(QrCode::format('png')
                                            ->merge('/public/common/images/fabicon.png', .5)
                                            ->size(200)
                                            ->margin(1)
                                            ->errorCorrection('H')
                                            ->generate($prs->secure_id))  }} ">





                            <p style="color: white">Scan with Inventory App

                                <button class="btn btn-icon btn-primary btn-icon-style-1"

                                        data-toggle="tooltip-info" data-placement="top"

                                        title="Available on Ios & Android on demand"
                                >
                                    <span class="btn-icon-wrap"><i style="color: white" class="fa fa-info"></i></span></button>


                            </p>

                        </div>
                    </div>
                    <div class="media-body">
                        <div class="text-white text-capitalize display-6 mb-5 font-weight-400">{{$prs->title}}</div>
                        <div class="font-14 text-white"><span class="mr-5">
                                                <span class="font-weight-500 pr-5">Code</span><span class="mr-5">{{$prs->hsn}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">In Stock</span><span class="mr-5">{{$prs->stock}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5">Secure Code</span><span class="mr-5">{{$prs->secure_id	}}</span></span><span> <br>
                                                <span class="font-weight-500 pr-5"> Code</span><span class="mr-5">{{$prs->code_name}}</span></span><span> <br>

                                            </span>
                        </div>



                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<div class="bg-white shadow-bottom">
    <div class="container">
        <ul class="nav nav-light nav-tabs" role="tablist">
            <li class="nav-item">
                <a href="{{route('admin.product.details',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link ">Variants</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.product.edit.details',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Edit</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.product.create.gallery',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Gallery</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.product.add.filter',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Filters</a>
            </li>

            <li class="nav-item">
                <a href="{{route('admin.product.add.group',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Primary Groups</a>
            </li>
            <li class="nav-item">
                <a href="{{route('admin.product.add.group.secondary',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Secondary Groups</a>
            </li>

            <li class="nav-item">
                <a href="{{route('admin.product.update.image',$prs->id)}}"
                   class="d-flex h-60p align-items-center nav-link">Update Image</a>
            </li>
        </ul>
    </div>
</div>