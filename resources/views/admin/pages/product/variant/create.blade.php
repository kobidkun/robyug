@extends('admin.base')

@section('content')
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Create Product Variant</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon"><span class="feather-icon"><i data-feather="external-link"></i></span></span>
                Create Product</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Create Product</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" method="post"
                                  enctype="multipart/form-data"
                                  action="{{route('admin.product.variant.create.save')}}">


                                @csrf
                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">Title</label>
                                        <input type="text" class="form-control product-name"
                                               placeholder="Title"  name="title"
                                               value="{{$product->title}}"
                                               required>

                                    </div>



                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom02">meta</label>
                                        <input type="text" class="form-control product-meta"
                                               placeholder="meta"
                                               name="meta"
                                               value="{{$product->meta}}"
                                               required>


                                    </div>

                                    <input type="hidden" name="product_id" value="{{$product->id}}">





                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">Slug</label>
                                        <input
                                                type="text"
                                                class="form-control product-slug"
                                                placeholder="Slug"
                                                name="slug"
                                                autocomplete="off"
                                                required  value="{{$product->slug}}"
                                                readonly
                                        >



                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">hsn</label>
                                        <input
                                                type="text"
                                                class="form-control "
                                                placeholder="hsn"
                                                name="hsn"
                                                autocomplete="off"
                                                  value="{{$product->hsn}}"

                                        >



                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">code name</label>
                                        <input
                                                type="text"
                                                class="form-control "
                                                placeholder="Slug"
                                                name="code_name"
                                                autocomplete="off"
                                                  value="{{$product->code_name}}"

                                        >



                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">type</label>
                                        <select class="form-control" name="type"  >
                                            <option value="goods">Goods</option>
                                            <option value="services">Services</option>
                                        </select>



                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">unit</label>
                                        <input
                                                type="text"
                                                class="form-control "
                                                placeholder="unit"
                                                name="unit"
                                                autocomplete="off"
                                                  value="{{$product->unit}}"

                                        >



                                    </div>






                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">featured</label>
                                        <select class="form-control" name="featured"  >
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>



                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">active</label>
                                        <select class="form-control" name="active"  >
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>



                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <input type="hidden" class="primarycatname" name="primary_group" value="{{$pgs[0]->title}}">
                                        <label for="validationCustom03">Primary Group</label>
                                        <select class="form-control primcat" name="primary_group_id"  >

                                            <option value="{{$product->primary_group_id}}">{{$product->primary_group}}</option>

                                            @foreach ($pgs as $pg)
                                                <option value="{{$pg->id}}">{{$pg->title}}</option>
                                            @endforeach



                                        </select>



                                    </div>

                                    <div class="col-md-3 mb-10">

                                        <input type="hidden" class="secondarycatname" name="secondary_group" value="{{$sgs[0]->title}}">

                                        <label for="validationCustom03">Primary Group</label>
                                        <select class="form-control seccat" name="secondary_group_id"  >

                                            <option value="{{$product->secondary_group_id}}">{{$product->secondary_group}}</option>

                                            @foreach ($sgs as $sg)
                                                <option value="{{$sg->id}}">{{$sg->title}}</option>
                                            @endforeach



                                        </select>



                                    </div>


















                                </div>

                                <br>


                                <h5 class="hk-sec-title">Variant Details</h5>
                                <div class="form-row">
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">variant type</label>
                                        <input
                                                type="text"
                                                class="form-control "
                                                placeholder="variant type"
                                                name="variant_type"
                                                autocomplete="off"


                                        >



                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom03">variant value</label>
                                        <input
                                                type="text"
                                                class="form-control "
                                                placeholder="variant value"
                                                name="variant_value"
                                                autocomplete="off"


                                        >



                                    </div>
                                </div>



                                <h5 class="hk-sec-title">Prices</h5>
                                <div class="form-row">





                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">value</label>
                                        <input type="text" class="form-control product-base-price"
                                               placeholder="value"    value="{{$product->value}}"  name="value" >

                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">discount</label>
                                        <input type="text" class="form-control product-discount "
                                               placeholder="discount"  value="{{$product->discount}}"
                                               name="discount" >

                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">discount_per</label>
                                        <input type="text" class="form-control product-discount-percentage"
                                               placeholder="discount_per"   value="{{$product->discount_per}}"
                                               name="discount_per" >

                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">after_discount_amt</label>
                                        <input type="text" class="form-control product-after-discount"
                                               placeholder="after_discount_amt"   value="{{$product->after_discount_amt}}"
                                               name="after_discount_amt" >

                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">taxper</label>

                                        <select   name="taxper"
                                                  class="form-control product-tax-percentage">
                                            <option value="{{$product->taxper}}">{{$product->taxper}}%</option>
                                            <option value="0">0%</option>
                                            <option value="5">5%</option>
                                            <option value="12">12%</option>
                                            <option value="18">18%</option>
                                            <option value="28">28%</option>

                                        </select>





                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">cgst</label>
                                        <input type="text" class="form-control product-cgst"
                                               placeholder="cgst"   value="{{$product->cgst}}"   name="cgst" >

                                    </div>
                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">sgst</label>
                                        <input type="text" class="form-control product-sgst"
                                               placeholder="sgst"   value="{{$product->sgst}}"   name="sgst" >

                                    </div>

                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">igst</label>
                                        <input type="text" class="form-control product-igst"
                                               placeholder="igst"   value="{{$product->igst}}"   name="igst" >

                                    </div>




                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">tax_amt</label>
                                        <input type="text" class="form-control product-tax-amt"
                                               placeholder="tax_amt"   value="{{$product->tax_amt}}"   name="tax_amt" >

                                    </div>



                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">total</label>
                                        <input type="text" class="form-control product-total-amt"
                                               placeholder="total"   value="{{$product->total}}"   name="total" >

                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">Large Image</label>
                                        <input type="file" class="form-control"
                                               name="img_hd"    >

                                    </div>


                                    <div class="col-md-3 mb-10">
                                        <label for="validationCustom01">Small Image</label>
                                        <input type="file" class="form-control"
                                               name="img_small"    >

                                    </div>







                                </div>







                                <div class="form-group">

                                    <div class="col-md-12 mb-10">

                                        <textarea style="height: 350px" name="description"  class="form-control my-editor">
                                            {!! $product->description !!}
                                        </textarea>

                                    </div>

                                </div>








                                <div class="form-group">
                                    <div class="form-check custom-control custom-checkbox">
                                        <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                        <label class="form-check-label custom-control-label" for="invalidCheck">
                                            Agree to terms and conditions
                                        </label>
                                        <div class="invalid-feedback">
                                            You must agree before submitting.
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-primary registercustomer"  type="submit">Save </button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')




    <script>



        $(document).ready(function(){
            $(".product-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".product-slug").val(actualSlug);
                $(".product-meta").val(cat_name_val);



            });

            $(".product-base-price").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".product-discount").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".product-tax-percentage").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }));


            $(".primcat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                updatehiddenpc();
            }));



            $(".seccat").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                updatehiddensc();
            }));

            function updatehiddenpc() {

                var pccnam =  $(".primcat option:selected").text();

                $(".primarycatname").val(pccnam);

            }

            function updatehiddensc() {

                var pccnam =  $(".seccat option:selected").text();

                $(".secondarycatname").val(pccnam);

            }








            //    var CGST =  $(".product-cgst").val();
            //    var SGST =  $(".product-sgst").val();
            //   var IGST =  $(".product-igst").val();
            // var TOTAL =  $(".product-total").val();
            //   var TOTALTAX =  $(".product-tax-amt").val();



            function onPriceupdate() {
                var BASEPRICE =  $(".product-base-price").val();
                var TAXPERCENTAGE =  $(".product-tax-percentage option:selected").val();
                var DISCOUNT =  $(".product-discount").val();



                //calcul;ate actual price

                var AFTERDISC = BASEPRICE - DISCOUNT ;

                var DISC_PERCENTAGE = (DISCOUNT/BASEPRICE )*100

                $(".product-after-discount").val(AFTERDISC.toFixed(2));
                $(".product-discount-percentage").val(DISC_PERCENTAGE.toFixed(2));


                //cal gst
                //
                //
                //

                var TocalculatePrice =  $(".product-after-discount").val();


                var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                $(".product-cgst").val(CGST.toFixed(2));
                $(".product-sgst").val(SGST.toFixed(2));
                $(".product-igst").val(IGST.toFixed(2));
                $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".product-total-amt").val((Number(TocalculatePrice) + Number(TOTALTAX)).toFixed(2));




                //  $(".product-after-discount").val(AFTERDISC);

                //  console.log(BASEPRICE);









            }





            /// cal change


            $( ".m_selectpicker" )
                .change(


                    function () {
                        // preventDefault();
                        var abr = '<option value="">==select Primary categories==</option>';
                        $("#secondarycatplaceholder").html(abr);


                        var str = $( "#primarycatplaceholder option:selected" ).val();


                        // console.log(str)
                        $.ajax({
                            type: "GET",
                            url: "/admin/category/sec/searchforproducy/"+str,

                            success: function( resp) {
                                //   resp.preventDefault();
                                // $("#msg").prepend(resp);
                                console.log(resp.cat2.length);
                                //  var str = '<option value="0">Select Secondary Category</option>';
                                if (resp.cat2.length > 0 ) {
                                    $.each(resp.cat2, function (index, value) {
                                        str = str+'<option  value="'+value.id+'">'+value.name+'</option>';
                                    });
                                    $("#secondarycatplaceholder").html(str);
                                } else {
                                    //  var abr = '';
                                    $("#secondarycatplaceholder").html('<option value="0">No Secondary categories</option>');
                                }

                                //turtiary

                                var abr2 = '<option value="">==select Secondary categories==</option>';
                                $("#turtiaryycatplaceholder").html(abr2);
                                var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                                $.ajax({
                                    type: "GET",
                                    url: "/admin/category/tur/searchforproducy/"+str2,
                                    success: function( resp2) {
                                        console.log(resp2.cat3.length);
                                        if (resp2.cat3.length > 0 ) {
                                            $.each(resp2.cat3, function (index, value) {
                                                str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                            });
                                            $("#turtiaryycatplaceholder").html(str2);
                                        } else {
                                            $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                                        }
                                    }
                                });



                            }
                        });




                        // console.log(str)
                    }



                )
                .change();


















        });






    </script>


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>




@endsection