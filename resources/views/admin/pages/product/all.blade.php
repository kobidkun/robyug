@extends('admin.base')

@section('content')


    <link href="{{asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page"> Products</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">
                <span class="pg-title-icon">
                    <span class="feather-icon"><i data-feather="external-link"></i></span></span>
                 Products</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">

                    <div class="row">
                        <div class="col-sm">

                            <table class="table table-striped table-bordered" id="user-table" style="width: 100%">
                                <thead class="thead-dark">
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>HSN</th>
                                    <th>GROUP</th>
                                    <th>GROUP</th>
                                    <th>CODE</th>
                                    <th>DISC %</th>
                                    <th>TAX</th>
                                    <th>TOTAL</th>
                                    <th style="width: 150px">Featured</th>
                                    <th style="width: 150px">Active</th>
                                    <th>ACTION</th>



                                </tr>
                                </thead>
                            </table>





                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection


@section('footer')

    <!-- Data Table JavaScript -->
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('dist/js/dataTables-data.js')}}"></script>
    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: '{{route('admin.product.datatable.api')}}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'hsn', name: 'hsn' },
                    { data: 'primary_group', name: 'primary_group' },
                    { data: 'secondary_group', name: 'secondary_group' },
                    { data: 'code_name', name: 'code_name' },
                    { data: 'discount_per', name: 'discount_per' },
                    { data: 'tax_amt', name: 'tax_amt' },
                    { data: 'total', name: 'total' },
                    {data: 'featuredtwp', name: 'featuredtwp', orderable: false, searchable: false},
                    {data: 'active', name: 'active', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>





@endsection