<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">



                    <li class="nav-item">




                        <a class="nav-link " href="javascript:void(0);" data-toggle="collapse" data-target="#app_drp">
                            <span class="feather-icon"><i data-feather="hard-drive"></i></span>
                            <span class="nav-link-text">Master</span>

                        </a>


                        <ul id="app_drp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.group.primary.all')}}">Primary Group</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.group.secondary.all')}}">Secondary Group</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.filter.all')}}">Filter</a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link " href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_tree">
                            <span class="feather-icon"><i data-feather="box"></i></span>
                            <span class="nav-link-text">Product </span>

                        </a>
                        <ul id="app_tree" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.product.create.view')}}">Create</a>
                                    </li>
<li class="nav-item">
                                        <a class="nav-link" href="{{route('admin.product')}}">Product List</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>



                    <li class="nav-item">
                        <a class="nav-link " href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_temp">
                            <span class="feather-icon"><i data-feather="shopping-cart"></i></span>
                            <span class="nav-link-text">Orders</span>

                        </a>
                        <ul id="app_temp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="">Placed Orders</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>

                <li class="nav-item">
                        <a class="nav-link " href="javascript:void(0);"

                           data-toggle="collapse" data-target="#app_cust">
                            <span class="feather-icon"><i data-feather="users"></i></span>
                            <span class="nav-link-text">Customer</span>

                        </a>
                        <ul id="app_cust" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link" href="">Manage Customer</a>
                                    </li>


                                </ul>
                            </li>
                        </ul>
                    </li>










              {{--  <li class="nav-item">
                    <a class="nav-link link-with-badge" href="javascript:void(0);" data-toggle="collapse" data-target="#app_drp">
                        <span class="feather-icon"><i data-feather="package"></i></span>
                        <span class="nav-link-text">Application</span>
                        <span class="badge badge-primary badge-pill">4</span>
                    </a>
                    <ul id="app_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="chats.html">Chat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="calendar.html">Calendar</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="email.html">Email</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="file-manager.html">File Manager</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#auth_drp">
                        <span class="feather-icon"><i data-feather="zap"></i></span>
                        <span class="nav-link-text">Authentication</span>
                    </a>
                    <ul id="auth_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#signup_drp">
                                        Sign Up
                                    </a>
                                    <ul id="signup_drp" class="nav flex-column collapse collapse-level-2">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup.html">Cover</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="signup-simple.html">Simple</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#recover_drp">
                                        Recover Password
                                    </a>
                                    <ul id="recover_drp" class="nav flex-column collapse collapse-level-2">
                                        <li class="nav-item">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link" href="forgot-password.html">Forgot Password</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="reset-password.html">Reset Password</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="lock-screen.html">Lock Screen</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="404.html">Error 404</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="maintenance.html">Maintenance</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>--}}

            </ul>
           {{-- <hr class="nav-separator">
            <div class="nav-header">
                <span>User Interface</span>
                <span>UI</span>
            </div>
            <ul class="navbar-nav flex-column">



                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#forms_drp">
                        <span class="feather-icon"><i data-feather="server"></i></span>
                        <span class="nav-link-text">Forms</span>
                    </a>
                    <ul id="forms_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.forms')}}">Form Layout</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.form')}}">Form Validation</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.form_upload')}}">File Upload</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#tables_drp">
                        <span class="feather-icon"><i data-feather="list"></i></span>
                        <span class="nav-link-text">Tables</span>
                    </a>
                    <ul id="tables_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.basictable')}}">Basic Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.datatable')}}">Data Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.responsivetable')}}">Responsive Table</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('frontend.editabletable')}}">Editable Table</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>


            </ul>
            <hr class="nav-separator">
            <div class="nav-header">
                <span>Getting Started</span>
                <span>GS</span>
            </div>
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="documentation.html" >
                        <span class="feather-icon"><i data-feather="book"></i></span>
                        <span class="nav-link-text">Documentation</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="#">
                        <span class="feather-icon"><i data-feather="eye"></i></span>
                        <span class="nav-link-text">Changelog</span>
                        <span class="badge badge-sm badge-danger badge-pill">v 1.0</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span class="feather-icon"><i data-feather="headphones"></i></span>
                        <span class="nav-link-text">Support</span>
                    </a>
                </li>




            </ul>--}}




        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!-- /Vertical Nav -->