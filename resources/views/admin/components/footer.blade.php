<!-- Footer -->
<div class="hk-footer-wrap container">
    <footer class="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p>Developed by<a href="https://tecions.com/" class="text-dark" target="_blank">Tecions</a> © 2019</p>
            </div>

        </div>
    </footer>
</div>
<!-- /Footer -->