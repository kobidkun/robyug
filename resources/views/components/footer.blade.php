<footer>

    <div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <!-- ABOUT Location -->
            <div class="col-md-4">
                <div class="about-footer"><p style="color: white;">Logo</p> <img class="margin-bottom-30" src="images/logo-foot.png" alt="">
                    <p></i> Street No. 12, Newyork 12, <br>
                        MD - 123, USA.</p>
                    <p><i class="fa fa-phone"></i> 1.800.123.456789 <br>
                        1.800.123.456789</p>
                    <p><i class="fa fa-envelope"></i> info@BoShop.com <br>
                        contact@BoShop.com</p>
                </div>
            </div>

            <!-- HELPFUL LINKS -->
            <div class="col-md-5">
                <h6>Links</h6>
                <ul class="link two-half">
                    <li><a href="#."> Products</a></li>
                    <li><a href="#."> Find a Store</a></li>
                    <li><a href="#."> Features</a></li>
                    <li><a href="#."> Privacy Policy</a></li>
                    <li><a href="#."> Blog</a></li>
                    <li><a href="#."> Press Kit </a></li>
                    <li><a href="#."> Products</a></li>
                    <li><a href="#."> Find a Store</a></li>
                    <li><a href="#."> Features</a></li>
                    <li><a href="#."> Privacy Policy</a></li>
                    <li><a href="#."> Blog</a></li>
                    <li><a href="#."> Press Kit </a></li>
                </ul>
            </div>

            <!-- HELPFUL LINKS -->
            <div class="col-md-3">
                <h6>Account Info</h6>
                <ul class="link">
                    <li><a href="#."> Products</a></li>
                    <li><a href="#."> Find a Store</a></li>
                    <li><a href="#."> Features</a></li>
                    <li><a href="#."> Privacy Policy</a></li>
                    <li><a href="#."> Blog</a></li>
                    <li><a href="#."> Press Kit </a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Rights -->
    <div class="rights">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>©  2019 Roboyug All right reserved. <a href="https://tecions.com/">Tecions</a></p>
                </div>
                <div class="col-md-6 text-right"> <img src="images/card-icon.png" alt=""> </div>
            </div>
        </div>
    </div>
</footer>
