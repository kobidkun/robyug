<link href="css/slider.css" rel="stylesheet">

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="mask flex-center">
                <div class="container">

                    <img src="/images/1.jpg" alt="" width="100%">
                    
                    
                    
                </div>
            </div>
        </div>


        <div class="carousel-item">
            <div class="mask flex-center">

                    <div class="container">

                        <img src="/images/2.jpg" alt="" width="100%">



                    </div>

            </div>
        </div>
        <div class="carousel-item">
            <div class="mask flex-center">
                <div class="container">
                    <div class="row align-items-center">
                        <img src="/images/3.jpg" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
<!--slide end-->
<script type="text/javascript">
    $('#myCarousel').carousel({
        interval: 3000,
    })
</script>
