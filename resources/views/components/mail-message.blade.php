@extends('parts.base')
@section('content')
    <link href="css/bootstrap2.min.css" rel="stylesheet" id="bootstrap-css">

 <style>
     .wrapper{width:60%; margin:5% auto;height:100vh;  box-shadow:0 0 2px #aaa; font-family:Hind;}
     .logo_header{width:100%; height:70px;background:#1CA8DD; padding:10px;}
     .email_body{width:100%; padding:0 15px;}
     .receipt_list{width:100%;}
     .receipt_list .left_list{float:left; width:60%;}
     .receipt_list .right_list{float:left; width:40%;}
     .left_list b,.right_list b{width:100%; float:left; margin:0 0 10px 0;}
     .left_list span,.right_list span{width:100%; float:left; margin:0 0 5px 0;}
     .right_list span{text-align:left; padding-left:15%;}
     .list_divider{width:100%; border-top:1px solid rgba(0,0,0,0.2);float:left;}
     .invoice_trans{width:100%;float:left; margin:5px 0;}
     .invoice_left{float:left; width:60%;}
     .invoice_right{float:left; width:40%;}
     .span {color: #00b1f1;}
 </style>
    <link href="https://fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <section class="sub-bnr" data-stellar-background-ratio="0.5" style="background-position: 0% -70.5px;">
        <div class="position-center-center">
            <div class="container">
                <h4>Mail Message</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Mail message</li>
                </ol>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">

            <div class="wrapper">
                <div class="logo_header">
                    <a href="#"><img src="{{asset('assets/images/logo5.jpg')}}" width="250px;" height="80px;"/></a>
                </div>
                <div class="email_banner">

                </div>
                <div class="email_body">
                    <h1 class="text-center">Order confirmed!!</h1>
                    <p>
                        Dear <u style="color: #00b1f1;">utpal@gmail.com</u>
                    </p>
                    <p>
                        Greetings from roboyug,
                    </p>
                    <p>We thank you for your order. This email contains your order summary. When the items in your order are shipped, you will receive an email with the Courier Tracking ID and the link where you can track your order.</p>
                    <p>Please find below the summary of your </p><h5>order id: #1234000H at roboyug.</h5>
                    <hr>
                    <p>Order id: <b style="color: darkred">#1234000H</b> | Seller:<b style="color: darkred"> Amit paul</b></p>
                    <p>Estimated delivery date: <b style="color: darkred">2/4/2019</b></p>
                    <p</p>
                    <p style="color: #0b0b0b">items ordered</p>
                    <hr>
                    <div class="receipt_list">
                        <div class="left_list">
                            <b>product details</b>


                            <span class="span">Microsoft desktop 2010 wireless mouse and keyboard kombo. </span>
                            <span class="span">Microsoft desktop 2015 wireless mouse and keyboard kombo.</span>
                            <span class="span">Microsoft desktop 2000 wireless mouse and keyboard kombo.</span>
                            <span class="span">interactive healthcare services</span>
                            <span class="span">interactive healthcare services</span>
                            <span class="span">interactive healthcare services</span>
                            <span>Shipping charge</span>

                        </div>


                        <div  class="right_list">
                            <b>product price</b>
                            <span>1000.90</span>
                            <span>1000.90</span>
                            <span>1000.90</span>
                            <span>1000.90</span>
                            <span>1000.90</span>
                            <span>1000.90</span>
                            <span>Free</span>

                        </div>
                        <span class="list_divider"></span>
                        <div class="left_list">
                            <b>Total</b>
                        </div>
                        <div class="right_list">
                            <span><b>10000.99</b></span>
                        </div>
                    </div>

                    <div class="invoice_trans">
                        <div class="invoice_left">
                            <p>Shipping address:</p>
                        </div>
                        <div class="invoice_right">
                            <p>Babu para, lane 20, siliguri, wb, 734006</p>
                        </div>
                    </div>
                    <div class="invoice_trans">
                        <div class="invoice_left">
                            <p></p>
                        </div>
                        <div class="invoice_right">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
