<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/icons.css" rel="stylesheet">
<!-- Wrap -->
    <div class="top-bar">
    <div class="container-full">
        <p><i class="fa fa-envelope" aria-hidden="true"></i>
            {{env('EMAIL')}} </p>
        <p class="call"><i class="fa fa-phone"></i> {{env('PhONE')}} </p>

        <!-- Login Info -->
        <div class="login-info">
            <ul>
                <li><a href=""><i class='fa fa-user'></i>
                        LOGIN</a></li>
                {{--<li><a href="#."> MY ACCOUNT </a></li>--}}
                {{--<li><a href="shopping-cart.html">MY CART</a></li>--}}

                <!-- USER BASKET -->
                <li class="dropdown user-basket"> <a href="{{route('components.cart')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Cart <i class='fa fa-shopping-cart' style='color:white'></i>
                    </a>
                    <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(15px, 50px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <li>
                            <div class="media-left">
                                <div class="cart-img"> <a href="/"> <img class="media-object img-responsive" src="{{asset('/common/images/logo.png')}}"
                                                                         alt="{{env('APP_NAME')}}"> </a>
                                </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading">Rise Skinny Jeans</h6>
                                <span class="price">129.00 USD</span> <span class="qty">QTY: 01</span> </div>
                        </li>
                        <li>
                            <div class="media-left">
                                <div class="cart-img"> <a href="#"> <img class="media-object img-responsive" src="{{asset('assets/images/1.jpg')}}" alt="..."> </a> </div>
                            </div>
                            <div class="media-body">
                                <h6 class="media-heading">Mid Rise Skinny Jeans</h6>
                                <span class="price">129.00 USD</span> <span class="qty">QTY: 01</span> </div>
                        </li>
                        <li>
                            <h5 class="text-left">SUBTOTAL: <small> 258.00 USD </small></h5>
                        </li>
                        <li class="margin-0">
                            <div class="row">
                                <div class="col-sm-6"> <a href="{{route('components.cart')}}" class="btn">VIEW CART</a></div>
                                <div class="col-sm-6 "> <a href="{{route('components.checkout')}}" class="btn">CHECK OUT</a></div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="wrap">

    <!-- header -->
    <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 91px;"><header class="sticky" style="">
            <div class="container">

                <!-- Logo -->
                <div class="logo"> <a href="{{route('components.welcome')}}">
                        <img class="img-responsive"
                             src="{{asset('/common/images/logo.png')}}"
                             width="200px;" height="70px;" alt=""></a>
                </div>

                <nav class="navbar ownmenu navbar-expand-lg">
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <span></span> </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="nav">
                            <li class="dropdown active"> <a href="{{route('components.welcome')}}" class="dropdown-toggle" data-toggle="dropdown">Home</a>

                            </li>
                            <li class="dropdown"> <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Pages</a>
                                <ul class="dropdown-menu">
                                    <li> <a href="shop_02.html">Shop Full </a> </li>
                                    <li> <a href="shop_03.html">Shop Shop </a> </li>
                                    <li> <a href="shop_04.html">Shop Creative  <span class="sm-tag">Hot</span></a> </li>
                                    <li class="dropdown"> <a href="shop_01.html">Shop </a>
                                        <ul class="dropdown-menu animated-3s fadeInRightSm">
                                            <li> <a href="shop_6_col.html">Shop 06 </a> </li>
                                            <li> <a href="shop_5_col.html">Shop 05</a> </li>
                                            <li> <a href="shop_4_col.html">Shop 04  </a> </li>
                                            <li> <a href="shop_3_col.html">Shop 03  </a> </li>
                                        </ul>
                                    </li>
                                    <li> <a href="product-detail_01.html">Products Detail 01</a> </li>
                                    <li> <a href="product-detail_02.html">Products Detail 02</a> </li>
                                    <li> <a href="product-detail_03.html">Products Detail 03</a> </li>
                                    <li> <a href="shopping-cart.html">Shopping Cart</a> </li>
                                    <li> <a href="checkout.html">Checkout</a> </li>
                                    <li> <a href="{{route('components.about-us')}}">About Us</a> </li>
                                    <li> <a href="contact.html">Contact</a> </li>
                                    <li> <a href="blog-list_01.html">Blog List 01</a> </li>
                                    <li> <a href="blog-list_02.html">Blog List 02</a> </li>
                                    <li> <a href="blog-list_03.html">Blog List 03 </a> </li>
                                    <li> <a href="blog-detail_01.html">Blog Detail 01 </a> </li>
                                </ul>
                            </li>
                            <li> <a href="{{route('components.about-us')}}">About </a> </li>
                            <li> <a href="{{route('components.shop')}}">Shop </a> </li>
                            <li> <a href="{{route('components.blog')}}">Blog </a> </li>



                            <!-- Two Link Option -->
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collection</a>
                                <div class="dropdown-menu two-option">
                                    <div class="row">
                                        <ul class="col-sm-6">
                                            <li> <a href="index-2.html"> Summer Store</a></li>
                                            <li> <a href="index-2.html"> Sarees</a></li>
                                            <li> <a href="index-2.html"> Kurtas</a></li>
                                            <li> <a href="index-2.html"> Shorts &amp; tshirts</a></li>
                                            <li> <a href="index-2.html"> Winter wear</a></li>
                                            <li> <a href="index-2.html"> Jeans</a></li>
                                            <li> <a href="index-2.html"> Bra</a></li>
                                            <li> <a href="index-2.html"> Babydools</a> </li>
                                        </ul>
                                        <ul class="col-sm-6">
                                            <li> <a href="index-2.html"> Deodornts</a></li>
                                            <li> <a href="index-2.html"> Skin care</a></li>
                                            <li> <a href="index-2.html"> Make up</a></li>
                                            <li> <a href="index-2.html"> Watch</a></li>
                                            <li> <a href="index-2.html"> Siting bags</a></li>
                                            <li> <a href="index-2.html"> Totes</a></li>
                                            <li> <a href="index-2.html"> Gold rings</a></li>
                                            <li> <a href="index-2.html"> Jewellery</a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>

                            <!-- MEGA MENU -->
                            <li class="dropdown megamenu"> <a href="#." class="dropdown-toggle" data-toggle="dropdown">Mega Store</a>
                                <div class="dropdown-menu">
                                    <div class="row">

                                        <!-- Shop Pages -->
                                        <div class="col-md-5">
                                            <h6>Shop Pages</h6>
                                            <ul class="col-2-li">
                                                <li> <a href="shop_02.html">Shop Full </a> </li>
                                                <li> <a href="shop_03.html">Shop Shop  </a> </li>
                                                <li> <a href="shop_04.html">Shop Creative </a> </li>
                                                <li> <a href="shop_01.html">Shop </a></li>
                                                <li> <a href="shop_6_col.html">Shop 06 Col </a> </li>
                                                <li> <a href="shop_5_col.html">Shop 05 Col </a> </li>
                                                <li> <a href="shop_4_col.html">Shop 04 Col </a> </li>
                                                <li> <a href="shop_3_col.html">Shop 03 Col </a> </li>
                                                <li> <a href="product-detail_01.html">Products Detail 01</a> </li>
                                                <li> <a href="product-detail_02.html">Products Detail 02</a> </li>
                                                <li> <a href="product-detail_03.html">Products Detail 03</a> </li>
                                                <li> <a href="shopping-cart.html">Shopping Cart</a> </li>
                                                <li> <a href="checkout.html">Checkout</a> </li>
                                                <li> <a href="about-us_01.html">About Us</a> </li>
                                                <li> <a href="contact.html">Contact</a> </li>
                                                <li> <a href="blog-list_01.html">Blog List 01</a> </li>
                                                <li> <a href="blog-detail_01.html">Blog Detail 01 </a> </li>
                                            </ul>
                                        </div>

                                        <!-- Shop Pages -->
                                        <div class="col-md-3">
                                            <h6>Latest items</h6>
                                            <ul>
                                                <li> <a href="index-2.html"> Deodornts</a></li>
                                                <li> <a href="index-2.html"> Skin care</a></li>
                                                <li> <a href="index-2.html"> Make up</a></li>
                                                <li> <a href="index-2.html"> Watch</a></li>
                                                <li> <a href="index-2.html"> Siting bags</a></li>
                                                <li> <a href="index-2.html"> Totes</a></li>
                                                <li> <a href="index-2.html"> Gold rings</a></li>
                                                <li> <a href="index-2.html"> Jewellery</a> </li>
                                            </ul>
                                        </div>

                                        <!-- Top Rate -->
                                        <div class="col-md-4">
                                            <h6>Top Rate Products</h6>
                                            <div class="top-rated">
                                                <ul>
                                                    <li>
                                                        <div class="media-left">
                                                            <div class="cart-img"> <a href="#"> <img class="media-object img-responsive" src="images/cart-img-1.jpg" alt="..."> </a> </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Best T-Shirt Design</h6>
                                                            <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                                            <span class="price">129.00 USD</span> </div>
                                                    </li>
                                                    <li>
                                                        <div class="media-left">
                                                            <div class="cart-img"> <a href="#"> <img class="media-object img-responsive" src="images/cart-img-2.jpg" alt="..."> </a> </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Bag Pack for Child</h6>
                                                            <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                                            <span class="price">129.00 USD</span> </div>
                                                    </li>
                                                    <li>
                                                        <div class="media-left">
                                                            <div class="cart-img"> <a href="#"> <img class="media-object img-responsive" src="images/cart-img-3.jpg" alt="..."> </a> </div>
                                                        </div>
                                                        <div class="media-body">
                                                            <h6 class="media-heading">Bag Pack for Child</h6>
                                                            <div class="stars"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                                            <span class="price">129.00 USD</span> </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- MEga Menu Elements -->
                                    <div class="mega-menu-elements"> <img src="images/nav-img.png" alt=""> <a href="#." class="btn btn-inverse">Shop Now</a> </div>
                                </div>
                            </li>
                            <li> <a href="{{route('components.contact')}}"> contact</a> </li>
                            <a href="{{route('components.wishlist')}}"<i class="fa fa-heart-o" style="font-size:28px;color:red"></i></a>

                        </ul>
                    </div>

                    <!-- Nav Right -->

                </nav>
            </div>
            <div class="clearfix"></div>
        </header></div>
</div>

