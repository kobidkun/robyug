<section class="small-about">
    <div class="">
        <div class="news-letter padding-top-150 padding-bottom-150">
            <div class="row">
                <div class="col-lg-6">
                    <h3>We always stay with our clients and respect their business. We deliver 100% and provide instant response to help them succeed in constantly changing and challenging business world. </h3>
                    <ul class="social_icons">
                        <li><a href="#."><i class="fa fa-facebook-f" style="color:white;"></i>
                            </a></li>
                        <li><a href="#."><i class="fa fa-twitter" style="color:white;"></i>
                            </a></li>
                        <li><a href="#."><i class="fa fa-youtube-play" style="color:white;"></i>
                            </a></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <h3>Subscribe Our Newsletter</h3>
                    <span>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac.</span>
                    <form>
                        <input type="email" placeholder="Enter your email address" required="">
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
