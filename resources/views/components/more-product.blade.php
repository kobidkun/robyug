<section class="dark-bg padding-top-100 padding-bottom-100">
    <div class="container-full">

        <!-- Main Heading -->
        <div class="heading text-center">
            <h4>Popular Products</h4>
            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus maximus vehicula.
          Sed feugiat, tellus vel tristique posuere, diam</span> </div>

        <!-- Popular Item Slide -->
        <div class="papular-block block-slide owl-carousel owl-theme owl-loaded">

            <!-- Item -->


            <!-- Item -->


            <!-- Item -->


            <!-- Item -->

            <!-- Item -->


            <!-- Item -->


            <!-- Item -->


            <!-- Item -->

            <div class="owl-stage"><div class="owl-stage" style="transform: translate3d(-1633px, 0px, 0px); transition: all 0s ease 0s; width: 5443.34px;"><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Sale -->
                            <div class="on-sale"> Sale </div>
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Sale -->
                            <div class="on-sale"> Sale </div>
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item active" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Sale -->
                            <div class="on-sale"> Sale </div>
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>

                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div><div class="owl-item cloned" style="width: 242.167px; margin-right: 30px;"><div class="item">
                            <!-- Item img -->
                            <div class="item-img"> <img class="img-1" src="assets/images/11.png" alt=""> <img class="img-2" src="assets/images/11.png" alt="">
                                <!-- Overlay -->
                                <div class="overlay">
                                    <div class="position-bottom">
                                        <div class="inn"><a href="assets/images/11.png" data-lighter=""><i class="fa fa-search-plus"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="fa fa-shopping-cart"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="fa fa-heart-o"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="#.">Mid Rise Skinny Jeans </a>
                                <p>Lorem ipsum dolor sit amet</p>
                            </div>
                            <!-- Price -->
                            <span class="price"><small>$</small>299</span> </div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div><div class="owl-next" style=""><i class="fa fa-angle-right"></i></div></div><div class="owl-dots" style=""><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div></div></div></div>
    </div>
</section>
