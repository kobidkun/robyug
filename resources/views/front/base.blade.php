<html lang="" itemscope="" itemtype="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Roboyug</title>
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/bootstrap.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/animate.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/font-electro.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/owl-carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/style.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/css/colors/flat-blue.css')}}" media="all" />
    <!-- PLUGINS CSS STYLE -->


    <!-- CUSTOM CSS -->


    <style>
        ul.products > li.product.first, .section-onsale-product ul.products > .first.onsale-product, .section-onsale-product-carousel .onsale-product-carousel .onsale-product ul.products > .first.onsale-product-content, .products-carousel .owl-item ul.products > .first.product{
            height: 350px;
        }
    </style>


    <!-- Bootstrap Core CSS -->


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,700italic,800,800italic,600italic,400italic,300italic' rel='stylesheet' type='text/css'>

    <link rel="" href="">
</head>

<body class="page home page-template-default">
<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>





@yield('content', 'Default Content')






    @include('front.components.footer')











    <script type="text/javascript" src="{{asset('front/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/bootstrap-hover-dropdown.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/echo.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/jquery.easing.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/jquery.waypoints.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('front/js/electro.js')}}"></script>




</div>
</body>
</html>

