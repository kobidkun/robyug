@extends('front.base')
@section('content')
<header id="masthead" class="site-header header-v2">
    <div class="container hidden-md-down">
        <div class="row">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">
                    <img src="{{asset('assets/images/logo.png')}}">

                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="primary-nav animate-dropdown">
                <div class="clearfix">
                    <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                        ☰
                    </button>
                </div>

                <div class="collapse navbar-toggleable-xs" id="default-header">
                    <nav>
                        <ul id="menu-main-menu" class="nav nav-inline yamm">
                            <li class="menu-item menu-item-has-children animate-dropdown dropdown"><a title="Home" href="shop.html" data-toggle="dropdown" class="" aria-haspopup="true">Home</a>

                            </li>
                            <li class="menu-item animate-dropdown"><a title="About Us" href="about.html">About Us</a></li>

                            <li class="menu-item menu-item-has-children animate-dropdown dropdown"><a title="Blog" href="blog.html" data-toggle="dropdown" class="" aria-haspopup="true">Blog</a>

                            </li>
                            <li class="menu-item"><a title="Features" href="#">Features</a></li>
                            <li class="menu-item"><a title="Contact Us" href="#">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="header-support-info">
                <div class="media">
                    <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
                    <div class="media-body">
                        <span class="support-number"><strong>Support</strong> (+800) 856 800 604</span><br>
                        <span class="support-email">Email: info@electro.com</span>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div>

    <div class="container hidden-lg-up">
        <div class="handheld-header">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">
                    <img src="{{asset('assets/images/logo5.png')}}">

                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="handheld-navigation-wrapper">
                <div class="handheld-navbar-toggle-buttons clearfix">
                    <button class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <button class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
                        <i class="ec ec-close-remove"></i>
                    </button>
                </div>
                <div class="handheld-navigation hidden-lg-up" id="default-hh-header">
                    <span class="ehm-close">Close</span>
                    <ul id="menu-all-departments-menu-1" class="nav nav-inline yamm">
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Value of the Day" href="home-v2.html">Camera</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Top 100 Offers" href="home-v3.html">Passive Components</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="New Arrivals" href="home-v3-full-color.html">Active Components</a>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computer Peripherals</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Computer Peripherals/li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#">Ultrasonic Sensors</a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Multirotors Drone</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Multirotors Drone</li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#">Flight Controller</a></li>
                                                <li><a href="#">ESC</a></li>
                                                <li><a href="#">Frame</a></li>
                                                <li><a href="#">Batteries and Charges</a></li>
                                                <li><a href="#">Telemetry and Remote RX/TX</a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Motors</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Motors</li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#">BLDC Motors</a></li>
                                                <li><a href="#">BO Motors</a></li>
                                                <li><a href="#">GEAR Motors</a></li>
                                                <li><a href="#">Jhonson Geared Motors</a></li>

                                            </ul>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Sensors</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Sensors</li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#">IR Sensors</a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li class="nav-divider"></li>

                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Modem</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Modem</li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#">GSM Modem</a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li><a href="#"></a></li>
                                                <li class="nav-divider"></li>
                                                <li>

                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Electronic Components</a>

                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Batteries & Charges</a>

                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Robotic & Diy Kits</a>

                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">IOT</a>

                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

</header>
@endsection
