<header id="masthead" class="site-header header-v1">
    <div class="container hidden-md-down">
        <div class="row">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">
                    <img src="{{asset('/common/images/logo.png')}}"
                         style="position: absolute; top: -28px;"
                         alt="" >
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <form class="navbar-search" method="get" action="/">
                <label class="sr-only screen-reader-text" for="search">Search for:</label>
                <div class="input-group">
                    <input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="Search for products" />
                    <div class="input-group-addon search-categories">
                        <select name='product_cat' id='product_cat' class='postform resizeselect' >
                            <option value='0' selected='selected'>All Categories</option>
                            <option class="level-0" value="laptops-laptops-computers">Laptops</option>
                            <option class="level-0" value="ultrabooks-laptops-computers">Ultrabooks</option>
                            <option class="level-0" value="mac-computers-laptops">Mac Computers</option>
                            <option class="level-0" value="all-in-one-laptops-computers">All in One</option>
                            <option class="level-0" value="servers">Servers</option>
                            <option class="level-0" value="peripherals">Peripherals</option>
                            <option class="level-0" value="gaming-laptops-computers">Gaming</option>
                            <option class="level-0" value="accessories-laptops-computers">Accessories</option>
                            <option class="level-0" value="audio-speakers">Audio Speakers</option>
                            <option class="level-0" value="headphones">Headphones</option>
                            <option class="level-0" value="computer-cases">Computer Cases</option>
                            <option class="level-0" value="printers">Printers</option>
                            <option class="level-0" value="cameras">Cameras</option>
                            <option class="level-0" value="smartphones">Smartphones</option>
                            <option class="level-0" value="game-consoles">Game Consoles</option>
                            <option class="level-0" value="power-banks">Power Banks</option>
                            <option class="level-0" value="smartwatches">Smartwatches</option>
                            <option class="level-0" value="chargers">Chargers</option>
                            <option class="level-0" value="cases">Cases</option>
                            <option class="level-0" value="headphone-accessories">Headphone Accessories</option>
                            <option class="level-0" value="headphone-cases">Headphone Cases</option>
                            <option class="level-0" value="tablets">Tablets</option>
                            <option class="level-0" value="tvs">TVs</option>
                            <option class="level-0" value="wearables">Wearables</option>
                            <option class="level-0" value="pendrives">Pendrives</option>
                        </select>
                    </div>
                    <div class="input-group-btn">
                        <input type="hidden" id="search-param" name="post_type" value="product" />
                        <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
                    </div>
                </div>
            </form>
            <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
                <li class="nav-item dropdown">
                    <a href="cart.html" class="nav-link" data-toggle="dropdown">
                        <i class="ec ec-shopping-bag"></i>
                        <span class="cart-items-count count">4</span>
                        <span class="cart-items-total-price total-price"><span class="amount">&#36;1,215.00</span></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-mini-cart">
                        <li>
                            <div class="widget_shopping_cart_content">

                                <ul class="cart_list product_list_widget ">


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
                                        </a>

                                        <span class="quantity">2 × <span class="amount">£150.00</span></span>
                                    </li>


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
                                        </a>

                                        <span class="quantity">1 × <span class="amount">£399.99</span></span>
                                    </li>

                                    <li class="mini_cart_item">
                                        <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

                                        </a>

                                        <span class="quantity">1 × <span class="amount">£269.99</span></span>
                                    </li>


                                </ul><!-- end product list -->


                                <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                                <p class="buttons">
                                    <a class="button wc-forward" href="cart.html">View Cart</a>
                                    <a class="button checkout wc-forward" href="checkout.html">Checkout</a>
                                </p>


                            </div>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="navbar-wishlist nav navbar-nav pull-right flip">
                <li class="nav-item">
                    <a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
                </li>
            </ul>
            <ul class="navbar-compare nav navbar-nav pull-right flip">
                <li class="nav-item">
                    <a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
                </li>
            </ul>
        </div><!-- /.row -->

        <div class="row">
            <div class="col-xs-12 col-lg-3">
                <nav>
                    <ul class="list-group vertical-menu yamm make-absolute">
                        <li class="list-group-item"><span><i class="fa fa-list-ul"></i> All Departments</span></li>

                        <li class="highlight menu-item animate-dropdown"><a title="Value of the Day" href="home-v2.html">Value of the Day</a></li>

                        <li class="highlight menu-item animate-dropdown"><a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a></li>

                        <li class="highlight menu-item animate-dropdown"><a title="New Arrivals" href="home-v3-full-color-background.html">New Arrivals</a></li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &#038; Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Cameras, Audio &amp; Video" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &#038; Video</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Mobiles &amp; Tablets" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &#038; Tablets</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Movies, Music &amp; Video Games" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &#038; Video Games</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="TV &amp; Audio" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &#038; Audio</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &#038; Eyewear</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Car, Motorbike &amp; Industrial" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &#038; Industrial</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper vc_box_border_grey">

                                                                    <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li id="menu-item-2695" class="menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown"><a title="Cases" href="product-category.html">Cases</a></li>
                                <li class="menu-item animate-dropdown"><a title="Chargers" href="product-category.html">Chargers</a></li>
                                <li class="menu-item animate-dropdown"><a title="Headphone Accessories" href="product-category.html">Headphone Accessories</a></li>
                                <li class="menu-item animate-dropdown"><a title="Headphone Cases" href="product-category.html">Headphone Cases</a></li>
                                <li class="menu-item animate-dropdown"><a title="Headphones" href="product-category.html">Headphones</a></li>
                                <li class="menu-item animate-dropdown"><a title="Computer Accessories" href="product-category.html">Computer Accessories</a></li>
                                <li class="menu-item animate-dropdown"><a title="Laptop Accessories" href="product-category.html">Laptop Accessories</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="col-xs-12 col-lg-9">
                <nav>
                    <ul id="menu-secondary-nav" class="secondary-nav">
                        <li class="highlight menu-item"><a href="home-v2.html">Super Deals</a></li>
                        <li class="menu-item"><a href="home-v3.html">Featured Brands</a></li>
                        <li class="menu-item"><a href="home-v3-full-color-background.html">Trending Styles</a></li>
                        <li class="menu-item"><a href="blog-v1.html">Gift Cards</a></li>
                        <li class="pull-right menu-item"><a href="blog-v2.html">Free Shipping on Orders $50+</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="container hidden-lg-up">
        <div class="handheld-header">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">

                    <img src="{{asset('/common/images/logo.png')}}"

                         alt="" >
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="handheld-navigation-wrapper">
                <div class="handheld-navbar-toggle-buttons clearfix">
                    <button class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <button class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
                        <i class="ec ec-close-remove"></i>
                    </button>
                </div>
                <div class="handheld-navigation hidden-lg-up" id="default-hh-header">
                    <span class="ehm-close">Close</span>
                    <ul id="menu-all-departments-menu-1" class="nav nav-inline yamm">
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Value of the Day" href="home-v2.html">Value of the Day</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="New Arrivals" href="home-v3-full-color.html">New Arrivals</a>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                <li><a href="#">Computer Accessories</a></li>
                                                <li><a href="#">Software</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Office &amp; Stationery</li>
                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                <li><a href="#">Pens &amp; Writing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Cameras &amp; Photography</li>
                                                <li><a href="#">All Cameras &amp; Photography</a></li>
                                                <li><a href="#">Digital SLRs</a></li>
                                                <li><a href="#">Point &amp; Shoot Cameras</a></li>
                                                <li><a href="#">Lenses</a></li>
                                                <li><a href="#">Camera Accessories</a></li>
                                                <li><a href="#">Security &amp; Surveillance</a></li>
                                                <li><a href="#">Binoculars &amp; Telescopes</a></li>
                                                <li><a href="#">Camcorders</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Headphones &amp; Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Mobiles &amp; Tablets</li>
                                                <li><a href="#">All Mobile Phones</a></li>
                                                <li><a href="#">Smartphones</a></li>
                                                <li><a href="#">Android Mobiles</a></li>
                                                <li><a href="#">Windows Mobiles</a></li>
                                                <li><a href="#">Refurbished Mobiles</a></li>
                                                <li class="nav-divider"></li>
                                                <li><a href="#">All Mobile Accessories</a></li>
                                                <li><a href="#">Cases &amp; Covers</a></li>
                                                <li><a href="#">Screen Protectors</a></li>
                                                <li><a href="#">Power Banks</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title"></li>
                                                <li><a href="#">All Tablets</a></li>
                                                <li><a href="#">Tablet Accessories</a></li>
                                                <li><a href="#">Landline Phones</a></li>
                                                <li><a href="#">Wearable Devices</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Movies &amp; TV Shows</li>
                                                <li><a href="#">All Movies &amp; TV Shows</a></li>
                                                <li><a href="#">Blu-ray</a></li>
                                                <li><a href="#">All English</a></li>
                                                <li><a href="#">All Hindi</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Video Games</li>
                                                <li><a href="#">All Consoles, Games &amp; Accessories</a></li>
                                                <li><a href="#">PC Games</a></li>
                                                <li><a href="#">Pre-orders &amp; New Releases</a></li>
                                                <li><a href="#">Consoles</a></li>
                                                <li><a href="#">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">All Music</a></li>
                                                <li><a href="#">International Music</a></li>
                                                <li><a href="#">Film Songs</a></li>
                                                <li><a href="#">Indian Classical</a></li>
                                                <li><a href="#">Musical Instruments</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                                <li><a href="#">Audio &amp; Video Accessories</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">Electro Home Appliances</span>
                                                        <span class="nav-subtext">Available in select cities</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Watches</li>
                                                <li><a href="#">All Watches</a></li>
                                                <li><a href="#">Men&#8217;s Watches</a></li>
                                                <li><a href="#">Women&#8217;s Watches</a></li>
                                                <li><a href="#">Premium Watches</a></li>
                                                <li><a href="#">Deals on Watches</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Eyewear</li>
                                                <li><a href="#">Men&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Women&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Spectacle Frames</a></li>
                                                <li><a href="#">All Sunglasses</a></li>
                                                <li><a href="#">Amazon Fashion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Car, Motorbike &amp; Industrial" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Car &amp; Motorbike</li>
                                                <li><a href="#">All Cars &amp; Bikes</a></li>
                                                <li><a href="#">Car &amp; Bike Care</a></li>
                                                <li><a href="#">Lubricants</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Shop for Bike</li>
                                                <li><a href="#">Helmets &amp; Gloves</a></li>
                                                <li><a href="#">Bike Parts</a></li>
                                                <li class="nav-title">Shop for Car</li>
                                                <li><a href="#">Air Fresheners</a></li>
                                                <li><a href="#">Car Parts</a></li>
                                                <li><a href="#">Tyre Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Industrial Supplies</li>
                                                <li><a href="#">All Industrial Supplies</a></li>
                                                <li><a href="#">Lab &amp; Scientific</a></li>
                                                <li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
                                                <li><a href="#">Test, Measure &amp; Inspect</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <a title="Cases" href="product-category.html">Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Chargers" href="product-category.html">Chargers</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Accessories" href="product-category.html">Headphone Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Cases" href="product-category.html">Headphone Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphones" href="product-category.html">Headphones</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Computer Accessories" href="product-category.html">Computer Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Laptop Accessories" href="product-category.html">Laptop Accessories</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header><!-- #masthead -->
