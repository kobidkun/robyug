@section('title', $pg->title)


 @extends('front.pages.shop.shopbase')


 @section('shopcontent')

    <nav class="navbar navbar-primary navbar-full hidden-md-down">
        <div class="container">
            <ul class="nav navbar-nav departments-menu animate-dropdown">
                <li class="nav-item dropdown ">

                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle" >
                        Shop by Department</a>
                    <ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown">
                        <li class="highlight menu-item animate-dropdown active"><a title="Value of the Day" href="product-category.html">Value of the Day</a></li>
                        <li class="highlight menu-item animate-dropdown"><a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a></li>
                        <li class="highlight menu-item animate-dropdown"><a title="New Arrivals" href="home-v3-full-color-background.html">New Arrivals</a></li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &#038; Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &#038; Video</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2586 dropdown">
                            <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &#038; Tablets</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2587 dropdown">
                            <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &#038; Video Games</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2588 dropdown">
                            <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &#038; Audio</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2589 dropdown">

                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &#038; Eyewear</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2590 dropdown">

                            <a title="Car, Motorbike &amp; Industrial" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &#038; Industrial</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/></div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                                    <li><a href="#">Computer Accessories</a></li>
                                                                    <li><a href="#">Software</a></li>
                                                                    <li class="nav-divider"></li>
                                                                    <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-item animate-dropdown"><a title="Accessories" href="product-category.html">Accessories</a></li>
                        <li class="menu-item animate-dropdown"><a title="Printers &amp; Ink" href="product-category.html">Printers &#038; Ink</a></li>
                        <li class="menu-item animate-dropdown"><a title="Software" href="product-category.html">Software</a></li>
                        <li class="menu-item animate-dropdown"><a title="Office Supplies" href="product-category.html">Office Supplies</a></li>
                        <li class="menu-item animate-dropdown"><a title="Computer Components" href="product-category.html">Computer Components</a></li>
                        <li class="menu-item animate-dropdown"><a title="Car Electronic &amp; GPS" href="product-category.html">Car Electronic &#038; GPS</a></li>
                        <li class="menu-item animate-dropdown"><a title="Accessories" href="product-category.html">Accessories</a></li>
                        <li class="menu-item animate-dropdown"><a title="Printers &amp; Ink" href="product-category.html">Printers &#038; Ink</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-search" method="get" action="/">
                <label class="sr-only screen-reader-text" for="search">Search for:</label>
                <div class="input-group">
                    <input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="Search for products" />
                    <div class="input-group-addon search-categories">
                        <select name='product_cat' id='product_cat' class='postform resizeselect' >
                            <option value='0' selected='selected'>All Categories</option>
                            <option class="level-0" value="laptops-laptops-computers">Laptops</option>
                            <option class="level-0" value="ultrabooks-laptops-computers">Ultrabooks</option>
                            <option class="level-0" value="mac-computers-laptops">Mac Computers</option>
                            <option class="level-0" value="all-in-one-laptops-computers">All in One</option>
                            <option class="level-0" value="servers">Servers</option>
                            <option class="level-0" value="peripherals">Peripherals</option>
                            <option class="level-0" value="gaming-laptops-computers">Gaming</option>
                            <option class="level-0" value="accessories-laptops-computers">Accessories</option>
                            <option class="level-0" value="audio-speakers">Audio Speakers</option>
                            <option class="level-0" value="headphones">Headphones</option>
                            <option class="level-0" value="computer-cases">Computer Cases</option>
                            <option class="level-0" value="printers">Printers</option>
                            <option class="level-0" value="cameras">Cameras</option>
                            <option class="level-0" value="smartphones">Smartphones</option>
                            <option class="level-0" value="game-consoles">Game Consoles</option>
                            <option class="level-0" value="power-banks">Power Banks</option>
                            <option class="level-0" value="smartwatches">Smartwatches</option>
                            <option class="level-0" value="chargers">Chargers</option>
                            <option class="level-0" value="cases">Cases</option>
                            <option class="level-0" value="headphone-accessories">Headphone Accessories</option>
                            <option class="level-0" value="headphone-cases">Headphone Cases</option>
                            <option class="level-0" value="tablets">Tablets</option>
                            <option class="level-0" value="tvs">TVs</option>
                            <option class="level-0" value="wearables">Wearables</option>
                            <option class="level-0" value="pendrives">Pendrives</option>
                        </select>
                    </div>
                    <div class="input-group-btn">
                        <input type="hidden" id="search-param" name="post_type" value="product" />
                        <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
                    </div>
                </div>
            </form>

            <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
                <li class="nav-item dropdown">
                    <a href="cart.html" class="nav-link" data-toggle="dropdown">
                        <i class="ec ec-shopping-bag"></i>
                        <span class="cart-items-count count">4</span>
                        <span class="cart-items-total-price total-price"><span class="amount">&#36;1,215.00</span></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-mini-cart">
                        <li>
                            <div class="widget_shopping_cart_content">

                                <ul class="cart_list product_list_widget ">


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
                                        </a>

                                        <span class="quantity">2 × <span class="amount">£150.00</span></span>
                                    </li>


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
                                        </a>

                                        <span class="quantity">1 × <span class="amount">£399.99</span></span>
                                    </li>

                                    <li class="mini_cart_item">
                                        <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

                                        </a>

                                        <span class="quantity">1 × <span class="amount">£269.99</span></span>
                                    </li>


                                </ul><!-- end product list -->


                                <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                                <p class="buttons">
                                    <a class="button wc-forward" href="cart.html">View Cart</a>
                                    <a class="button checkout wc-forward" href="checkout.html">Checkout</a>
                                </p>


                            </div>
                        </li>
                    </ul>
                </li>
            </ul>


        </div>
    </nav>

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">

            <nav class="woocommerce-breadcrumb" ><a href="/">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>
                {{$pg->title}}

            </nav>

            <div id="primary" class="content-area">
                <main id="main" class="site-main">

                    <section class="section-product-cards-carousel" >
                        <header>
                            <h2 class="h1">Recommended Products</h2>
                            <div class="owl-nav">
                                <a href="#products-carousel-prev" data-target="#recommended-product" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                                <a href="#products-carousel-next" data-target="#recommended-product" class="slider-next"><i class="fa fa-angle-right"></i></a>
                            </div>
                        </header>

                        <div id="recommended-product">
                            <div class="woocommerce columns-4">
                                <div class="products owl-carousel products-carousel columns-4 owl-loaded owl-drag">

                                    @foreach($featureds as $product)
                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html"
                                                                                         rel="tag">{{$product->title}}</a></span>
                                                <a href="single-product.html">
                                                    <h3>{{$product->title}}</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="assets/images/blank.gif"
                                                             data-echo="{{asset($product->img_small)}}" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">

                                                                @if ($product->discount === null)
                                                                        <ins><span class="amount">
                                                                           {{$product->value}}
                                                                    </span></ins>


                                                                    @else

                                                                        <ins><span class="amount">
                                                                           {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                        <del><span class="amount">

                                                                           {{$product->value}}

                                                                    </span></del>

                                                                    @endif
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->



                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                   @endforeach


                                </div>
                            </div>
                        </div>
                    </section>

                    <header class="page-header">
                        <h1 class="page-title"> {{$pg->title}}</h1>
                        <p class="woocommerce-result-count">Showing 1&ndash;15 of 20 results</p>
                    </header>

                    <div class="shop-control-bar">
                        <ul class="shop-view-switcher nav nav-tabs" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" title="Grid View" href="#grid"><i class="fa fa-th"></i></a></li>
                            <li class="nav-item"><a class="nav-link " data-toggle="tab" title="Grid Extended View" href="#grid-extended"><i class="fa fa-align-justify"></i></a></li>
                            <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View" href="#list-view"><i class="fa fa-list"></i></a></li>
                            <li class="nav-item"><a class="nav-link " data-toggle="tab" title="List View Small" href="#list-view-small"><i class="fa fa-th-list"></i></a></li>
                        </ul>
                        <form class="woocommerce-ordering" method="get">
                            <select name="orderby" class="orderby">
                                <option value="menu_order"  selected='selected'>Default sorting</option>
                                <option value="popularity" >Sort by popularity</option>
                                <option value="rating" >Sort by average rating</option>
                                <option value="date" >Sort by newness</option>
                                <option value="price" >Sort by price: low to high</option>
                                <option value="price-desc" >Sort by price: high to low</option>
                            </select>
                        </form>
                        <form class="form-electro-wc-ppp"><select name="ppp" onchange="this.form.submit()" class="electro-wc-wppp-select c-select"><option value="15"  selected='selected'>Show 15</option><option value="-1" >Show All</option></select></form>
                        <nav class="electro-advanced-pagination">
                            <form method="post" class="form-adv-pagination"><input id="goto-page" size="2" min="1" max="2" step="1" type="number" class="form-control" value="1" /></form> of 2<a class="next page-numbers" href="#">&rarr;</a>			<script>
                                jQuery(document).ready(function($){
                                    $( '.form-adv-pagination' ).on( 'submit', function() {
                                        var link 		= '#',
                                            goto_page 	= $( '#goto-page' ).val(),
                                            new_link 	= link.replace( '%#%', goto_page );

                                        window.location.href = new_link;
                                        return false;
                                    });
                                });
                            </script>
                        </nav>
                    </div>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="grid" aria-expanded="true">

                            <ul class="products columns-3">


                                @foreach($products as $product)

                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories">
                                                <a href="/" rel="tag">

                                                    {{$product->primary_group}}
                                                </a></span>
                                            <a href="{{route('front.productdetails',$product->slug)}}">
                                                <h3>{{$product->title}}</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="{{asset($product->img_small)}}"
                                                         height="230px"

                                                         src="/front/assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                               
                                                               
                                                                @if ($product->discount === null)
                                                                    <ins><span class="amount">
                                                                           {{$product->value}}
                                                                    </span></ins>


                                                                    @else

                                                                    <ins><span class="amount">
                                                                           {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                    <del><span class="amount">

                                                                           {{$product->value}}

                                                                    </span></del>




                                                                @endif
                                                                

                                                                
                                                                
                                                                
                                                                
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>

                               @endforeach



                               {{-- <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>


                                --}}


                               {{-- <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>



                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">

                                                    <img data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
--}}
                            </ul>
                        </div>



                        <div role="tabpanel" class="tab-pane" id="grid-extended" aria-expanded="true">

                            <ul class="products columns-3">
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product first">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product ">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                                <li class="product last">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                                </div>

                                                <div class="product-rating">
                                                    <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                </div>

                                                <div class="product-short-description">
                                                    <ul>
                                                        <li><span class="a-list-item">Intel Core i5 processors (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Intel Iris Graphics 6100 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Flash storage</span></li>
                                                        <li><span class="a-list-item">Up to 10 hours of battery life2 (13-inch model)</span></li>
                                                        <li><span class="a-list-item">Force Touch trackpad (13-inch model)</span></li>
                                                    </ul>
                                                </div>

                                                <div class="product-sku">SKU: 5487FB8/15</div>
                                            </a>
                                            <div class="price-add-to-cart">
                                                        <span class="price">
                                                            <span class="electro-price">
                                                                <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                <del><span class="amount">&#036;2,299.00</span></del>
                                                            </span>
                                                        </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">
                                                    <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>

                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                            </ul>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="list-view" aria-expanded="true">
                            <ul class="products columns-3">
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="#">Tablets</a></span><a href="single-product.html"><h3>Tablet Air 3 WiFi 64GB  Gold</h3>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">

                                                    <div class="availability in-stock">Availablity: <span>In stock</span></div>

                                                    <span class="price"><span class="electro-price"><span class="amount">$629.00</span></span></span>
                                                    <a class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_sku="5487FB8/35" data-product_id="2706" data-quantity="1" href="single-product.html" rel="nofollow">Add to cart</a>
                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-2706">
                                                                <a class="add_to_wishlist" data-product-type="simple" data-product-id="2706" rel="nofollow" href="#">Wishlist</a>

                                                                <div style="display:none;" class="yith-wcwl-wishlistaddedbrowse hide">
                                                                    <span class="feedback">Product added!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="display:none" class="yith-wcwl-wishlistexistsbrowse hide">
                                                                    <span class="feedback">The product is already in the wishlist!</span>
                                                                    <a rel="nofollow" href="#">Wishlist</a>
                                                                </div>

                                                                <div style="clear:both"></div>
                                                                <div class="yith-wcwl-wishlistaddresponse"></div>

                                                            </div>
                                                            <div class="clear"></div>
                                                            <a data-product_id="2706" class="add-to-compare-link" href="#">Compare</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="list-view-small" aria-expanded="true">

                            <ul class="products columns-3">
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="product list-view list-view-small">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="single-product.html">
                                                <img class="wp-post-image" data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body media-middle">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <span class="loop-product-categories"><a rel="tag" href="product-category.html">Smartphones</a></span><a href="product-category.html"><h3>Ultrabook UX605CA-FC050T</h3>
                                                        <div class="product-short-description">
                                                            <ul style="padding-left: 18px;">
                                                                <li>4.5 inch HD Screen</li>
                                                                <li>Android 4.4 KitKat OS</li>
                                                                <li>1.4 GHz Quad Core&trade; Processor</li>
                                                                <li>20 MP front Camera</li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-rating">
                                                            <div title="Rated 4 out of 5" class="star-rating"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div> (3)
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="price-add-to-cart">
                                                        <span class="price"><span class="electro-price"><span class="amount">$1,218.00</span></span></span>
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">
                                                            <a href="#" rel="nofollow" class="add_to_wishlist">Wishlist</a>
                                                            <a href="compare.html" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="shop-control-bar-bottom">
                        <form class="form-electro-wc-ppp">
                            <select class="electro-wc-wppp-select c-select" onchange="this.form.submit()" name="ppp"><option selected="selected" value="15">Show 15</option><option value="-1">Show All</option></select>
                        </form>
                        <p class="woocommerce-result-count">Showing 1&ndash;15 of 20 results</p>
                        <nav class="woocommerce-pagination">
                            <ul class="page-numbers">
                                <li><span class="page-numbers current">1</span></li>
                                <li><a href="#" class="page-numbers">2</a></li>
                                <li><a href="#" class="next page-numbers">→</a></li>
                            </ul>
                        </nav>
                    </div>

                </main><!-- #main -->
            </div><!-- #primary -->

            <div id="sidebar" class="sidebar" role="complementary">
                <aside class="widget woocommerce widget_product_categories electro_widget_product_categories">
                    <ul class="product-categories category-single">
                        <li class="product_cat">
                            <ul class="show-all-cat">
                                <li class="product_cat"><span class="show-all-cat-dropdown">Show All Categories</span>
                                    <ul>
@foreach ($pgs as $pc)


                                        <li class="cat-item"><a href="product-category.html">
                                                {{$pc->title}}

                                            </a> <span class="count">


                                            </span></li>

                                        @endforeach

                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li class="cat-item current-cat"><a href="product-category.html">{{$pg->title}}</a>


                                    @if ($pg->primary_group_id === null)
                                        @foreach ($pg->secondary_groups as $sc)


                                    <ul class='children'>
                                        <li class="cat-item"><a href="product-category.html">
{{$sc->title}}


                                            </a> <span class="count"></span></li>
                                          </ul>

                                        @endforeach

                                    @endif
                                    


                                </li>
                            </ul>
                        </li>
                    </ul>
                </aside>
               {{-- <aside class="widget widget_electro_products_filter">
                    <h3 class="widget-title">Filters</h3>
                    <aside class="widget woocommerce widget_layered_nav">
                        <h3 class="widget-title">Brands</h3>
                        <ul>
                            <li style=""><a href="#">Apple</a> <span class="count">(4)</span></li>
                            <li style=""><a href="#">Gionee</a> <span class="count">(2)</span></li>
                            <li style=""><a href="#">HTC</a> <span class="count">(2)</span></li>
                            <li style=""><a href="#">LG</a> <span class="count">(2)</span></li>
                            <li style=""><a href="#">Micromax</a> <span class="count">(1)</span></li>
                        </ul>
                        <p class="maxlist-more"><a href="#">+ Show more</a></p>
                    </aside>
                    <aside class="widget woocommerce widget_layered_nav">
                        <h3 class="widget-title">Color</h3>
                        <ul>
                            <li style=""><a href="#">Black</a> <span class="count">(4)</span></li>
                            <li style=""><a href="#">Black Leather</a> <span class="count">(2)</span></li>
                            <li style=""><a href="#">Turquoise</a> <span class="count">(2)</span></li>
                            <li style=""><a href="#">White</a> <span class="count">(4)</span></li>
                            <li style=""><a href="#">Gold</a> <span class="count">(4)</span></li>
                        </ul>
                        <p class="maxlist-more"><a href="#">+ Show more</a></p>
                    </aside>
                    <aside class="widget woocommerce widget_price_filter">
                        <h3 class="widget-title">Price</h3>
                        <form action="#">
                            <div class="price_slider_wrapper">
                                <div style="" class="price_slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span>
                                </div>
                                <div class="price_slider_amount">
                                    <a href="#" class="button">Filter</a>
                                    <div style="" class="price_label">Price: <span class="from">$428</span> &mdash; <span class="to">$3485</span></div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </form>
                    </aside>
                </aside>--}}

            </div>

        </div><!-- .container -->
    </div><!-- #content -->


@endsection
