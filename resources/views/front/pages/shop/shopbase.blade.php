
<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> @yield('title', ' ') | {{env('APP_NAME')}}</title>

    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/bootstrap.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/font-awesome.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/animate.min.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/font-electro.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/owl-carousel.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/style.css')}}" media="all" />
    <link rel="stylesheet" type="text/css" href="{{asset('front/assets/css/colors/flat-blue.css')}}" media="all" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,700italic,800,800italic,600italic,400italic,300italic' rel='stylesheet' type='text/css'>

    <link rel="shortcut icon" href="{{asset('/common/images/fabicon.png')}}">
</head>

<body class="left-sidebar">
<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

   @include('front.pages.shop.shoptopbar')

    @include('front.pages.shop.shopheaer')
    @yield('shopcontent')

    @include('front.pages.shop.shopfooter')

</div><!-- #page -->

<script type="text/javascript" src="{{asset('front/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/tether.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/bootstrap-hover-dropdown.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/echo.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/jquery.easing.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front/assets/js/electro.js')}}"></script>

</body>
</html>
