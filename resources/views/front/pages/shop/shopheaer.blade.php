<header id="masthead" class="site-header header-v2">
    <div class="container hidden-md-down">
        <div class="row">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="/" class="header-logo-link">
                    <img style="position: absolute; top: -25px;" src="{{asset('common/images/logo.png')}}">
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="primary-nav animate-dropdown">
                <div class="clearfix">
                    <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                        &#9776;
                    </button>
                </div>

                <div class="collapse navbar-toggleable-xs" id="default-header">
                    <nav>
                        <ul id="menu-main-menu" class="nav nav-inline yamm">

                            <li class="menu-item "><a title="About Us" href="#">Home</a></li>



                            <li class="menu-item"><a title="Features" href="#">About</a></li>
                            <li class="menu-item"><a title="Contact Us" href="#">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="header-support-info">
                <div class="media">
                    <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
                    <div class="media-body">
                        <span class="support-number"><strong>Support</strong> {{env('PHONE')}}</span><br/>
                        <span class="support-email">Email: {{env('EMAIL')}}</span>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div>

    <div class="container hidden-lg-up">
        <div class="handheld-header">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="/" class="header-logo-link">
                    <img src="{{asset('common/images/logo.png')}}">
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="handheld-navigation-wrapper">
                <div class="handheld-navbar-toggle-buttons clearfix">
                    <button class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <button class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
                        <i class="ec ec-close-remove"></i>
                    </button>
                </div>
                <div class="handheld-navigation hidden-lg-up" id="default-hh-header">
                    <span class="ehm-close">Close</span>
                    <ul id="menu-all-departments-menu-1" class="nav nav-inline yamm">

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Computers &amp; Accessories" href="" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">COMPUTER PERIPHERALS</li>
                                                <li><a href="#">Ultrasonic sensors</a></li>

                                            </ul>
                                        </div>

                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Cameras &amp; Photography</li>
                                                <li><a href="#">All Cameras &amp; Photography</a></li>
                                                <li><a href="#">Digital SLRs</a></li>
                                                <li><a href="#">Point &amp; Shoot Cameras</a></li>
                                                <li><a href="#">Lenses</a></li>
                                                <li><a href="#">Camera Accessories</a></li>
                                                <li><a href="#">Security &amp; Surveillance</a></li>
                                                <li><a href="#">Binoculars &amp; Telescopes</a></li>
                                                <li><a href="#">Camcorders</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Headphones &amp; Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Mobiles &amp; Tablets</li>
                                                <li><a href="#">All Mobile Phones</a></li>
                                                <li><a href="#">Smartphones</a></li>
                                                <li><a href="#">Android Mobiles</a></li>
                                                <li><a href="#">Windows Mobiles</a></li>
                                                <li><a href="#">Refurbished Mobiles</a></li>
                                                <li class="nav-divider"></li>
                                                <li><a href="#">All Mobile Accessories</a></li>
                                                <li><a href="#">Cases &amp; Covers</a></li>
                                                <li><a href="#">Screen Protectors</a></li>
                                                <li><a href="#">Power Banks</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title"></li>
                                                <li><a href="#">All Tablets</a></li>
                                                <li><a href="#">Tablet Accessories</a></li>
                                                <li><a href="#">Landline Phones</a></li>
                                                <li><a href="#">Wearable Devices</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Movies &amp; TV Shows</li>
                                                <li><a href="#">All Movies &amp; TV Shows</a></li>
                                                <li><a href="#">Blu-ray</a></li>
                                                <li><a href="#">All English</a></li>
                                                <li><a href="#">All Hindi</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Video Games</li>
                                                <li><a href="#">All Consoles, Games &amp; Accessories</a></li>
                                                <li><a href="#">PC Games</a></li>
                                                <li><a href="#">Pre-orders &amp; New Releases</a></li>
                                                <li><a href="#">Consoles</a></li>
                                                <li><a href="#">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">All Music</a></li>
                                                <li><a href="#">International Music</a></li>
                                                <li><a href="#">Film Songs</a></li>
                                                <li><a href="#">Indian Classical</a></li>
                                                <li><a href="#">Musical Instruments</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                                <li><a href="#">Audio &amp; Video Accessories</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">Electro Home Appliances</span>
                                                        <span class="nav-subtext">Available in select cities</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Watches</li>
                                                <li><a href="#">All Watches</a></li>
                                                <li><a href="#">Men&#8217;s Watches</a></li>
                                                <li><a href="#">Women&#8217;s Watches</a></li>
                                                <li><a href="#">Premium Watches</a></li>
                                                <li><a href="#">Deals on Watches</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Eyewear</li>
                                                <li><a href="#">Men&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Women&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Spectacle Frames</a></li>
                                                <li><a href="#">All Sunglasses</a></li>
                                                <li><a href="#">Amazon Fashion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Car, Motorbike &amp; Industrial" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Car &amp; Motorbike</li>
                                                <li><a href="#">All Cars &amp; Bikes</a></li>
                                                <li><a href="#">Car &amp; Bike Care</a></li>
                                                <li><a href="#">Lubricants</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Shop for Bike</li>
                                                <li><a href="#">Helmets &amp; Gloves</a></li>
                                                <li><a href="#">Bike Parts</a></li>
                                                <li class="nav-title">Shop for Car</li>
                                                <li><a href="#">Air Fresheners</a></li>
                                                <li><a href="#">Car Parts</a></li>
                                                <li><a href="#">Tyre Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Industrial Supplies</li>
                                                <li><a href="#">All Industrial Supplies</a></li>
                                                <li><a href="#">Lab &amp; Scientific</a></li>
                                                <li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
                                                <li><a href="#">Test, Measure &amp; Inspect</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <a title="Cases" href="product-category.html">Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Chargers" href="product-category.html">Chargers</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Accessories" href="product-category.html">Headphone Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Cases" href="product-category.html">Headphone Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphones" href="product-category.html">Headphones</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Computer Accessories" href="product-category.html">Computer Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Laptop Accessories" href="product-category.html">Laptop Accessories</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header><!-- #masthead -->
