@extends('front.pages.common.base')

@section('content')
    <header id="masthead" class="site-header header-v2">
        <div class="container hidden-md-down">
            <div class="row">

                <!-- ============================================================= Header Logo ============================================================= -->
                <div class="header-logo">
                    <a href="/" class="header-logo-link">
                        <img src="{{asset('front/assets/images/logo.png')}}" style="position: absolute; top: -35px;">
                    </a>
                </div>
                <!-- ============================================================= Header Logo : End============================================================= -->

                <div class="primary-nav animate-dropdown">
                    <div class="clearfix">
                        <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                            &#9776;
                        </button>
                    </div>

                    <div class="collapse navbar-toggleable-xs" id="default-header">
                        <nav>
                            <ul id="menu-main-menu" class="nav nav-inline yamm">

                                <li class="menu-item "><a title="About Us" href="#">Home</a></li>



                                <li class="menu-item"><a title="Features" href="#">About</a></li>
                                <li class="menu-item"><a title="Contact Us" href="#">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="header-support-info">
                    <div class="media">
                        <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
                        <div class="media-body">
                            <span class="support-number"><strong>Support</strong> {{env('PHONE')}}</span><br/>
                            <span class="support-email">Email: {{env('EMAIL')}}</span>
                        </div>
                    </div>
                </div>

            </div><!-- /.row -->
        </div>

        <div class="container hidden-lg-up">
            <div class="handheld-header">
                <!-- ============================================================= Header Logo ============================================================= -->
                <div class="header-logo">
                    <a href="/" class="header-logo-link">
                        <img src="{{asset('front/assets/images/logo.png')}}" alt="">
                    </a>
                </div>
                <!-- ============================================================= Header Logo : End============================================================= -->


                <div class="handheld-navigation-wrapper">
                    <div class="handheld-navbar-toggle-buttons clearfix">
                        <button class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                        <button class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
                            <i class="ec ec-close-remove"></i>
                        </button>
                    </div>
                    <div class="handheld-navigation hidden-lg-up" id="default-hh-header">
                        <span class="ehm-close">Close</span>
                        <ul id="menu-all-departments-menu-1" class="nav nav-inline yamm">
                            <li class="highlight menu-item animate-dropdown ">
                                <a title="Value of the Day" href="home-v2.html">Value of the Day</a>
                            </li>
                            <li class="highlight menu-item animate-dropdown ">
                                <a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a>
                            </li>
                            <li class="highlight menu-item animate-dropdown ">
                                <a title="New Arrivals" href="home-v3-full-color.html">New Arrivals</a>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown ">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Computers &amp; Accessories</li>
                                                    <li><a href="#">All Computers &amp; Accessories</a></li>
                                                    <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                    <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                    <li><a href="#">Printers &amp; Ink</a></li>
                                                    <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                    <li><a href="#">Computer Accessories</a></li>
                                                    <li><a href="#">Software</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li>
                                                        <a href="#">
                                                            <span class="nav-text">All Electronics</span>
                                                            <span class="nav-subtext">Discover more products</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Office &amp; Stationery</li>
                                                    <li><a href="#">All Office &amp; Stationery</a></li>
                                                    <li><a href="#">Pens &amp; Writing</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Cameras &amp; Photography</li>
                                                    <li><a href="#">All Cameras &amp; Photography</a></li>
                                                    <li><a href="#">Digital SLRs</a></li>
                                                    <li><a href="#">Point &amp; Shoot Cameras</a></li>
                                                    <li><a href="#">Lenses</a></li>
                                                    <li><a href="#">Camera Accessories</a></li>
                                                    <li><a href="#">Security &amp; Surveillance</a></li>
                                                    <li><a href="#">Binoculars &amp; Telescopes</a></li>
                                                    <li><a href="#">Camcorders</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li>
                                                        <a href="#">
                                                            <span class="nav-text">All Electronics</span>
                                                            <span class="nav-subtext">Discover more products</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Audio &amp; Video</li>
                                                    <li><a href="#">All Audio &amp; Video</a></li>
                                                    <li><a href="#">Headphones &amp; Speakers</a></li>
                                                    <li><a href="#">Home Entertainment Systems</a></li>
                                                    <li><a href="#">MP3 &amp; Media Players</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown ">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Mobiles &amp; Tablets</li>
                                                    <li><a href="#">All Mobile Phones</a></li>
                                                    <li><a href="#">Smartphones</a></li>
                                                    <li><a href="#">Android Mobiles</a></li>
                                                    <li><a href="#">Windows Mobiles</a></li>
                                                    <li><a href="#">Refurbished Mobiles</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li><a href="#">All Mobile Accessories</a></li>
                                                    <li><a href="#">Cases &amp; Covers</a></li>
                                                    <li><a href="#">Screen Protectors</a></li>
                                                    <li><a href="#">Power Banks</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li>
                                                        <a href="#">
                                                            <span class="nav-text">All Electronics</span>
                                                            <span class="nav-subtext">Discover more products</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title"></li>
                                                    <li><a href="#">All Tablets</a></li>
                                                    <li><a href="#">Tablet Accessories</a></li>
                                                    <li><a href="#">Landline Phones</a></li>
                                                    <li><a href="#">Wearable Devices</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown ">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Movies &amp; TV Shows</li>
                                                    <li><a href="#">All Movies &amp; TV Shows</a></li>
                                                    <li><a href="#">Blu-ray</a></li>
                                                    <li><a href="#">All English</a></li>
                                                    <li><a href="#">All Hindi</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li class="nav-title">Video Games</li>
                                                    <li><a href="#">All Consoles, Games &amp; Accessories</a></li>
                                                    <li><a href="#">PC Games</a></li>
                                                    <li><a href="#">Pre-orders &amp; New Releases</a></li>
                                                    <li><a href="#">Consoles</a></li>
                                                    <li><a href="#">Accessories</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Music</li>
                                                    <li><a href="#">All Music</a></li>
                                                    <li><a href="#">International Music</a></li>
                                                    <li><a href="#">Film Songs</a></li>
                                                    <li><a href="#">Indian Classical</a></li>
                                                    <li><a href="#">Musical Instruments</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Audio &amp; Video</li>
                                                    <li><a href="#">All Audio &amp; Video</a></li>
                                                    <li><a href="#">Televisions</a></li>
                                                    <li><a href="#">Headphones</a></li>
                                                    <li><a href="#">Speakers</a></li>
                                                    <li><a href="#">Home Entertainment Systems</a></li>
                                                    <li><a href="#">MP3 &amp; Media Players</a></li>
                                                    <li><a href="#">Audio &amp; Video Accessories</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li>
                                                        <a href="#">
                                                            <span class="nav-text">Electro Home Appliances</span>
                                                            <span class="nav-subtext">Available in select cities</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Music</li>
                                                    <li><a href="#">Televisions</a></li>
                                                    <li><a href="#">Headphones</a></li>
                                                    <li><a href="#">Speakers</a></li>
                                                    <li><a href="#">Media Players</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Watches &amp; Eyewear" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Watches</li>
                                                    <li><a href="#">All Watches</a></li>
                                                    <li><a href="#">Men&#8217;s Watches</a></li>
                                                    <li><a href="#">Women&#8217;s Watches</a></li>
                                                    <li><a href="#">Premium Watches</a></li>
                                                    <li><a href="#">Deals on Watches</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Eyewear</li>
                                                    <li><a href="#">Men&#8217;s Sunglasses</a></li>
                                                    <li><a href="#">Women&#8217;s Sunglasses</a></li>
                                                    <li><a href="#">Spectacle Frames</a></li>
                                                    <li><a href="#">All Sunglasses</a></li>
                                                    <li><a href="#">Amazon Fashion</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Car, Motorbike &amp; Industrial" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <div class="yamm-content">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Car &amp; Motorbike</li>
                                                    <li><a href="#">All Cars &amp; Bikes</a></li>
                                                    <li><a href="#">Car &amp; Bike Care</a></li>
                                                    <li><a href="#">Lubricants</a></li>
                                                    <li class="nav-divider"></li>
                                                    <li class="nav-title">Shop for Bike</li>
                                                    <li><a href="#">Helmets &amp; Gloves</a></li>
                                                    <li><a href="#">Bike Parts</a></li>
                                                    <li class="nav-title">Shop for Car</li>
                                                    <li><a href="#">Air Fresheners</a></li>
                                                    <li><a href="#">Car Parts</a></li>
                                                    <li><a href="#">Tyre Accessories</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li class="nav-title">Industrial Supplies</li>
                                                    <li><a href="#">All Industrial Supplies</a></li>
                                                    <li><a href="#">Lab &amp; Scientific</a></li>
                                                    <li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
                                                    <li><a href="#">Test, Measure &amp; Inspect</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Cases" href="product-category.html">Cases</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Chargers" href="product-category.html">Chargers</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Headphone Accessories" href="product-category.html">Headphone Accessories</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Headphone Cases" href="product-category.html">Headphone Cases</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Headphones" href="product-category.html">Headphones</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Computer Accessories" href="product-category.html">Computer Accessories</a>
                                    </li>
                                    <li class="menu-item animate-dropdown ">
                                        <a title="Laptop Accessories" href="product-category.html">Laptop Accessories</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </header><!-- #masthead -->
    <nav class="navbar navbar-primary navbar-full hidden-md-down">
        <div class="container">
            <ul class="nav navbar-nav departments-menu animate-dropdown">
                <li class="nav-item dropdown ">

                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle" >Shop by Department</a>
                    <ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown">

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">COMPUTER PERIPHERALS</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">

                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">COMPUTER PERIPHERALS</li>
                                                                    <li><a href="#">Test Donot Delete</a></li>
                                                                    <li><a href="#">Ultrasonic sensors</a></li>

                                                                    </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">ELECTRONIC COMPONENTS	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">

                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">ELECTRONIC COMPONENTS</li>


                                                                    </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">BATTERIES & CHARGERS	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">

                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">BATTERIES & CHARGERS	</li>


                                                                    </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">ROBOTIC & DIY KITS	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">

                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">ROBOTIC & DIY KITS	</li>

                                                                    </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2584 dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">MULITIROTORS DRONE	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">

                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>
                                                                    <li class="nav-title">MULITIROTORS DRONE	</li>
                                                                    <li><a href="#">FLIGHT CONTROLLER</a></li>
                                                                    <li><a href="#">FRAME</a></li>
                                                                    <li><a href="#">BATTERIES & CHARGERS</a></li>
                                                                    <li><a href="#">TELEMETRY & REMOTE RX/TX</a></li>


                                                                    </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">IOT</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">IOT</li>



                                                                   </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">WIRELESS</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">WIRELESS</li>



                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">E-BIKE / E-CAR	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">E-BIKE / E-CAR	</li>



                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">BREAKOUT BOARDS	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">BREAKOUT BOARDS	</li>



                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">RASPBERRY PI	</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">RASPBERRY PI	</li>



                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">ARDUINO</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">ARDUINO</li>



                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">MOTORS</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">MOTORS</li>

                                                                    <li><a href="#">BLDC MOTORS	</a></li>
                                                                    <li><a href="#">BO MOTORS	</a></li>
                                                                    <li><a href="#">GEAR MOTORS	</a></li>
                                                                    <li><a href="#">BLDC MOTORS	</a></li>
                                                                    <li><a href="#">JHONSON GEARED MOTORS	</a></li>


                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown menu-item-2585 dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Electronic Components</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown menu-item-object-static_block">
                                    <div class="yamm-content">
                                        <div class="vc_row row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_left">
                                                            <figure class="wpb_wrapper vc_figure">
                                                                <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    {{--<img width="540" height="460" src="front/assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="megamenu-2"/>--}}
                                                                </div>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_row row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-6 col-sm-6">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <ul>

                                                                    <li class="nav-title">ELECTRONIC COMPONENTS</li>

                                                                    <li><a href="#">ELECTRONIC COMPONENTS	</a></li>

                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </li>
            </ul>
            <form class="navbar-search" method="get" action="/">
                <label class="sr-only screen-reader-text" for="search">Search for:</label>
                <div class="input-group">
                    <input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="Search for products" />
                    <div class="input-group-addon search-categories">
                        <select name='product_cat' id='product_cat' class='postform resizeselect' >
                            <option value='0' selected='selected'>All Categories</option>

                        </select>
                    </div>
                    <div class="input-group-btn">
                        <input type="hidden" id="search-param" name="post_type" value="product" />
                        <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
                    </div>
                </div>
            </form>

            <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link" data-toggle="dropdown">
                        <i class="ec ec-shopping-bag"></i>
                        <span class="cart-items-count count">


                        </span>
                        <span class="cart-items-total-price total-price"><span class="amount">


                            </span></span>
                    </a>
                   {{-- <ul class="dropdown-menu dropdown-menu-mini-cart">
                        <li>
                            <div class="widget_shopping_cart_content">

                                <ul class="cart_list product_list_widget ">


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="front/assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
                                        </a>

                                        <span class="quantity">2 × <span class="amount">£150.00</span></span>
                                    </li>


                                    <li class="mini_cart_item">
                                        <a title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="front/assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
                                        </a>

                                        <span class="quantity">1 × <span class="amount">£399.99</span></span>
                                    </li>

                                    <li class="mini_cart_item">
                                        <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
                                        <a href="single-product.html">
                                            <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="front/assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

                                        </a>

                                        <span class="quantity">1 × <span class="amount">£269.99</span></span>
                                    </li>


                                </ul><!-- end product list -->


                                <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                                <p class="buttons">
                                    <a class="button wc-forward" href="cart.html">View Cart</a>
                                    <a class="button checkout wc-forward" href="checkout.html">Checkout</a>
                                </p>


                            </div>
                        </li>
                    </ul>--}}
                </li>
            </ul>

            {{--<ul class="navbar-wishlist nav navbar-nav pull-right flip">
                <li class="nav-item">
                    <a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
                </li>
            </ul>
            <ul class="navbar-compare nav navbar-nav pull-right flip">
                <li class="nav-item">
                    <a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
                </li>
            </ul>--}}
        </div>
    </nav>

    <div id="content" class="site-content" tabindex="-1">
        <div class="container">

            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="home-v2-slider" >
                        <!-- ========================================== SECTION – HERO : END========================================= -->

                        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

                            <div class="item" style="background-image: url(/slider/ardruino-nano_red.jpg);">
                                {{--<div class="container">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-5">
                                            <div class="caption vertical-center text-left">
                                                <div class="hero-1 fadeInDown-1">
                                                    The New <br> Standard
                                                </div>

                                                <div class="hero-subtitle fadeInDown-2">
                                                    under favorable smartwatches
                                                </div>
                                                <div class="hero-v2-price fadeInDown-3">
                                                    from <br><span>$749</span>
                                                </div>
                                                <div class="hero-action-btn fadeInDown-4">
                                                    <a href="single-product.html" class="big le-button ">Start Buying</a>
                                                </div>
                                            </div><!-- /.caption -->
                                        </div>
                                    </div>
                                </div><!-- /.container -->--}}
                            </div><!-- /.item -->
                            <div class="item" style="background-image: url(/slider/raspberrypi_yellowOchre.jpg);">
                                {{--<div class="container">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-5">
                                            <div class="caption vertical-center text-left">
                                                <div class="hero-1 fadeInDown-1">
                                                    The New <br> Standard
                                                </div>

                                                <div class="hero-subtitle fadeInDown-2">
                                                    under favorable smartwatches
                                                </div>
                                                <div class="hero-v2-price fadeInDown-3">
                                                    from <br><span>$749</span>
                                                </div>
                                                <div class="hero-action-btn fadeInDown-4">
                                                    <a href="single-product.html" class="big le-button ">Start Buying</a>
                                                </div>
                                            </div><!-- /.caption -->
                                        </div>
                                    </div>
                                </div><!-- /.container -->--}}
                            </div><!-- /.item -->


                            <div class="item" style="background-image: url(/slider/ir-sensor_teal.jpg);">
                                {{--<div class="container">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-5">
                                            <div class="caption vertical-center text-left">
                                                <div class="hero-subtitle-v2 fadeInDown-1">
                                                    shop to get what you loves
                                                </div>

                                                <div class="hero-2 fadeInDown-2">
                                                    Timepieces that make a statement up to <strong>40% Off</strong>
                                                </div>

                                                <div class="hero-action-btn fadeInDown-3">
                                                    <a href="single-product.html" class="big le-button ">Start Buying</a>
                                                </div>
                                            </div><!-- /.caption -->
                                        </div>
                                    </div>
                                </div><!-- /.container -->--}}
                            </div><!-- /.item -->

                            <div class="item" style="background-image: url(/slider/bldc-motor_blue.jpg);">
                                <div class="container">
                                   {{-- <div class="row">
                                        <div class="col-md-offset-3 col-md-5">
                                            <div class="caption vertical-center text-left">
                                                <div class="hero-subtitle-v2 fadeInLeft-1">
                                                    shop to get what you loves
                                                </div>

                                                <div class="hero-2 fadeInRight-1">
                                                    Timepieces that make a statement up to <strong>40% Off</strong>
                                                </div>

                                                <div class="hero-action-btn fadeInLeft-2">
                                                    <a href="single-product.html" class="big le-button ">Start Buying</a>
                                                </div>
                                            </div><!-- /.caption -->
                                        </div>
                                    </div>--}}
                                </div><!-- /.container -->
                            </div><!-- /.item -->


                        </div><!-- /.owl-carousel -->

                        <!-- ========================================= SECTION – HERO : END ========================================= -->

                    </div><!-- /.home-v1-slider -->
                    <div class="home-v2-ads-block animate-in-view fadeIn animated" data-animation=" animated fadeIn">
                        <div class="ads-block row">
                            <div class="ad col-xs-12 col-sm-6">
                                <div class="media">
                                    <div class="media-left media-middle"><img src="{{asset('/product/1.jpg')}}" alt="" /></div>
                                    <div class="media-body media-middle">
                                        <div class="ad-text">
                                             <strong>Raspberry Pi </strong>at<br> Best Prices
                                        </div>
                                        <div class="ad-action">
                                            <a href="#">Shop now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ad col-xs-12 col-sm-6">
                                <div class="media">
                                    <div class="media-left media-middle"><img src="{{asset('/product/2.jpg')}}" alt="" /></div>
                                    <div class="media-body media-middle">
                                        <div class="ad-text">
                                            Arduino, <br>Devices<br>
                                        </div>
                                        <div class="ad-action">
                                            <a href="#">Shop now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="products-carousel-tabs animate-in-view fadeIn animated" data-animation="fadeIn">
                        <h2 class="sr-only">Product Carousel Tabs</h2>
                        <ul class="nav nav-inline">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tab-products-1" data-toggle="tab">Featured</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tab-products-2" data-toggle="tab">On Sale</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#tab-products-3" data-toggle="tab">Top Rated</a>
                            </li>
                        </ul><!-- /.nav -->

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-products-1" role="tabpanel">
                                <section class="section-products-carousel" >
                                    <div class="home-v2-owl-carousel-tabs">
                                        <div class="woocommerce columns-3">


                                            <div class="products owl-carousel home-v2-carousel-tabs products-carousel columns-3">

                                                @foreach($rfeaturedproducts as $product)
                                                <div class="product ">
                                                    <div class="product-outer">
                                                        <div class="product-inner">
                                                            <span class="loop-product-categories"><a href="#" rel="tag">{{$product->primary_group}}</a></span>
                                                            <a href="{{route('front.productdetails',$product->slug)}}">
                                                                <h3>{{$product->title}}</h3>
                                                                <div class="product-thumbnail">
                                                                    <img src="{{asset('front/assets/images/blank.gif')}}" data-echo="{{asset($product->img_small)}}" class="img-responsive" alt="">
                                                                </div>
                                                            </a>

                                                            <div class="price-add-to-cart">
                                                                        <span class="price">
                                                                            <span class="electro-price">
                                                                                 @if ($product->discount === null)
                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>      {{$product->value}}
                                                                    </span></ins>


                                                                                @else

                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>        {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                                    <del><span class="amount">

                                                                     <i class="fa fa-inr"></i>      {{$product->value}}

                                                                    </span></del>




                                                                                @endif
                                                                                <span class="amount"> </span>
                                                                            </span>
                                                                        </span>
                                                                <a rel="nofollow" href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                            </div><!-- /.price-add-to-cart -->


                                                        </div><!-- /.product-inner -->
                                                    </div><!-- /.product-outer -->
                                                </div><!-- /.product -->
                                                @endforeach


                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="tab-products-2" role="tabpanel">
                                <section class="section-products-carousel">
                                    <div class="home-v2-owl-carousel-tabs">
                                        <div class="woocommerce columns-3">


                                            <div class="products owl-carousel home-v2-carousel-tabs products-carousel columns-3">


                                                @foreach($tops as $product)
                                                    <div class="product ">
                                                        <div class="product-outer">
                                                            <div class="product-inner">
                                                                <span class="loop-product-categories"><a href="#" rel="tag">{{$product->primary_group}}</a></span>
                                                                <a href="{{route('front.productdetails',$product->slug)}}">
                                                                    <h3>{{$product->title}}</h3>
                                                                    <div class="product-thumbnail">
                                                                        <img src="{{asset('front/assets/images/blank.gif')}}" data-echo="{{asset($product->img_small)}}" class="img-responsive" alt="">
                                                                    </div>
                                                                </a>

                                                                <div class="price-add-to-cart">
                                                                        <span class="price">
                                                                            <span class="electro-price">
                                                                                 @if ($product->discount === null)
                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>      {{$product->value}}
                                                                    </span></ins>


                                                                                @else

                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>        {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                                    <del><span class="amount">

                                                                     <i class="fa fa-inr"></i>      {{$product->value}}

                                                                    </span></del>




                                                                                @endif
                                                                                <span class="amount"> </span>
                                                                            </span>
                                                                        </span>
                                                                    <a rel="nofollow" href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                                </div><!-- /.price-add-to-cart -->


                                                            </div><!-- /.product-inner -->
                                                        </div><!-- /.product-outer -->
                                                    </div><!-- /.product -->
                                                @endforeach



                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="tab-products-3" role="tabpanel">
                                <section class="section-products-carousel">
                                    <div class="home-v2-owl-carousel-tabs">
                                        <div class="woocommerce columns-3">


                                            <div class="products owl-carousel home-v2-carousel-tabs products-carousel columns-3">


                                                @foreach($recentproducts as $product)
                                                    <div class="product ">
                                                        <div class="product-outer">
                                                            <div class="product-inner">
                                                                <span class="loop-product-categories"><a href="#" rel="tag">{{$product->primary_group}}</a></span>
                                                                <a href="{{route('front.productdetails',$product->slug)}}">
                                                                    <h3>{{$product->title}}</h3>
                                                                    <div class="product-thumbnail">
                                                                        <img src="{{asset('front/assets/images/blank.gif')}}" data-echo="{{asset($product->img_small)}}" class="img-responsive" alt="">
                                                                    </div>
                                                                </a>

                                                                <div class="price-add-to-cart">
                                                                        <span class="price">
                                                                            <span class="electro-price">
                                                                                 @if ($product->discount === null)
                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>      {{$product->value}}
                                                                    </span></ins>


                                                                                @else

                                                                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>        {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                                    <del><span class="amount">

                                                                     <i class="fa fa-inr"></i>      {{$product->value}}

                                                                    </span></del>




                                                                                @endif
                                                                                <span class="amount"> </span>
                                                                            </span>
                                                                        </span>
                                                                    <a rel="nofollow" href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                                </div><!-- /.price-add-to-cart -->


                                                            </div><!-- /.product-inner -->
                                                        </div><!-- /.product-outer -->
                                                    </div><!-- /.product -->
                                                @endforeach
                                            </div><!-- /.products -->
                                        </div>
                                    </div>
                                </section>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </section><!-- /.products-carousel-tabs -->
                    {{--<section class=" section-onsale-product-carousel" data-animation="fadeIn">

                        <header>
                            <h1 class="h1">Deals of the week</h1>
                        </header>
                        <div class="owl-nav">
                            <a href="#onsale-products-carousel-prev" data-target="#onsale-products-carousel-57176fb23fad9" class="slider-prev"><i class="fa fa-angle-left"></i>Previous Deal</a>
                            <a href="#onsale-products-carousel-next" data-target="#onsale-products-carousel-57176fb23fad9" class="slider-next">Next Deal<i class="fa fa-angle-right"></i></a>
                        </div>
                        <div id="onsale-products-carousel-57176fb23fad9">
                            <div class="onsale-product-carousel owl-carousel">
                                <div class="onsale-product">
                                    <div class="onsale-product-thumbnails">


                                        <div class="savings">
                                                    <span class="savings-text">
                                                        Save <span class="amount">&#36;20.00</span>
                                                    </span>
                                        </div>


                                        <div class="images"><a href="single-product.html" itemprop="image" class="woocommerce-main-image" title=""><img width="600" height="600" src="front/assets/images/deals/1.jpg" class="wp-post-image" alt="GamePad" title="GamePad"/></a>
                                            <div class="thumbnails columns-3">
                                                <a href="single-product.html" class="first" title=""><img width="180" height="180" src="front/assets/images/deals/1-1.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad" title="GamePad"/></a>
                                                <a href="single-product.html" class="" title=""><img width="180" height="180" src="front/assets/images/deals/1-2.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad2" title="GamePad2" /></a>
                                                <a href="single-product.html" class="last" title=""><img width="180" height="180" src="front/assets/images/deals/1-3.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad3" title="GamePad3" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="onsale-product-content">

                                        <a href="single-product.html"><h3>Game Console Controller <br/>+ USB 3.0 Cable</h3></a>
                                        <span class="price"><span class="electro-price"><ins><span class="amount">&#36;79.00</span></ins> <del><span class="amount">&#36;99.00</span></del></span></span>
                                        <div class="deal-progress">
                                            <div class="deal-stock">
                                                <span class="stock-sold">Already Sold: <strong>2</strong></span>
                                                <span class="stock-available">Available: <strong>26</strong></span>
                                            </div>
                                            <div class="progress">
                                                <span class="progress-bar" style="width:8%">8</span>
                                            </div>
                                        </div>
                                        <div class="deal-countdown-timer">
                                            <div class="marketing-text text-xs-center">Hurry Up! Offer ends in:</div>
                                            <span class="deal-end-date" style="display:none;">2016-12-31</span>
                                            <div id="deal-countdown" class="countdown"></div>
                                            <script>
                                                // set the date we're counting down to
                                                var deal_end_date = document.querySelector(".deal-end-date").textContent;
                                                var target_date = new Date( deal_end_date ).getTime();

                                                // variables for time units
                                                var days, hours, minutes, seconds;

                                                // get tag element
                                                var countdown = document.getElementById( 'deal-countdown' );

                                                // update the tag with id "countdown" every 1 second
                                                setInterval( function () {

                                                    // find the amount of "seconds" between now and target
                                                    var current_date = new Date().getTime();
                                                    var seconds_left = (target_date - current_date) / 1000;

                                                    // do some time calculations
                                                    days = parseInt(seconds_left / 86400);
                                                    seconds_left = seconds_left % 86400;

                                                    hours = parseInt(seconds_left / 3600);
                                                    seconds_left = seconds_left % 3600;

                                                    minutes = parseInt(seconds_left / 60);
                                                    seconds = parseInt(seconds_left % 60);

                                                    // format countdown string + set tag value
                                                    countdown.innerHTML = '<span data-value="' + days + '" class="days"><span class="value">' + days +  '</span><b>Days</b></span><span class="hours"><span class="value">' + hours + '</span><b>Hours</b></span><span class="minutes"><span class="value">'
                                                        + minutes + '</span><b>Mins</b></span><span class="seconds"><span class="value">' + seconds + '</span><b>Secs</b></span>';

                                                }, 1000 );
                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <div class="onsale-product">
                                    <div class="onsale-product-thumbnails">


                                        <div class="savings">
                                                    <span class="savings-text">
                                                        Save <span class="amount">&#36;0.00</span>
                                                    </span>
                                        </div>


                                        <div class="images"><a href="single-product.html" itemprop="image" class="woocommerce-main-image" title=""><img width="600" height="600" src="front/assets/images/deals/2.jpg" class="wp-post-image" alt="GamePad" title="GamePad"/></a>
                                            <div class="thumbnails columns-3">
                                                <a href="single-product.html" class="first" title=""><img width="180" height="180" src="front/assets/images/deals/2-1.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad" title="GamePad"/></a>
                                                <a href="single-product.html" class="" title=""><img width="180" height="180" src="front/assets/images/deals/2-2.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad2" title="GamePad2" /></a>
                                                <a href="single-product.html" class="last" title=""><img width="180" height="180" src="front/assets/images/deals/2-3.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad3" title="GamePad3" /></a>
                                                <a href="single-product.html" class="last" title=""><img width="180" height="180" src="front/assets/images/deals/2-4.jpg" class="attachment-shop_thumbnail size-shop_thumbnail" alt="GamePad3" title="GamePad3" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="onsale-product-content">

                                        <a href="single-product.html"><h3>Ultra Wireless S50 Headphones S50 with Bluetooth</h3></a>
                                        <span class="price"><span class="electro-price"><ins><span class="amount">&#36;1,215.00</span></ins> <del><span class="amount">&#36;2,299.00</span></del></span></span>
                                        <div class="deal-progress">
                                            <div class="deal-stock">
                                                <span class="stock-sold">Already Sold: <strong>0</strong></span>
                                                <span class="stock-available">Available: <strong>30</strong></span>
                                            </div>
                                            <div class="progress">
                                                <span class="progress-bar" style="width:0%">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>--}}





                    <section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">

                        <header>

                            <h2 class="h1">Best Sellers</h2>

                            <ul class="nav nav-inline">

                                <li class="nav-item active"><span class="nav-link">TOP 20</span></li>

                                <li class="nav-item"><a class="nav-link"
                                                        href="{{route('front.category.primary','computer-peripherals')}}">COMPUTER PERIPHERALS</a></li>

                                <li class="nav-item"><a class="nav-link" style="text-transform: initial" href="{{route('front.category.primary','batteries-&-chargers')}}">BATTERIES & CHARGERS</a></li>

                                <li class="nav-item"><a style="text-transform: initial" class="nav-link" href="{{route('front.category.primary','robotic-&-diy-kits')}}">ROBOTIC & DIY KITS</a></li>
                            </ul>
                        </header>

                        <div id="home-v1-product-cards-careousel">
                            <div class="woocommerce columns-2 home-v1-product-cards-carousel product-cards-carousel owl-carousel">

                                <ul class="products columns-2">


                                    @foreach($resentproductstwo as $product)
                                    <li class="product product-card ">

                                        <div class="product-outer">
                                            <div class="media product-inner">

                                                <a class="media-left"
                                                   href="{{route('front.productdetails',$product->slug)}}"
                                                   title="{{$product->title}}">
                                                    <img class="media-object wp-post-image img-responsive"
                                                         src="front/assets/images/blank.gif"
                                                         data-echo="{{asset($product->img_small)}}" alt="">
                                                </a>

                                                <div class="media-body">
                                                            <span class="loop-product-categories">
                                                                <a href="#" rel="tag">
                                                                      {{$product->primary_group}}</a>

                                                            </span>

                                                    <a href="{{route('front.productdetails',$product->slug)}}">
                                                        <h3>{{$product->title}}</h3>
                                                    </a>

                                                    <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                         @if ($product->discount === null)
                                                                            <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>      {{$product->value}}
                                                                    </span></ins>


                                                                        @else

                                                                            <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>        {{$product->after_discount_amt}}
                                                                    </span></ins>



                                                                            <del><span class="amount">

                                                                     <i class="fa fa-inr"></i>      {{$product->value}}

                                                                    </span></del>




                                                                        @endif
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>

                                                        <a href="{{route('front.productdetails',$product->slug)}}" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->



                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->

                                    </li><!-- /.products -->


                                    @endforeach



                                </ul>
                              {{--  <ul class="products columns-2">
                                    <li class="product product-card first">

                                        <div class="product-outer">
                                            <div class="media product-inner">

                                                <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB">
                                                    <img class="media-object wp-post-image img-responsive" src="front/assets/images/blank.gif" data-echo="front/assets/images/product-cards/6.jpg" alt="">

                                                </a>

                                                <div class="media-body">
                                                            <span class="loop-product-categories">
                                                                <a href="product-category.html" rel="tag">Peripherals</a>
                                                            </span>

                                                    <a href="single-product.html">
                                                        <h3>External SSD USB 3.1  750 GB</h3>
                                                    </a>

                                                    <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $3,788.00</span></ins>
                                                                        <del><span class="amount">$4,780.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>

                                                        <a href="cart.html" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">

                                                            <a href="#" class="add_to_wishlist">
                                                                Wishlist</a>

                                                            <a href="#" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->

                                    </li><!-- /.products -->
                                    <li class="product product-card last">

                                        <div class="product-outer">
                                            <div class="media product-inner">

                                                <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB">
                                                    <img class="media-object wp-post-image img-responsive" src="front/assets/images/blank.gif" data-echo="front/assets/images/product-cards/3.jpg" alt="">

                                                </a>

                                                <div class="media-body">
                                                            <span class="loop-product-categories">
                                                                <a href="product-category.html" rel="tag">Smartphones</a>
                                                            </span>

                                                    <a href="single-product.html">
                                                        <h3>Notebook Purple G752VT-T7008T</h3>
                                                    </a>

                                                    <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $3,788.00</span></ins>
                                                                        <del><span class="amount">$4,780.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>

                                                        <a href="cart.html" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">

                                                            <a href="#" class="add_to_wishlist">
                                                                Wishlist</a>

                                                            <a href="#" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->

                                    </li><!-- /.products -->
                                    <li class="product product-card first">

                                        <div class="product-outer">
                                            <div class="media product-inner">

                                                <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB">
                                                    <img class="media-object wp-post-image img-responsive" src="front/assets/images/blank.gif" data-echo="front/assets/images/product-cards/2.jpg" alt="">

                                                </a>

                                                <div class="media-body">
                                                            <span class="loop-product-categories">
                                                                <a href="product-category.html" rel="tag">Headphone Cases</a>
                                                            </span>

                                                    <a href="single-product.html">
                                                        <h3>Universal Headphones Case in Black</h3>
                                                    </a>

                                                    <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> </span></ins>
                                                                        <span class="amount"> $1500</span>
                                                                    </span>
                                                                </span>

                                                        <a href="cart.html" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">

                                                            <a href="#" class="add_to_wishlist">
                                                                Wishlist</a>

                                                            <a href="#" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->

                                    </li><!-- /.products -->
                                    <li class="product product-card last">

                                        <div class="product-outer">
                                            <div class="media product-inner">

                                                <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB">
                                                    <img class="media-object wp-post-image img-responsive" src="front/assets/images/blank.gif" data-echo="front/assets/images/product-cards/1.jpg" alt="">

                                                </a>

                                                <div class="media-body">
                                                            <span class="loop-product-categories">
                                                                <a href="product-category.html" rel="tag">Smartphones</a>
                                                            </span>

                                                    <a href="single-product.html">
                                                        <h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
                                                    </a>

                                                    <div class="price-add-to-cart">
                                                                <span class="price">
                                                                    <span class="electro-price">
                                                                        <ins><span class="amount"> $3,788.00</span></ins>
                                                                        <del><span class="amount">$4,780.00</span></del>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                </span>

                                                        <a href="cart.html" class="button add_to_cart_button">Add to cart</a>
                                                    </div><!-- /.price-add-to-cart -->

                                                    <div class="hover-area">
                                                        <div class="action-buttons">

                                                            <a href="#" class="add_to_wishlist">
                                                                Wishlist</a>

                                                            <a href="#" class="add-to-compare-link">Compare</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->

                                    </li><!-- /.products -->
                                </ul>--}}
                            </div>
                        </div><!-- #home-v1-product-cards-careousel -->

                    </section>
                    {{--<div class="home-v2-banner-block animate-in-view fadeIn animated" data-animation=" animated fadeIn">
                        <div class="home-v2-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px">
                            <a href="#">
                                <img src="info.jpeg" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>--}}




                   {{-- <section class="home-v2-categories-products-carousel section-products-carousel animate-in-view fadeIn animated animation" data-animation="fadeIn">


                        <header>

                            <h2 class="h1">Laptops &amp; Computers</h2>

                            <div class="owl-nav">
                                <a href="#products-carousel-prev" data-target="#products-carousel-57176fb2c4230" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                                <a href="#products-carousel-next" data-target="#products-carousel-57176fb2c4230" class="slider-next"><i class="fa fa-angle-right"></i></a>
                            </div>

                        </header>


                        <div id="products-carousel-57176fb2c4230">
                            <div class="woocommerce">
                                <div class="products owl-carousel home-v2-categories-products products-carousel columns-6">


                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Laptop Yoga 21 80JH0035GE  W8.1 (Copy)</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/5.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $1,999.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/1.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $1,999.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Notebook Purple G952VX-T7008T</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/4.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $1,999.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Notebook Widescreen Z51-70  40K6013UPB</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/3.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> $1,999.00</span></ins>
                                                                    <del><span class="amount">$2,299.00</span></del>
                                                                    <span class="amount"> </span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Notebook Purple G952VX-T7008T</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/3.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $1,999.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/2.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $1,999.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/2.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> $1,999.00</span></ins>
                                                                    <del><span class="amount">$2,299.00</span></del>
                                                                    <span class="amount"> </span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->



                                    <div class="product">
                                        <div class="product-outer">
                                            <div class="product-inner">
                                                <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                <a href="single-product.html">
                                                    <h3>Smartphone 6S 128GB LTE</h3>
                                                    <div class="product-thumbnail">
                                                        <img src="front/assets/images/blank.gif" data-echo="front/assets/images/product-category/6.jpg" class="img-responsive" alt="">
                                                    </div>
                                                </a>

                                                <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount"> </span></ins>
                                                                    <span class="amount"> $200.00</span>
                                                                </span>
                                                            </span>
                                                    <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                </div><!-- /.price-add-to-cart -->

                                                <div class="hover-area">
                                                    <div class="action-buttons">

                                                        <a href="#" rel="nofollow" class="add_to_wishlist"> Wishlist</a>

                                                        <a href="compare.html" class="add-to-compare-link"> Compare</a>
                                                    </div>
                                                </div>
                                            </div><!-- /.product-inner -->
                                        </div><!-- /.product-outer -->
                                    </div><!-- /.products -->


                                </div>
                            </div>
                        </div>
                    </section>
                --}}



                </main><!-- #main -->
            </div><!-- #primary -->

            <div id="sidebar" class="sidebar" role="complementary" style="margin-top: 778px;">
                <aside class="widget widget_text">
                    <div class="textwidget">
                        <a href="#">
                            <img src="info.jpeg" alt="Banner">
                        </a>
                    </div>
                </aside>


                <aside class="widget widget_products">
                    <h3 class="widget-title">Latest Products</h3>
                    <ul class="product_list_widget">

                        @foreach ($recentproducts as $product)


                        <li>
                            <a href="{{route('front.productdetails',$product->slug)}}" title="Notebook Black Spire V Nitro  VN7-591G">
                                <img width="180" height="180" src="{{asset($product->img_small)}}"
                                     alt="" class="wp-post-image"/><span class="product-title">
                                    {{$product->title}}</span>
                            </a>


                            <span class="electro-price">
                                @if ($product->discount === null)
                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>      {{$product->value}}
                                                                    </span></ins>


                                @else

                                    <ins><span class="amount">
                                                                         <i class="fa fa-inr"></i>        {{$product->after_discount_amt}}
                                                                    </span></ins>



                                    <del><span class="amount">

                                                                     <i class="fa fa-inr"></i>      {{$product->value}}

                                                                    </span></del>




                                @endif


                            </span>







                        </li>


                        @endforeach


                    </ul>
                </aside>
                <aside id="electro_features_block_widget-2" class="widget widget_electro_features_block_widget">
                    <div class="features-list columns-1">
                        <div class="feature">
                            <div class="media">
                                <div class="media-left media-middle feature-icon">
                                    <i class="ec ec-transport"></i>
                                </div>
                                <div class="media-body media-middle feature-text">
                                    <strong>Free Delivery</strong> from  <i class="fa fa-inr"></i>   500
                                </div>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="media">
                                <div class="media-left media-middle feature-icon">
                                    <i class="ec ec-customers"></i>
                                </div>
                                <div class="media-body media-middle feature-text">
                                    <strong>99% Positive</strong> Feedbacks
                                </div>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="media">
                                <div class="media-left media-middle feature-icon">
                                    <i class="ec ec-returning"></i>
                                </div>
                                <div class="media-body media-middle feature-text">
                                    <strong>30 days</strong> for free return
                                </div>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="media">
                                <div class="media-left media-middle feature-icon">
                                    <i class="ec ec-payment"></i>
                                </div>
                                <div class="media-body media-middle feature-text">
                                    <strong>Payment</strong> Secure System
                                </div>
                            </div>
                        </div>
                        <div class="feature">
                            <div class="media">
                                <div class="media-left media-middle feature-icon">
                                    <i class="ec ec-tag"></i>
                                </div>
                                <div class="media-body media-middle feature-text">
                                    <strong>Only Best</strong> Brands
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
                <aside class="widget widget_electro_products_carousel_widget">
                    <section class="section-products-carousel" >


                        <header>

                            <h1>Featured Products</h1>

                            <div class="owl-nav">
                                <a href="#products-carousel-prev" data-target="#products-carousel-57176fb2dc4a8" class="slider-prev"><i class="fa fa-angle-left"></i></a>
                                <a href="#products-carousel-next" data-target="#products-carousel-57176fb2dc4a8" class="slider-next"><i class="fa fa-angle-right"></i></a>
                            </div>

                        </header>


                        <div id="products-carousel-57176fb2dc4a8">
                            <div class="products owl-carousel  products-carousel-widget columns-1">

                                @foreach ($rfeaturedproducts as $product)



                                <div class="product-carousel-alt">
                                    <a href="{{route('front.productdetails',$product->slug)}}">
                                        <div class="product-thumbnail">

                                            <img width="250" height="232"
                                              src="{{asset($product->img_small)}}"
                                                 class="attachment-shop_catalog size-shop_catalog wp-post-image"
                                                 alt="Smartwatch2" /></div>
                                    </a>

                                    <span class="loop-product-categories"><a href="single-product.html" rel="tag">
                                                    {{$product->primary_group}}</a></span>
                                    <a href="single-product.html"><h3>{{$product->title}}</h3></a>
                                    <span class="price"><span class="electro-price"><span class="amount">
                                                   <i class="fa fa-inr"></i>      {{$product->value}}

                                            </span></span></span>
                                </div>

                                @endforeach
                            </div>
                        </div>
                    </section>
                </aside>
              x
            </div>

        </div><!-- .container -->
    </div><!-- #content -->



    @endsection