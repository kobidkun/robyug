<header id="masthead" class="site-header header-v2">
    <div class="container hidden-md-down">
        <div class="row">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="home.html" class="header-logo-link">
                    <img src="{{asset('front/assets/images/logo.png')}}">
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="primary-nav animate-dropdown">
                <div class="clearfix">
                    <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
                        &#9776;
                    </button>
                </div>

                <div class="collapse navbar-toggleable-xs" id="default-header">
                    <nav>
                        <ul id="menu-main-menu" class="nav nav-inline yamm">
                            <li class="menu-item menu-item-has-children animate-dropdown dropdown"><a title="Home" href="shop.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Home</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown  "><a title="Home v1" href="home.html">Home</a></li>

                                </ul>
                            </li>
                            <li class="menu-item animate-dropdown"><a title="About Us" href="about.html">About Us</a></li>

                            <li class="menu-item menu-item-has-children animate-dropdown dropdown"><a title="Blog" href="blog.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Blog</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown"><a title="Blog v1" href="blog-v1.html">Blog v1</a></li>
                                    <li class="menu-item animate-dropdown"><a title="Blog v2" href="blog-v2.html">Blog v2</a></li>
                                    <li class="menu-item animate-dropdown"><a title="Blog v3" href="blog-v3.html">Blog v3</a></li>
                                </ul>
                            </li>
                            <li class="yamm-fw menu-item menu-item-has-children animate-dropdown dropdown">
                                <a title="Pages" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Pages</a>
                                <ul role="menu" class=" dropdown-menu">
                                    <li class="menu-item animate-dropdown">
                                        <div class="yamm-content" style="display:inline-block; width: 100%;">
                                            <div class="row">
                                                <div class="wpb_column vc_column_container col-sm-4">
                                                    <div class="vc_column-inner ">
                                                        <div class="wpb_wrapper">
                                                            <div class="vc_wp_custommenu wpb_content_element">
                                                                <div class="widget widget_nav_menu">
                                                                    <div class="menu-pages-menu-1-container">
                                                                        <ul id="menu-pages-menu-1" class="menu">
                                                                            <li class="nav-title menu-item"><a href="#">Home &#038; Static Pages</a></li>
                                                                            <li class="menu-item"><a href="home.html">Home v1</a></li>
                                                                            <li class="menu-item current-menu-item current_page_item"><a href="home-v2.html">Home v2</a></li>
                                                                            <li class="menu-item"><a href="home-v3.html">Home v3</a></li>
                                                                            <li class="menu-item"><a href="about.html">About</a></li>
                                                                            <li class="menu-item"><a href="contact-v2.html">Contact v2</a></li>
                                                                            <li class="menu-item"><a href="contact-v1.html">Contact v1</a></li>
                                                                            <li class="menu-item"><a href="faq.html">FAQ</a></li>
                                                                            <li class="menu-item"><a href="store-directory.html">Store Directory</a></li>
                                                                            <li class="menu-item"><a href="terms-and-conditions.html">Terms and Conditions</a></li>
                                                                            <li class="menu-item"><a href="404.html">404</a></li>
                                                                            <li class="nav-title menu-item"><a href="#">Product Categories</a></li>
                                                                            <li class="menu-item"><a href="cat-3-col.html">3 Column Sidebar</a></li>
                                                                            <li class="menu-item"><a href="cat-4-col.html">4 Column Sidebar</a></li>
                                                                            <li class="menu-item"><a href="cat-4-fw.html">4 Column Full width</a></li>
                                                                            <li class="menu-item"><a href="product-category-6-column.html">6 Columns Full width</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container col-sm-4">
                                                    <div class="vc_column-inner ">
                                                        <div class="wpb_wrapper">
                                                            <div class="vc_wp_custommenu wpb_content_element">
                                                                <div class="widget widget_nav_menu">
                                                                    <div class="menu-pages-menu-2-container">
                                                                        <ul id="menu-pages-menu-2" class="menu">
                                                                            <li class="nav-title menu-item"><a href="#">Shop Pages</a></li>
                                                                            <li class="menu-item"><a href="shop.html#grid">Shop Grid</a></li>
                                                                            <li class="menu-item"><a href="shop.html#grid-extended">Shop Grid Extended</a></li>
                                                                            <li class="menu-item"><a href="shop.html#list-view">Shop List View</a></li>
                                                                            <li class="menu-item"><a href="shop.html#list-view-small">Shop List View Small</a></li>
                                                                            <li class="menu-item"><a href="shop.html">Shop Left Sidebar</a></li>
                                                                            <li class="menu-item"><a href="shop-fw.html">Shop Full width</a></li>
                                                                            <li class="menu-item"><a href="shop-right-side-bar.html">Shop Right Sidebar</a></li>
                                                                            <li class="nav-title menu-item"><a href="#">Blog Pages</a></li>
                                                                            <li class="menu-item"><a href="blog-v1.html">Blog v1</a></li>
                                                                            <li class="menu-item"><a href="blog-v3.html">Blog v3</a></li>
                                                                            <li class="menu-item"><a href="blog-v2.html">Blog v2</a></li>
                                                                            <li class="menu-item"><a href="blog-fw.html">Blog Full Width</a></li>
                                                                            <li class="menu-item"><a href="blog-single.html">Single Blog Post</a></li>

                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container col-sm-4">
                                                    <div class="vc_column-inner ">
                                                        <div class="wpb_wrapper">
                                                            <div class="vc_wp_custommenu wpb_content_element">
                                                                <div class="widget widget_nav_menu">
                                                                    <div class="menu-pages-menu-3-container">
                                                                        <ul id="menu-pages-menu-3" class="menu">
                                                                            <li class="nav-title menu-item"><a href="single-product.html">Single Product Pages</a></li>
                                                                            <li class="menu-item"><a href="single-product-extended.html">Single Product Extended</a></li>
                                                                            <li class="menu-item"><a href="single-product.html">Single Product Fullwidth</a></li>
                                                                            <li class="menu-item"><a href="single-product-sidebar.html">Single Product Sidebar</a></li>
                                                                            <li class="menu-item"><a href="single-product-sidebar-accessories.html">Single Product Sidebar Accessories </a></li>
                                                                            <li class="menu-item"><a href="single-product-sidebar-specification.html">Single Product Sidebar Specification </a></li>
                                                                            <li class="menu-item"><a href="single-product-sidebar-reviews.html">Single Product Sidebar Reviews </a></li>
                                                                            <li class="nav-title menu-item"><a href="#">Ecommerce Pages</a></li>
                                                                            <li class="menu-item"><a href="shop.html">Shop</a></li>
                                                                            <li class="menu-item"><a href="cart.html">Cart</a></li>
                                                                            <li class="menu-item"><a href="checkout.html">Checkout</a></li>
                                                                            <li class="menu-item"><a href="my-account.html">My Account</a></li>
                                                                            <li class="menu-item"><a href="compare.html">Compare</a></li>
                                                                            <li class="menu-item"><a href="wishlist.html">Wishlist</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item"><a title="Features" href="#">Features</a></li>
                            <li class="menu-item"><a title="Contact Us" href="#">Contact Us</a></li>
                        </ul>
                    </nav>

                </div>
            </div>

            <div class="header-support-info">
                <div class="media">
                    <span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
                    <div class="media-body">
                        <span class="support-number"><strong>Support</strong> (+800) 856 800 604</span><br/>
                        <span class="support-email">Email: info@electro.com</span>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div>

    <div class="container hidden-lg-up">
        <div class="handheld-header">

            <!-- ============================================================= Header Logo ============================================================= -->
            <div class="header-logo">
                <a href="#" class="header-logo-link">
                    <img src="{{asset('front/assets/images/logo.png')}}">
                </a>
            </div>
            <!-- ============================================================= Header Logo : End============================================================= -->

            <div class="handheld-navigation-wrapper">
                <div class="handheld-navbar-toggle-buttons clearfix">
                    <button class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <button class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
                        <i class="ec ec-close-remove"></i>
                    </button>
                </div>
                <div class="handheld-navigation hidden-lg-up" id="default-hh-header">
                    <span class="ehm-close">Close</span>
                    <ul id="menu-all-departments-menu-1" class="nav nav-inline yamm">
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Value of the Day" href="#">Value of the Day</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a>
                        </li>
                        <li class="highlight menu-item animate-dropdown ">
                            <a title="New Arrivals" href="home-v3-full-color.html">New Arrivals</a>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Computers &amp; Accessories" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                <li><a href="#">All Computers &amp; Accessories</a></li>
                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                                <li><a href="#">Printers &amp; Ink</a></li>
                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
                                                <li><a href="#">Computer Accessories</a></li>
                                                <li><a href="#">Software</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Office &amp; Stationery</li>
                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                <li><a href="#">Pens &amp; Writing</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Cameras, Audio &amp; Video" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Cameras &amp; Photography</li>
                                                <li><a href="#">All Cameras &amp; Photography</a></li>
                                                <li><a href="#">Digital SLRs</a></li>
                                                <li><a href="#">Point &amp; Shoot Cameras</a></li>
                                                <li><a href="#">Lenses</a></li>
                                                <li><a href="#">Camera Accessories</a></li>
                                                <li><a href="#">Security &amp; Surveillance</a></li>
                                                <li><a href="#">Binoculars &amp; Telescopes</a></li>
                                                <li><a href="#">Camcorders</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Headphones &amp; Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Mobiles &amp; Tablets" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Mobiles &amp; Tablets</li>
                                                <li><a href="#">All Mobile Phones</a></li>
                                                <li><a href="#">Smartphones</a></li>
                                                <li><a href="#">Android Mobiles</a></li>
                                                <li><a href="#">Windows Mobiles</a></li>
                                                <li><a href="#">Refurbished Mobiles</a></li>
                                                <li class="nav-divider"></li>
                                                <li><a href="#">All Mobile Accessories</a></li>
                                                <li><a href="#">Cases &amp; Covers</a></li>
                                                <li><a href="#">Screen Protectors</a></li>
                                                <li><a href="#">Power Banks</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">All Electronics</span>
                                                        <span class="nav-subtext">Discover more products</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title"></li>
                                                <li><a href="#">All Tablets</a></li>
                                                <li><a href="#">Tablet Accessories</a></li>
                                                <li><a href="#">Landline Phones</a></li>
                                                <li><a href="#">Wearable Devices</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Movies, Music &amp; Video Games" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Movies &amp; TV Shows</li>
                                                <li><a href="#">All Movies &amp; TV Shows</a></li>
                                                <li><a href="#">Blu-ray</a></li>
                                                <li><a href="#">All English</a></li>
                                                <li><a href="#">All Hindi</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Video Games</li>
                                                <li><a href="#">All Consoles, Games &amp; Accessories</a></li>
                                                <li><a href="#">PC Games</a></li>
                                                <li><a href="#">Pre-orders &amp; New Releases</a></li>
                                                <li><a href="#">Consoles</a></li>
                                                <li><a href="#">Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">All Music</a></li>
                                                <li><a href="#">International Music</a></li>
                                                <li><a href="#">Film Songs</a></li>
                                                <li><a href="#">Indian Classical</a></li>
                                                <li><a href="#">Musical Instruments</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="TV &amp; Audio" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Audio &amp; Video</li>
                                                <li><a href="#">All Audio &amp; Video</a></li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Home Entertainment Systems</a></li>
                                                <li><a href="#">MP3 &amp; Media Players</a></li>
                                                <li><a href="#">Audio &amp; Video Accessories</a></li>
                                                <li class="nav-divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="nav-text">Electro Home Appliances</span>
                                                        <span class="nav-subtext">Available in select cities</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Music</li>
                                                <li><a href="#">Televisions</a></li>
                                                <li><a href="#">Headphones</a></li>
                                                <li><a href="#">Speakers</a></li>
                                                <li><a href="#">Media Players</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Watches &amp; Eyewear" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Watches</li>
                                                <li><a href="#">All Watches</a></li>
                                                <li><a href="#">Men&#8217;s Watches</a></li>
                                                <li><a href="#">Women&#8217;s Watches</a></li>
                                                <li><a href="#">Premium Watches</a></li>
                                                <li><a href="#">Deals on Watches</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Eyewear</li>
                                                <li><a href="#">Men&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Women&#8217;s Sunglasses</a></li>
                                                <li><a href="#">Spectacle Frames</a></li>
                                                <li><a href="#">All Sunglasses</a></li>
                                                <li><a href="#">Amazon Fashion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Car, Motorbike &amp; Industrial" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown">
                                    <div class="yamm-content">
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Car &amp; Motorbike</li>
                                                <li><a href="#">All Cars &amp; Bikes</a></li>
                                                <li><a href="#">Car &amp; Bike Care</a></li>
                                                <li><a href="#">Lubricants</a></li>
                                                <li class="nav-divider"></li>
                                                <li class="nav-title">Shop for Bike</li>
                                                <li><a href="#">Helmets &amp; Gloves</a></li>
                                                <li><a href="#">Bike Parts</a></li>
                                                <li class="nav-title">Shop for Car</li>
                                                <li><a href="#">Air Fresheners</a></li>
                                                <li><a href="#">Car Parts</a></li>
                                                <li><a href="#">Tyre Accessories</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul>
                                                <li class="nav-title">Industrial Supplies</li>
                                                <li><a href="#">All Industrial Supplies</a></li>
                                                <li><a href="#">Lab &amp; Scientific</a></li>
                                                <li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
                                                <li><a href="#">Test, Measure &amp; Inspect</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item menu-item-has-children animate-dropdown dropdown">
                            <a title="Accessories" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
                            <ul role="menu" class=" dropdown-menu">
                                <li class="menu-item animate-dropdown ">
                                    <a title="Cases" href="#">Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Chargers" href="#">Chargers</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Accessories" href="#">Headphone Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphone Cases" href="#">Headphone Cases</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Headphones" href="#">Headphones</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Computer Accessories" href="#">Computer Accessories</a>
                                </li>
                                <li class="menu-item animate-dropdown ">
                                    <a title="Laptop Accessories" href="#">Laptop Accessories</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header><!-- #masthead -->