@extends('base')
@section('content')

    <section class="sub-bnr" data-stellar-background-ratio="0.5" style="background-position: 0% -70.5px;">
        <div class="position-center-center">
            <div class="container">
                <h4>The Best Shop Display Collection</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Pages</a></li>
                    <li class="active">Display</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="shop-page padding-top-100 padding-bottom-100">
        <div class="container-full">
            <div class="row">

                <!-- Shop SideBar -->
                <div class="col-md-2">
                    <div class="shop-sidebar">

                        <!-- Category -->
                        <h5 class="shop-tittle margin-bottom-30">Category</h5>
                        <ul class="shop-cate">
                            <li><a href="#."> Chair <span>24</span></a></li>
                            <li><a href="#."> Bag <span>122</span></a></li>
                            <li><a href="#."> Soffa <span>09</span></a></li>
                            <li><a href="#."> Bed <span>12</span></a></li>
                            <li><a href="#."> Shoes <span>98</span></a></li>
                            <li><a href="#."> Table <span>34</span></a></li>
                            <li><a href="#."> Bedsheets <span>23</span></a></li>
                            <li><a href="#."> Curtains <span>43</span></a></li>
                            <li><a href="#."> TV Cabinets <span>12</span></a></li>
                            <li><a href="#."> Clocks <span>18</span></a></li>
                            <li><a href="#."> Towels <span>25</span></a></li>
                        </ul>

                        <!-- FILTER BY PRICE -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-30">Filter By Price</h5>

                        <!-- TAGS -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-30">Filter By Colors</h5>
                        <ul class="colors">
                            <li><a href="#." style="background:#958170;"></a></li>
                            <li><a href="#." style="background:#c9a688;"></a></li>
                            <li><a href="#." style="background:#c9c288;"></a></li>
                            <li><a href="#." style="background:#a7c988;"></a></li>
                            <li><a href="#." style="background:#9ed66b;"></a></li>
                            <li><a href="#." style="background:#6bd6b1;"></a></li>
                            <li><a href="#." style="background:#82c2dc;"></a></li>
                            <li><a href="#." style="background:#8295dc;"></a></li>
                            <li><a href="#." style="background:#9b82dc;"></a></li>
                            <li><a href="#." style="background:#dc82d9;"></a></li>
                            <li><a href="#." style="background:#dc82a2;"></a></li>
                            <li><a href="#." style="background:#e04756;"></a></li>
                            <li><a href="#." style="background:#f56868;"></a></li>
                            <li><a href="#." style="background:#eda339;"></a></li>
                            <li><a href="#." style="background:#edd639;"></a></li>
                            <li><a href="#." style="background:#daed39;"></a></li>
                            <li><a href="#." style="background:#a3ed39;"></a></li>
                            <li><a href="#." style="background:#f56868;"></a></li>
                        </ul>

                        <!-- TAGS -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-30">Papular Tags</h5>
                        <ul class="shop-tags">
                            <li><a href="#.">Towels</a></li>
                            <li><a href="#.">Chair</a></li>
                            <li><a href="#.">Bedsheets</a></li>
                            <li><a href="#.">Shoe</a></li>
                            <li><a href="#.">Curtains</a></li>
                            <li><a href="#.">Clocks</a></li>
                            <li><a href="#.">TV Cabinets</a></li>
                            <li><a href="#.">Best Seller</a></li>
                            <li><a href="#.">Top Selling</a></li>
                        </ul>

                        <!-- BRAND -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-30">Brands</h5>
                        <ul class="shop-cate">
                            <li><a href="#.">G-Furniture</a></li>
                            <li><a href="#.">BigYellow</a></li>
                            <li><a href="#.">WoodenBazaar</a></li>
                            <li><a href="#.">GreenWoods</a></li>
                            <li><a href="#.">Hot-n-Fire </a></li>
                        </ul>

                        <!-- SIDE BACR BANER -->
                        <div class="side-bnr margin-top-50" style="padding: 100px;"> <img class="img-responsive" src="images/sidebar-bnr.jpg" alt="">
                            <div class="position-center-center">
                                <div class="bnr-text">look hot with style</div>
                                <span class="price"><small>$</small>299</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Item Content -->
                <div class="col-md-10">
                    <div class="sidebar-layout">

                        <!-- Item Filter -->
                        <div class="item-fltr">
                            <!-- short-by -->
                            <div class="short-by"> Showing 1–10 of 20 results </div>
                            <!-- List and Grid Style -->
                            <div class="lst-grd"> <a href="#" id="list"><i class="flaticon-interface"></i></a> <a href="#" id="grid"><i class="icon-grid"></i></a>
                                <!-- Select -->
                                <select>
                                    <option>Short By: New</option>
                                    <option>Short By: high to low</option>
                                    <option>Short By: low to high</option>
                                    <option>Short By: brands</option>
                                    <option>Short By: short</option>
                                </select>
                            </div>
                        </div>

                        <!-- Item -->
                        <div id="products" class="arrival-block col-item-4 list-group">
                            <div class="row">
                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <div class="on-sale"> Sale </div>

                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/1.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <div class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <a href="#." class="wsh-list"><i class="icon-heart"></i> ADD TO WISHLIST</a> </div>
                                                <!-- Details -->
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

                                                <!-- List Style -->
                                                <ul class="list-style">
                                                    <li> Best Shop Products </li>
                                                    <li> Color Option </li>
                                                    <li> All Sizes </li>
                                                    <li> Discounted Prices </li>
                                                    <li> Refund Poloicy </li>
                                                    <li> New Arrival </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/2.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/3.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/4.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/5.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/6.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/7.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/8.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/9.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/11.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/12.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Item -->
                                <div class="item grid-group-item">
                                    <div class="img-ser">
                                        <!-- Images -->
                                        <div class="thumb"> <img class="img-1" src="assets/images/13.jpg" alt="">
                                            <!-- Overlay  -->
                                            <div class="overlay">
                                                <div class="position-center-center"> <a class="popup-with-move-anim" href="#qck-view-shop"><i class="fa fa-eye"></i></a> </div>
                                                <div class="add-crt"><a href="#."><i class="icon-basket margin-right-10"></i> Add To Cart</a></div>
                                            </div>
                                        </div>

                                        <!-- Item Name -->
                                        <div class="item-name fr-grd"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                                        <!-- Item Details -->
                                        <div class="cap-text">
                                            <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>199.00</span>
                                                <!-- Stars -->
                                                <span class="stras"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> </span>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed ullamcorper sapien lacus, eu luctus non. Nulla lacinia, eros vel fermentum consectetur,</p>
                                                <p>Phasellus lacinia fermentum bibendum. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- View All Items -->

                        <!-- Pagination -->
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>

                        <!-- Quick View -->
                        <div id="qck-view-shop" class="zoom-anim-dialog qck-inside mfp-hide">
                            <div class="row">
                                <div class="col-md-6">

                                    <!-- Images Slider -->
                                    <div class="images-slider">
                                        <ul class="slides">
                                            <li data-thumb="images/item-img-1-1.jpg" class="flex-active-slide" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"> <img src="images/item-img-1-1.jpg" alt="" draggable="false"> </li>
                                            <li data-thumb="images/item-img-1-1-1.jpg" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> <img src="images/item-img-1-1-1.jpg" alt="" draggable="false"> </li>
                                            <li data-thumb="images/item-img-1-1.jpg" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> <img src="images/item-img-1-1.jpg" alt="" draggable="false"> </li>
                                        </ul>
                                        <ol class="flex-control-nav flex-control-thumbs"><li><img src="images/item-img-1-1.jpg" class="flex-active" draggable="false"></li><li><img src="images/item-img-1-1-1.jpg" draggable="false"></li><li><img src="images/item-img-1-1.jpg" draggable="false"></li></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul></div>
                                </div>

                                <!-- Content Info -->
                                <div class="col-md-6">
                                    <div class="contnt-info">
                                        <h3>Mid Rise Skinny Jeans</h3>
                                        <p>This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed. <br>
                                            <br>
                                            These words are here to provide the reader with a basic impression of how actual text will appear in its final presentation. </p>

                                        <!-- Btn  -->
                                        <div class="add-info">
                                            <div class="quantity">
                                                <input type="number" min="1" max="100" step="1" value="1" class="form-control qty"><div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fa fa-caret-up"></i></div><div class="quantity-button quantity-down"><i class="fa fa-caret-down"></i></div></div>
                                            </div>
                                            <a href="#." class="btn btn-inverse"><i class="icon-heart"></i></a> <a href="#." class="btn">ADD TO CART </a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection
