@extends('base')
@section('content')
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/crossicon.css" rel="stylesheet">
<style>
    .error-template {padding: 40px 15px;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
</style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-template">
                    <h1>
                        Oops!
                        <div class="swal2-icon swal2-error swal2-animate-error-icon" style="display: flex;"><span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span></div>
                    </h1>
                    <h2>
                        your transaction could not be processed</h2>
                    <div class="error-details">
                        Sorry, an error has occured, Requested page not found!
                        or your system is corrently unavailable
                    </div>
                    <div class="error-actions">
                        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                            Take Me Home </a><a href="#" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @endsection
