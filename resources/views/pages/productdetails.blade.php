@extends('base')
@section('content')
   <link rel="stylesheet" href="css/comments.css">
    <script>
        $(document).ready(function(){


            $("[data-toggle=tooltip]").tooltip();
        });
    </script>
    <section class="padding-top-100 padding-bottom-100">
        <div class="container">

            <!-- SHOP DETAIL -->
            <div class="shop-detail">
                <div class="row">

                    <!-- Popular Images Slider -->
                    <div class="col-md-7">

                        <!-- Images Slider -->
                        <div class="images-slider">
                            <ul class="slides">
                                <li data-thumb="assets/images/6.jpg" class="" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> <img class="img-responsive" src="assets/images/scan.jpg" alt="" draggable="false"> </li>
                                <li data-thumb="assets/images/5.jpg" data-thumb-alt="" class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"> <img class="img-responsive" src="assets/images/scan.jpg" alt="" draggable="false"> </li>
                                <li data-thumb="assets/images/4.jpg" data-thumb-alt="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> <img class="img-responsive" src="assets/images/scan.jpg" alt="" draggable="false"> </li>
                            </ul>
                            <ol class="flex-control-nav flex-control-thumbs"><li><img src="assets/images/5.jpg" class="" draggable="false"></li><li><img src="assets/images/6.jpg" draggable="false" class="flex-active"></li><li><img src="assets/images/4.jpg" draggable="false"></li></ol></div>
                    </div>

                    <!-- COntent -->
                    <div class="col-md-5">
                        <h4>Rise Skinny Jeans</h4>
                        <div class="rating-strs"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></div>
                        <span class="price"><small>$</small>299</span>
                        <ul class="item-owner">
                            <li>Brand:<span> Top Shop</span></li>
                            <li>Category:<span> <a href="#">women</a></span></li>
                        </ul>

                        <!-- Item Detail -->
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text,</p>

                        <!-- Short By -->
                        <div class="some-info">
                            <ul class="row margin-top-30">
                                <li class="col-md-6">

                                    <!-- Quantity -->
                                    <div class="quinty">
                                        <button type="button" class="quantity-left-minus" data-type="minus" data-field=""> <span>-</span> </button>
                                        <input type="number" id="quantity" name="quantity" class="form-control input-number" value="1">
                                        <button type="button" class="quantity-right-plus" data-type="plus" data-field=""> <span>+</span> </button>
                                    </div>
                                </li>

                                <!-- ADD TO CART -->
                                <li class="col-md-6"> <a href="#." class="btn">ADD TO CART</a> </li>

                                <!-- LIKE -->
                                <li class="col-md-6"> <a href="#." class="like-us"><i class="fa fa-heart-o"></i> ADD TO WISHLIST </a> </li>
                            </ul>

                            <!-- INFOMATION -->
                            <div class="inner-info">
                                <h5>Share this item with your friends</h5>
                                <!-- Social Icons -->
                                <ul class="social_icons">
                                    <li><a href="#."><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#."><i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <h1 style="padding:10px 280px;">Description:</h1>
    <p style="padding:40px 280px;text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

    <div class="container" style="padding: 100px;">
        <div class="row">
            <h3>Questions/Comments</h3>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="widget-area no-padding blank">
                    <div class="status-upload">
                        <form>
                            <textarea placeholder="What are you doing right now?" ></textarea>
                            <ul>
                                <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a></li>
                                <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a></li>
                                <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
                                <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
                            </ul>
                            <button type="submit" class="btn btn-success green"><i class="fa fa-share"></i>Post</button>
                        </form>
                    </div><!-- Status Upload  -->
                </div><!-- Widget Area -->
            </div>

        </div>
    </div>
    @include('components.more-product')
    @include('components.brand-product')
    @endsection
