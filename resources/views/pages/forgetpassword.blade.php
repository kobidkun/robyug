@extends('base')
@section('content')

    <div style="padding: 200px 200px 100px 500px;">
        <div class="row">
            <div class="col-md-8 ">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">

                            <form><br>
                                <h3 style="text-align: center;"> <i class="fa fa-lock "></i> Reset password</h3><br>
                                <p style="text-align: center;">Enter your email address we will sent you the password reset link.</p>
                                <div style="text-align: center;">
                                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email address" tabindex="4">
                                </div>
                                <br>
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-12"><a href="#" style="color: green;font-size: 20px;" class="btn btn-success">Reset</a></div>
                                </div>
                                <br>
                            </form>

                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>

    @endsection
