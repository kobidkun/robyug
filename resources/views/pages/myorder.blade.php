@extends('base')
@section('content')
    <link href="css/bootstrap2.min.css" rel="stylesheet" id="bootstrap-css">

    <style>
        .custab{
            border: 1px solid #ccc;
            padding: 5px;
            margin: 5% 0;
            box-shadow: 3px 3px 2px #ccc;
            transition: 0.5s;
        }
        .custab:hover{
            box-shadow: 3px 3px 0px transparent;
            transition: 0.5s;
        }
    </style>
    <section class="sub-bnr" data-stellar-background-ratio="0.5" style="background-position: 0% -70.5px;">
        <div class="position-center-center">
            <div class="container">
                <h4>My Order</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">My Order</li>
                </ol>
            </div>
        </div>
    </section>
    <h5 style="text-align: center;">in case you would like to change your order, please contact any support.</h5>
    <div class="container">
        <div class="row col-md-12 custyle">
            <table class="table table-striped custab">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Order ID</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tr>
                    <td>1.1.19</td>
                    <td><a href="#"> News</a></td>
                    <td> <button type="button" class="btn btn-warning">Ordered</button></td>
                    <td>Rs.200</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class=""></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class=""></span> Del</a></td>
                </tr>
                <tr>
                    <td>2.4.19</td>
                    <td><a href="#">Products</a></td>
                    <td>  <button type="button" class="btn btn-success">Shipped</button>
                    </td>
                    <td>Rs.400</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class=""></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class=""></span> Del</a></td>
                </tr>
                <tr>
                    <td>3.6.19</td>
                    <td><a href="#">Blogs</a></td>
                    <td>  <button type="button" class="btn btn-success">Shipped</button>
                    </td>
                    <td>Rs.600</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class=""></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class=""></span> Del</a></td>
                </tr>
                <tr>
                    <td>4.6.19</td>
                    <td><a href="#">Books</a></td>
                    <td>  <button type="button" class="btn btn-success">Shipped</button>
                    </td>
                    <td>Rs.600</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class=""></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class=""></span> Del</a></td>
                </tr>
                <tr>
                    <td>7.6.19</td>
                    <td><a href="#">Ads</a></td>
                    <td>  <button type="button" class="btn btn-success">Shipped</button>
                    </td>
                    <td>Rs.600</td>
                    <td class="text-center"><a class='btn btn-info btn-xs' href="#"><span class=""></span> Edit</a> <a href="#" class="btn btn-danger btn-xs"><span class=""></span> Del</a></td>
                </tr>
            </table>
        </div>
    </div>
    @endsection
