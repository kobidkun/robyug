@extends('base')
@section('content')

    <section class="chart-page login gray-bg padding-top-100 padding-bottom-100">
        <div class="container">

            <!-- Payments Steps -->
            <div class="shopping-cart">

                <!-- SHOPPING INFORMATION -->
                <div class="cart-ship-info">
                    <div class="row">

                        <!-- Login Register -->
                        <div class="col-sm-7 center-block">

                            <!-- Nav Tabs -->
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" id="login-tab" data-toggle="tab" href="#log" role="tab" aria-selected="true">Login</a> </li>
                                <li class="nav-item"> <a class="nav-link" id="reg-tab" data-toggle="tab" href="#reg" role="tab" aria-selected="false">Register</a> </li>
                            </ul>

                            <!-- Login Register Inside -->
                            <div class="tab-content" id="myTabContent">

                                <!-- Login -->
                                <div class="tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="login-tab">
                                    <form>
                                        <ul class="row">

                                            <!-- Name -->
                                            <li class="col-md-12">
                                                <label> Username Or Email Address
                                                    <input type="text" name="first-name" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>
                                            <!-- LAST NAME -->
                                            <li class="col-md-12">
                                                <label> Password
                                                    <input type="password" name="last-name" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>

                                            <!-- LOGIN -->
                                            <li class="col-md-6">
                                                <button type="submit" class="btn">LOGIN</button>
                                            </li>

                                            <!-- FORGET PASS -->
                                            <li class="col-md-6">
                                                <div class="margin-top-15 text-right"> <a href="{{route('components.forgetpassword')}}">Forget Password</a> </div>
                                            </li>
                                        </ul>
                                    </form>

                                    <!-- Main Heading -->
                                    <div class="heading text-center margin-bottom-50 margin-top-50">
                                        <h4>LOGIN WITH SOCIAL MEDIA</h4>
                                        <hr>
                                    </div>
                                    <ul class="login-with">
                                        <li> <a href="#."><i class="fa fa-facebook"></i>FACEBOOK</a> </li>
                                        <li> <a href="#."><i class="fa fa-google"></i>GOOGLE</a> </li>
                                        <li> <a href="#."><i class="fa fa-twitter"></i>TWITTER</a> </li>
                                    </ul>
                                </div>

                                <!-- Register -->
                                <div class="tab-pane fade" id="reg" role="tabpanel" aria-labelledby="reg-tab">
                                    <form>
                                        <ul class="row">

                                            <!-- Name -->
                                            <li class="col-md-12">
                                                <label> Name *
                                                    <input type="text" name="name" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>
                                            <li class="col-md-12">
                                                <label> Phone *
                                                    <input type="text" name="phone" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>

                                            <li class="col-md-12">
                                                <label> Email Address *
                                                    <input type="text" name="first-name" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>
                                            <li class="col-md-12">
                                                <label> Address *
                                                    <input type="text" name="address" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>
                                            <!-- LAST NAME -->
                                            <li class="col-md-12">
                                                <label> Password *
                                                    <input type="password" name="last-name" value="" placeholder="" class="form-control">
                                                </label>
                                            </li>

                                            <!-- LOGIN -->
                                            <li class="col-md-6">
                                                <button type="submit" class="btn">Register</button>
                                            </li>

                                            <!-- FORGET PASS -->
                                            <li class="col-md-6">
                                                <div class="margin-top-15 text-right"> <a href="#.">Forget Password</a> </div>
                                            </li>
                                        </ul>
                                    </form>

                                    <!-- Main Heading -->
                                    <div class="heading text-center margin-bottom-50 margin-top-50">
                                        <h4>LOGIN WITH SOCIAL MEDIA</h4>
                                        <hr>
                                    </div>
                                    <ul class="login-with">
                                        <li> <a href="#."><i class="fa fa-facebook"></i>FACEBOOK</a> </li>
                                        <li> <a href="#."><i class="fa fa-google"></i>GOOGLE</a> </li>
                                        <li> <a href="#."><i class="fa fa-twitter"></i>TWITTER</a> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('components.subscribe')

    @endsection
