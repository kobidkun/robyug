@extends('base')
@section('content')

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/crossicon.css" rel="stylesheet">
    <style>
        .error-template {padding: 40px 15px;text-align: center;}
        .error-actions {margin-top:15px;margin-bottom:15px;}
        .error-actions .btn { margin-right:10px; }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-template">
                    <h1>
                        <div class="swal2-icon swal2-success swal2-animate-success-icon" style="">
                            <span class="swal2-success-line-tip"></span>
                            <span class="swal2-success-line-long"></span>
                            <div class=""></div>
                            <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
                        </div>
                        Thank you !!
                        <h6>YOUR ORDER WAS SUCCESSFUL.</h6>
                        <HR>

                    </h1>
                    <h2> ORDER ID- 120000201
                    </h2>

                    <h5 style="color: #00b1f1;"> ORDER EMAIL- email@gmail.com
                    </h5>
                    <h5 style="color: grey;"> SHIPPING ADDRESS- Downtown road,
                                           national para,
                                           siliguri, 734006
                    </h5>


                    <div class="error-actions">
                        <a href="#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                             Home </a><a href="#" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact  </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
