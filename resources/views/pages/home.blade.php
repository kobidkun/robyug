@extends('base')
@section('content')

@include('components.slider')

<section class="padding-top-100 padding-bottom-100">
    <div class="container-full">

        <!-- Main Heading -->
        <div class="heading text-center">
            <h4>Best Collection Arrived</h4>
            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus maximus vehicula. </span> </div>

        <!-- New Arrival -->
        <div class="arrival-block">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"> <a class="active" id="Best-tab" data-toggle="tab" href="#bst-sell" role="tab" aria-selected="true">Best Selling</a> </li>
                <li class="nav-item"> <a id="Papular-tab" data-toggle="tab" href="#pap" role="tab" aria-selected="false">Hand Made Items</a> </li>
                <li class="nav-item"> <a id="Top-tab" data-toggle="tab" href="#top-10" role="tab" aria-selected="false">Top 10 Items <span class="top-tgs">Specials</span></a> </li>
            </ul>

            <!-- Tab Content -->
            <div class="tab-content" id="arrival-tab">

                <!-- Best Selling  -->
                <div class="tab-pane fade show active" id="bst-sell" role="tabpanel">

                  {{--  @foreach ($topproducts as $product)


                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">

                            <img class="img-1 lazyload"
                                 src="{{asset($product->img_hd)}}" alt="">
                            <img class="img-2 lazyload"
                                 src="{{asset($product->img_hd)}}" alt="">

                            <div class="overlay">
                                <div class="position-center-center"> <a class=""
                                                                        href="{{route('components.productdetails')}}">
                                        <i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">{{$product->title}}</a>
                            <span class="price"><small>$</small><span class="line-through">299.00</span>
                                <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                        @endforeach--}}


                      <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/5.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Ladies Sandle Clean</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/6.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Lather Bags Inside and outside</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <!-- Sale -->
                              <div class="on-sale"> Sale </div>
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/7.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Neck Skaff Full </a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/8.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Men's Fashion Winter Blue</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">

                              <!-- Sale -->
                              <div class="on-sale"> Sale </div>
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/9.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Angry T-Shirts White</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/1.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Angry T-shites</a> <span class="price"> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/11.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Child Dressing Shorts Jeans</a> <span class="price"> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/12.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">The Best Hand Back Small</a> <span class="price"> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>

                          <!-- Item -->
                          <div class="item">
                              <div class="img-ser">
                                  <!-- Images -->
                                  <img class="img-1 lazyloaded" src="assets/images/13.jpg" alt="">
                                  <!-- Overlay  -->
                                  <div class="overlay">
                                      <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
      </a> </div>
                                      <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                                  </div>
                              </div>
                              <!-- Item Name -->
                              <div class="item-name"> <a href="#." class="i-tittle">Child White Skinny Jeans</a> <span class="price"><small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                          </div>--}}
                </div>

                <!-- Papular Items -->
                <div class="tab-pane animated fadeInDown" id="pap" role="tabpanel">
                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/14.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}">Quick View</a> <a href="#."><i class="icon-magnifier"></i></a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small>299.00</span> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">

                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/1.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/2.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/3.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/4.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>
                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/5.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/6.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/7.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/8.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <!-- Sale -->
                        <div class="on-sale"> Sale </div>
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/9.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>
                </div>

                <!-- Top 10 Items -->
                <div class="tab-pane fade" id="top-10" role="tabpanel">

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/11.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/12.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/13.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/14.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/1.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>
                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/2.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/3.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/4.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/5.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>

                    <!-- Item -->
                    <div class="item">
                        <div class="img-ser">
                            <!-- Images -->
                            <img class="img-1 lazyload" src="assets/images/6.jpg" alt="">
                            <!-- Overlay  -->
                            <div class="overlay">
                                <div class="position-center-center"> <a class="popup-with-move-anim" href="{{route('components.productdetails')}}"><i class="fa fa-eye"></i>
</a> </div>
                                <div class="add-crt"><a href="#."><i class="fa fa-shopping-cart"></i> Add To Cart</a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="#." class="i-tittle">Mid Rise Skinny Jeans</a> <span class="price"><small>$</small><span class="line-through">299.00</span> <small>$</small>199.00</span> <a class="deta animated fadeInRight" href="#.">View Detail</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('components.more-product')

<section class="shipment">
    <div class="container">
        <ul>
            <li><i class="fa fa-truck"></i></i>
                <h4>Free Shipment Over 50$</h4>
            </li>
            <li><i class="fa fa-support"></i>

                <h4>24/7 online Support</h4>
            </li>
            <li><i class="fa fa-credit-card"></i></i>
                <h4>100% Secure Payment </h4>
            </li>
            <li><i class="fa fa-globe"></i>

                <h4>World Wide Shipment</h4>
            </li>
        </ul>
    </div>
</section>


@include('components.subscribe')
@include('components.brand-product')

@endsection
