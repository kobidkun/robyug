@extends('base')
@section('content')


    <section class="sub-bnr" data-stellar-background-ratio="0.5" style="background-position: 0% -70.5px;">
        <div class="position-center-center">
            <div class="container">
                <h4>About BoShop - The Best Collection</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">About</li>
                </ol>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="light-gray-bg">
            <div class="main-page-section half_left_layout">

                <!-- Left Background -->
                <div class="main-half-layout half_left_layout studio-bg"> </div>
                <!-- Right Content -->
                <div class="main-half-layout-container half_left_layout">
                    <div class="about-us-con">
                        <h3>A Brief History of the BoShop</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh dolor, efficitur eget pharetra ac, cursus sed sapien. Cras posuere ligula ut blandit varius. Nunc consectetur scelerisque felis, et volutpat massa aliquam in.<br>
                            <br>
                            Consectetur adipiscing elit. Maecenas nibh dolor, efficitur eget pharetra ac, cursus sed sapien.</p>
                        <h6>1950 <span></span> 1999</h6>
                        <p>Lorem ipsum dolor sit amet, efficitur eget pharetra ac, cursus sed sapien. Cras posuere ligula ut blandit varius. Nunc consectetur scelerisque felis. consectetur adipiscing elit. Maecenas nibh dolor</p>
                        <h6>2000 <span></span> 2018</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nibh dolor, efficitur eget pharetra ac, cursus sed sapien.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="fun-facts padding-top-100 padding-bottom-80">
        <div class="container">

            <!-- HEADING -->
            <div class="heading text-center">
                <h4>Few Facts About BoShop</h4>
                <hr>
            </div>

            <!-- FUN FACTS -->
            <ul class="row">

                <!-- SALES -->
                <li class="col-sm-4"> <span>457</span>
                    <h5>Sales</h5>
                </li>

                <!-- Products -->
                <li class="col-sm-4"> <span>571</span>
                    <h5>Items</h5>
                </li>

                <!-- Clients -->
                <li class="col-sm-4"> <span>289</span>
                    <h5>Clients Worldwide</h5>
                </li>
            </ul>
        </div>
    </section>

    @endsection
