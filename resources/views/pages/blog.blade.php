@extends('base')
@section('content')

    <section class="sub-bnr" data-stellar-background-ratio="0.5" style="background-position: 0% -70.5px;">
        <div class="position-center-center">
            <div class="container">
                <h4>Blog List</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Blog</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="blog-list padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-9">

                    <!-- Article -->
                    <article>
                        <!-- Post Img -->
                        <img class="img-responsive" src="assets/images/1.jpg" alt="">
                        <!-- Tittle -->
                        <div class="post-tittle left"> <a href="#." class="tittle">The unique Chair By ecoshop</a>
                            <!-- Post Info -->
                            <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2018</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Furniture</span> </div>
                        <!-- Post Content -->
                        <div class="text-left">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                <br>
                                Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                            <a href="#." class="btn">READ MORE</a> </div>
                    </article>

                    <!-- Article -->
                    <article>
                        <!-- Post Img -->
                        <img class="img-responsive" src="assets/images/2.jpg" alt="">
                        <!-- Tittle -->
                        <div class="post-tittle left"> <a href="#." class="tittle">Look Beautiful in this Seasons</a>
                            <!-- Post Info -->
                            <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2018</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Fashion</span> </div>
                        <!-- Post Content -->
                        <div class="text-left">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                <br>
                                Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                            <a href="#." class="btn">READ MORE</a> </div>
                    </article>

                    <!-- Article -->
                    <article>
                        <!-- Post Img -->
                        <img class="img-responsive" src="assets/images/3.jpg" alt="">
                        <!-- Tittle -->
                        <div class="post-tittle left"> <a href="#." class="tittle">We Craft An Awesome for your Home</a>
                            <!-- Post Info -->
                            <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2018</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Wood</span> </div>
                        <!-- Post Content -->
                        <div class="text-left">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                <br>
                                Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                            <a href="#." class="btn">READ MORE</a> </div>
                    </article>

                    <!-- Article -->
                    <article>
                        <!-- Post Img -->
                        <img class="img-responsive" src="assets/images/4.jpg" alt="">
                        <!-- Tittle -->
                        <div class="post-tittle left"> <a href="#." class="tittle">The Classic Razor for the Modern Man</a>
                            <!-- Post Info -->
                            <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2018</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Personal</span> </div>
                        <!-- Post Content -->
                        <div class="text-left">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                <br>
                                Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                            <a href="#." class="btn">READ MORE</a> </div>
                    </article>

                    <!-- Pagination -->
                    <ul class="pagination in-center">
                        <a href="#"><i class="fa fa-angle-left"></i></a>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <a href="#"><i class="fa fa-angle-right"></i></a>
                    </ul>
                </div>

                <!-- Sider Bar -->
                <div class="col-md-3">
                    <div class="search">
                        <input class="form-control" type="search" placeholder="Search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </div>
                    <div class="sidebar">
                        <!-- Category -->
                        <h5 class="shop-tittle margin-bottom-20">Category</h5>
                        <ul class="shop-cate">
                            <li><a href="#."> Chair <span>24</span></a></li>
                            <li><a href="#."> Bag <span>122</span></a></li>
                            <li><a href="#."> Soffa <span>09</span></a></li>
                            <li><a href="#."> Bed <span>12</span></a></li>
                            <li><a href="#."> Shoes <span>98</span></a></li>
                            <li><a href="#."> Table <span>34</span></a></li>
                            <li><a href="#."> Bedsheets <span>23</span></a></li>
                            <li><a href="#."> Curtains <span>43</span></a></li>
                            <li><a href="#."> TV Cabinets <span>12</span></a></li>
                            <li><a href="#."> Clocks <span>18</span></a></li>
                            <li><a href="#."> Towels <span>25</span></a></li>
                        </ul>

                        <!-- Recent Post -->

                        <!-- TAGS -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-20">Papular Tags</h5>
                        <ul class="shop-tags">
                            <li><a href="#.">Towels</a></li>
                            <li><a href="#.">Chair</a></li>
                            <li><a href="#.">Bedsheets</a></li>
                            <li><a href="#.">Shoe</a></li>
                            <li><a href="#.">Curtains</a></li>
                            <li><a href="#.">Clocks</a></li>
                            <li><a href="#.">TV Cabinets</a></li>
                            <li><a href="#.">Best Seller</a></li>
                            <li><a href="#.">Top Selling</a></li>
                        </ul>

                        <!-- BRAND -->
                        <h5 class="shop-tittle margin-top-60 margin-bottom-10">archives</h5>
                        <ul class="shop-cate">
                            <li><a href="#."> January 2015 </a></li>
                            <li><a href="#."> February 2015 </a></li>
                            <li><a href="#."> March 2015 </a></li>
                            <li><a href="#."> April 2015 </a></li>
                            <li><a href="#."> May 2015 </a></li>
                        </ul>

                        <!-- SIDE BACR BANER -->
                        <div class="side-bnr margin-top-50"> <img class="img-responsive" src="images/sidebar-bnr.jpg" alt="">
                            <div class="position-center-center"> <span class="price"><small>$</small>299</span>
                                <div class="bnr-text">look
                                    hot
                                    with
                                    style</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@include('components.brand-product')
    @endsection
