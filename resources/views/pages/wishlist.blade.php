@extends('base')
@section('content')
    <link rel="stylesheet" href="css/wishlist.css">
    <div class="shopping-cart" style="padding: 260px 290px 260px 290px;">
        <h1>Wishlist</h1>
        <div class="column-labels">
            <label class="product-image">Image</label>
            <label class="product-details">Product</label>

            <label class="product-price">Price</label>
            <label class="product-quantity">Quantity</label>
            <label class="product-removal">Remove</label>
            <label class="">Available</label>
        </div>

        <div class="product">
            <div class="product-image">
                <img src="images/nike.jpg">
            </div>
            <div class="product-details">
                <div class="product-title">Nike Flex Form TR Women's Sneaker</div>
                <p class="product-description"> It has a lightweight, breathable mesh upper with forefoot cables for a locked-down fit.</p>
            </div>

            <div class="product-price">12.99</div>
            <div class="product-quantity">
                <input type="number" value="2" min="1">
            </div>
            <div class="product-removal">
                <button class="remove-product">
                    Remove
                </button>
            </div>
            <div class="">In Stock</div>
        </div>

        <div class="product">
            <div class="product-image">
                <img src="images/adidas.jpg">
            </div>
            <div class="product-details">
                <div class="product-title">ULTRABOOST UNCAGED SHOES</div>
                <p class="product-description">Born from running culture, these men's shoes deliver the freedom of a cage-free design</p>
            </div>
            <div class="product-price">45.99</div>
            <div class="product-quantity">
                <input type="number" value="1" min="1">
            </div>
            <div class="product-removal">
                <button class="remove-product">
                    Remove
                </button>
            </div>
            <div class="">In Stock</div>
        </div>



    </div>

    @endsection
