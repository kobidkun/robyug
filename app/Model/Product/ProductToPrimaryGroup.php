<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductToPrimaryGroup extends Model
{
    public function products()
    {
        return $this->hasMany('App\Model\Product\Product','id','product_id');
    }
}
