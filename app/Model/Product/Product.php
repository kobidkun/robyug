<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{


    public function product_variants()
    {
        return $this->hasMany('App\Model\Product\Variant\ProductVariant','product_id','id');
    }

    public function product_to_images()
    {
        return $this->hasMany('App\Model\Product\ProductToImage','product_id','id');
    }

    public function product_to_filters()
    {
        return $this->hasMany('App\Model\Product\ProductToFilter','product_id','id');
    }

    public function product_to_reviews()
    {
        return $this->hasMany('App\Model\Product\ProductToReview','product_id','id');
    }

    public function product_to_groups()
    {
        return $this->hasMany('App\Model\Product\ProductToGroups','product_id','id');
    }

    public function product_to_primary_groups()
    {
        return $this->hasMany('App\Model\Product\ProductToPrimaryGroup','product_id','id');
    }

    public function product_to_secondary_groups()
    {
        return $this->hasMany('App\Model\Product\ProductToSecondaryGroup','product_id','id');
    }




}
