<?php

namespace App\Model\Filter;

use Illuminate\Database\Eloquent\Model;

class FilterType extends Model
{
    public function filter_values()
    {
        return $this->hasMany('App\Model\Filter\FilterValue','filter_type_id','id');
    }
}
