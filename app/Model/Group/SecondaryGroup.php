<?php

namespace App\Model\Group;

use Illuminate\Database\Eloquent\Model;

class SecondaryGroup extends Model
{
    public function primary_groups()
    {
        return $this->belongsTo('App\Model\Group\PrimaryGroup','primary_group_id','id');
    }
}
