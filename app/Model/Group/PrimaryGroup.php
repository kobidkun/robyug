<?php

namespace App\Model\Group;

use Illuminate\Database\Eloquent\Model;

class PrimaryGroup extends Model
{
    public function secondary_groups()
    {
        return $this->hasMany('App\Model\Group\SecondaryGroup','primary_group_id','id');
    }
}
