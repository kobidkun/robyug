<?php

namespace App\Http\Controllers\Front;

use App\Model\Group\PrimaryGroup;
use App\Model\Product\Product;
use App\Model\Product\ProductToPrimaryGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagePage extends Controller
{
    public function HomePagetwo(){



        $products = Product::where('active', 1)->get();


        $resentproducts = Product::where('active', 1)->orderBy('id','DESC')->take(3)->get();
        $resentproductstwo = Product::where('active', 1)->orderBy('id','DESC')->take(6)->get();
        $rfeaturedproducts = Product::where('active', 1)->where('featured', 1)->take(3)->get();
        $top = Product::where('active', 1)->orderBy('id','ASC')->take(3)->get();



       // $FeaturedTenProducts = Product::all()->random(12);


      //  return $resentproducts;


        return view('front.pages.common.home')->with([
            'recentproducts' => $resentproducts,
            'rfeaturedproducts' => $rfeaturedproducts,
            'tops' => $top,
            'resentproductstwo' => $resentproductstwo,
        ]);

    }


    public function ProductsbyPrimaryCategory($category){

        $pc = PrimaryGroup::where('slug',$category)->first();

        $allpc = PrimaryGroup::all();


        $b = ProductToPrimaryGroup::where('primary_group_id',$pc->id)->get('product_id');


        $products = Product::find($b)->where('active', 1);

        $FeaturedTenProducts = $products->where('featured', 1)->take(5);



     //  return $products;

        // $FeaturedTenProducts = Product::all()->random(12);

        return view('front.pages.shop.shoppage')->with([
            'products' => $products,
            'featureds' => $FeaturedTenProducts,
            'pg' => $pc,
            'pgs' => $allpc
        ]);

    }


    public function ProductDetails($slug){

        $product = Product::where('slug', $slug)->first();

        return view('front.pages.productdetails.productview')->with(
            [
                'product' => $product
            ]
        );


    }


    public function ContactPage(){

        return view('front.pages.contact');

    }


    public function AboutPage(){

        return view('front.pages.about-us');
    }


    public function ShopPage(){

        return view('front.pages.shop');
    }


    public function FilterPage(){


    }


    public function CartPage(){
        return view('front.pages.cart');

    }





}
