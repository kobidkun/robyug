<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests\Admin\Product\CreateProductRequest;
use App\Model\Filter\FilterType;
use App\Model\Filter\FilterValue;
use App\Model\Group\PrimaryGroup;
use App\Model\Group\SecondaryGroup;
use App\Model\Product\Product;
use App\Model\Product\ProductToFilter;
use App\Model\Product\ProductToGroups;
use App\Model\Product\ProductToImage;
use App\Model\Product\ProductToPrimaryGroup;
use App\Model\Product\ProductToSecondaryGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use QCod\ImageUp\HasImageUploads;
use Yajra\Datatables\Datatables;


class ManageProduct extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }




    public function CreateProductView(){

       $primarygroup = PrimaryGroup::all();

       $secondaryGroup = SecondaryGroup::all();

       $filter = FilterType::all();




       return view('admin.pages.product.create')->with([
           'pgs' => $primarygroup,
           'sgs' => $secondaryGroup,
           'filters' => $filter
       ]);

   }

   public function AllProducts(){



       return view('admin.pages.product.all')->with([

       ]);
   }

   public function ProductDetails($id){

       $pr = Product::findorfail($id);

       return view('admin.pages.product.details.details')->with([
           'prs' => $pr
       ]);
   }

    public function ProductGallery($id){

        $pr = Product::findorfail($id);

        return view('admin.pages.product.details.gallery')->with([
            'prs' => $pr
        ]);
    }

   public function Productedit($id){

       $pr = Product::findorfail($id);
       $primarygroup = PrimaryGroup::all();

       $secondaryGroup = SecondaryGroup::all();

       $filter = FilterType::all();



       return view('admin.pages.product.details.edit')->with([
           'product' => $pr,
           'pgs' => $primarygroup,
           'sgs' => $secondaryGroup,
           'filters' => $filter,
           'prs' => $pr
       ]);
   }

    public function SaveProductupdateedit(Request $request){







        $a = Product::findorfail($request->product_id);

        $a->title = $request->title;
        $a->description = $request->description;

        $a->meta = $request->meta;
        $a->slug = $request->slug.time();
        $a->hsn = $request->hsn;
        $a->primary_group = $request->primary_group;
        $a->primary_group_id = $request->primary_group_id;
        $a->secondary_group = $request->secondary_group;
        $a->secondary_group_id = $request->secondary_group_id;
        $a->code_name = $request->code_name;
        $a->type = $request->type;
        $a->unit = $request->unit;
        $a->value = $request->value;
        $a->discount = $request->discount;
        $a->discount_per = $request->discount_per;
        $a->after_discount_amt = $request->after_discount_amt;
        $a->cgst = $request->cgst;
        $a->sgst = $request->sgst;
        $a->igst = $request->igst;
        $a->cgst_per = ($request->taxper/2);
        $a->sgst_per = ($request->taxper/2);
        $a->igst_per = $request->taxper;
        $a->taxper = $request->taxper;
        $a->tax_amt = $request->tax_amt;
        $a->total = $request->total;
        $a->featured = $request->featured;
        $a->active = $request->active;
        $a->save();

        return back();

    }

    public function SaveProduct(CreateProductRequest $request){


        if ($request->img_hd === null){

            $imglocnamepath = '/common/images/placeholder/placeholder.png';

        } else {
            $imglocname =  time().'-'.$request->img_hd->getClientOriginalName();
            $imglocnamepath = '/storage/images/product/hd/'.$imglocname;

            $imgloc = $request->img_hd->storeAs('public/images/product/hd', $imglocname);
        }


        if ($request->img_small === null){

            $imglocsmnamepath = '/common/images/placeholder/placeholder.png';
        } else {

            $imglocsmname =  time().'-'.$request->img_small->getClientOriginalName();
            $imglocsmnamepath = '/storage/images/product/sm/'.$imglocsmname;
            $imglocsm = $request->img_small->storeAs('public/images/product/sm',$imglocsmname);
        }















        $a = new Product();

       $a->title = $request->title;
       $a->description = $request->description;
       $a->secure_id = $this->generateRandomString().time();
       $a->meta = $request->meta;
       $a->slug = $request->slug;
       $a->hsn = $request->hsn;
       $a->img_hd = $imglocsmnamepath;
       $a->img_small = $imglocnamepath;
       $a->primary_group = $request->primary_group;
       $a->primary_group_id = $request->primary_group_id;
       $a->secondary_group = $request->secondary_group;
       $a->secondary_group_id = $request->secondary_group_id;
       $a->code_name = $request->code_name;
       $a->type = $request->type;
       $a->unit = $request->unit;
       $a->value = $request->value;
       $a->discount = $request->discount;
       $a->discount_per = $request->discount_per;
       $a->after_discount_amt = $request->after_discount_amt;
       $a->cgst = $request->cgst;
       $a->sgst = $request->sgst;
       $a->igst = $request->igst;
       $a->cgst_per = ($request->taxper/2);
       $a->sgst_per = ($request->taxper/2);
       $a->igst_per = $request->taxper;
        $a->taxper = $request->taxper;
        $a->tax_amt = $request->tax_amt;
       $a->total = $request->total;
       $a->featured = $request->featured;
       $a->active = $request->active;
       $a->save();

       return back();

    }


    public function DatatableApi(){



        $users = Product::select([
            'id',
            'title',
            'slug',
            'hsn',
            'primary_group',
            'secondary_group',
            'code_name',
            'value',
            'discount_per',
            'tax_amt',
            'total',
            'featured',
            'active',
        ]);

        //src=".asset('.'/storage/'.$user->sm_img.')"


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class=" btn btn-outline-primary" href="'.route('admin.product.details',$user->id).'"></i> Details</a>';
            })

            ->editColumn('primary_group', function ($user) {
                return '<button class="btn-gradient-info"></i>' .$user->primary_group.'</a>';
            })

            ->addColumn('featuredtwp', function ($user) {

                if ($user->featured === '0'){
                    return '<button class="btn-gradient-light">Not Featured</a>';
                } else if ($user->featured === '1'){

                    return '<button class="btn-gradient-bunting">Featured</a>';
                } else {

                 return   'Not Available';
                }




            })


            ->editColumn('active', function ($user) {

                if ($user->active === 0){
                    return '<button class="btn-gradient-danger">Not Active</a>';
                } else{

                    return '<button class="btn-gradient-success">Active</a>';
                }




            })

            ->editColumn('secondary_group', function ($user) {
                return '<button class="btn-gradient-primary"></i>' .$user->secondary_group.'</a>';
            })
            ->rawColumns(['action', 'secondary_group','primary_group','active','featuredtwp'])

            ->make(true);






    }



    public function UploadGallery(Request $request){
        $imglocname =  time().'-'.$request->image->getClientOriginalName();
        $imglocnamepath = '/images/product/gallery/'.$imglocname;

        $imgloc = $request->image->storeAs('public/images/product/gallery', $imglocname);

        $a = new ProductToImage();

        $a->product_id = $request->product_id;
        $a->alt = $request->product_name;
        $a->path = $imglocnamepath;
        $a->name = $request->product_name;

        $a->save();

        return response()->json('success');
    }


    public function DeleteGallery($id){

        $a = ProductToImage::find($id);

        $a->delete();

        return back();


    }


    public function AddFilter($id){

        $pr = Product::findorfail($id);
        $primarygroup = PrimaryGroup::all();

        $secondaryGroup = SecondaryGroup::all();

        $filter = FilterType::all();

        $a = FilterType::all();

        return view('admin.pages.product.details.filter')->with([
            'filter' => $a,
            'product' => $pr,
            'pgs' => $primarygroup,
            'sgs' => $secondaryGroup,
            'filters' => $filter,
            'prs' => $pr
        ]);
    }

    public function AddFiltersec(Request $request){

        $pr = Product::findorfail($request->product_id);

        $filter = FilterType::find($request->filter);

        $val = FilterValue::where('filter_type_id', $request->filter)->get();

        return view('admin.pages.product.details.filtertwo')->with([
            'filtervalues' => $val,
            'product' => $pr,
            'prs' => $pr,
            'filter' => $filter

        ]);
    }

    public function AddFilterSave(Request $request){


        $filtervalue = FilterValue::find($request->filter);

        $a = new ProductToFilter();
        $a->product_id = $request->product_id;
        $a->filter_type_id = $request->filter_type_id;
        $a->filter_name = $filtervalue->filter_types_title;
        $a->filter_slug = $filtervalue->value;
        $a->save();

        return redirect()->route('admin.product.details',$request->product_id);
    }


    public function CreateFeatureProduct($id){
        $a = Product::find($id);

        $a->featured = 1 ;

        $a->save();

        return back();
    }

    public function CreateFeaturenonProduct($id){
        $a = Product::find($id);

        $a->featured = 0 ;

        $a->save();

        return back();
    }


    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



    // group


    public function AddGroup($id){

        $pr = Product::findorfail($id);
        $primarygroup = PrimaryGroup::all();

        $secondaryGroup = SecondaryGroup::all();

        $filter = FilterType::all();

        $a = FilterType::all();

        return view('admin.pages.product.details.groups')->with([
            'filter' => $a,
            'product' => $pr,
            'pgs' => $primarygroup,
            'sgs' => $secondaryGroup,
            'filters' => $filter,
            'prs' => $pr
        ]);
    }

    public function AddGroupsec(Request $request){


        //dd('kkk');


        $pr = Product::find($request->product_id);



        $secgrp = SecondaryGroup::where('primary_group_id', $request->primary_group_id)->get();



        $prigrp = PrimaryGroup::find($request->primary_group_id);

        return view('admin.pages.product.details.groupstwo')->with([
            'secondary' => $secgrp,
            'primary' => $prigrp,
            'product' => $pr,
            'prs' => $pr

        ]);
    }

    public function AddGroupSave(Request $request){


        $prigrp = PrimaryGroup::find($request->primary_group_id);
        $secgrp = SecondaryGroup::find($request->secondary_group_id);

        $a = new ProductToGroups();
        $a->product_id = $request->product_id;
        $a->primary_group_id = $request->primary_group_id;
        $a->primary_group = $prigrp->title;
        $a->secondary_group_id = $request->secondary_group_id;
        $a->secondary_group =  $secgrp->title;

        $a->save();

        return redirect()->route('admin.product.details',$request->product_id);
    }


    public function DeleteSecondaty($id){
        $a = ProductToGroups::find($id);

        $a->secondary_group_id = null;
        $a->secondary_group = null;

        $a->save();

        return back();
    }

    public function DeletePrimary($id){
        $a = ProductToGroups::find($id);

        $a->delete();

        return back();
    }


    public function CreateGroupProduct($id){
        $a = Product::find($id);

        $a->featured = 1 ;

        $a->save();

        return back();
    }


    public function updateImage($id){

        $pr = Product::findorfail($id);
        $primarygroup = PrimaryGroup::all();

        $secondaryGroup = SecondaryGroup::all();

        $filter = FilterType::all();

        $a = FilterType::all();

        return view('admin.pages.product.details.productimage')->with([
            'filter' => $a,
            'product' => $pr,
            'pgs' => $primarygroup,
            'sgs' => $secondaryGroup,
            'filters' => $filter,
            'prs' => $pr
        ]);
    }

    public function UpdatePrimaryhdimg(Request $request){
        $imglocname =  time().'-'.$request->image->getClientOriginalName();
        $imglocnamepath = '/storage/images/product/hd/'.$imglocname;

        $imgloc = $request->image->storeAs('public/images/product/hd', $imglocname);



        $a = Product::findorfail($request->product_id);

        $a->img_hd = $imglocnamepath;
        $a->save();


        return response()->json('success');




    }


    public function UpdatePrimarysmimg(Request $request){
        $imglocsmname =  time().'-'.$request->image->getClientOriginalName();
        $imglocsmnamepath = '/storage/images/product/sm/'.$imglocsmname;
        $imglocsm = $request->image->storeAs('public/images/product/sm',$imglocsmname);



        $a = Product::findorfail($request->product_id);

        $a->img_small = $imglocsmnamepath;
        $a->save();


        return response()->json('success');




    }


    public function AddPrimaryGroup(Request $request){

        foreach ($request->primary_group_id as $primary){

            $b = PrimaryGroup::find($primary);

            $a = new ProductToPrimaryGroup();
            $a->product_id = $request->product_id;
            $a->primary_group_id = $primary;
            $a->primary_group = $b->title;
            $a->save();

        }




        return back();
    }

    public function DeletePrimaryGroup($id){

        $a = ProductToPrimaryGroup::find($id);
        $a->delete();
        return back();




    }

    public function AddGroupSecondary($id){
        $pr = Product::findorfail($id);
        $primarygroup = PrimaryGroup::all();

        $secondaryGroup = SecondaryGroup::all();

        $filter = FilterType::all();

        $a = FilterType::all();

        return view('admin.pages.product.details.groupstwo')->with([
            'filter' => $a,
            'product' => $pr,
            'pgs' => $primarygroup,
            'sgs' => $secondaryGroup,
            'filters' => $filter,
            'prs' => $pr
        ]);
    }


    public function AddSecondaryGroup(Request $request){

        foreach ($request->secondary_group_id as $primary){

            $b = SecondaryGroup::find($primary);

            $a = new ProductToSecondaryGroup();
            $a->product_id = $request->product_id;
            $a->secondary_group_id = $primary;
            $a->secondary_group = $b->title;
            $a->save();

        }


        return back();
    }

    public function DeleteSecondaryGroup($id){

        $a = ProductToSecondaryGroup::find($id);
        $a->delete();
        return back();




    }




}
