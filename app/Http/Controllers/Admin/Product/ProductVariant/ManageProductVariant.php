<?php

namespace App\Http\Controllers\Admin\Product\ProductVariant;

use App\Model\Product\Product;
use App\Model\Product\Variant\ProductVariant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Filter\FilterType;
use App\Model\Group\PrimaryGroup;
use App\Model\Group\SecondaryGroup;

use Yajra\Datatables\Datatables;

class ManageProductVariant extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }




    public function CreateProductView($id){

        $primarygroup = PrimaryGroup::all();

        $secondaryGroup = SecondaryGroup::all();

        $filter = FilterType::all();

        $product = Product::find($id);




        return view('admin.pages.product.variant.create')->with([
            'pgs' => $primarygroup,
            'sgs' => $secondaryGroup,
            'filters' => $filter,
            'product' => $product
        ]);

    }

    public function AllProducts(){
        return view('admin.pages.product.all');
    }

    public function ProductDetails($id){

        $pr = ProductVariant::findorfail($id);

        return view('admin.pages.product.details.details')->with([
            'prs' => $pr
        ]);
    }

    public function SaveProduct(Request $request){




        if ($request->img_hd === null){

            $imglocnamepath = '/common/images/placeholder/placeholder.png';

        } else {
            $imglocname =  time().'-'.$request->img_hd->getClientOriginalName();
            $imglocnamepath = '/storage/images/product/hd/'.$imglocname;

            $imgloc = $request->img_hd->storeAs('public/images/product/hd', $imglocname);
        }


        if ($request->img_small === null){

            $imglocsmnamepath = '/common/images/placeholder/placeholder.png';
        } else {

            $imglocsmname =  time().'-'.$request->img_small->getClientOriginalName();
            $imglocsmnamepath = '/storage/images/product/sm/'.$imglocsmname;
            $imglocsm = $request->img_small->storeAs('public/images/product/sm',$imglocsmname);
        }






        $a = new ProductVariant();

        $a->title = $request->title;
        $a->description = $request->description;
        $a->product_id = $request->product_id;
        $a->secure_id = $this->generateRandomString().time();
        $a->meta = $request->meta;
        $a->variant_type = $request->variant_type;
        $a->variant_type = $request->variant_type;
        $a->variant_value = $request->variant_value;
        $a->slug = $request->slug.time();
        $a->hsn = $request->hsn;
        $a->img_hd = $imglocsmnamepath;
        $a->img_small = $imglocnamepath;
        $a->primary_group = $request->primary_group;
        $a->primary_group_id = $request->primary_group_id;
        $a->secondary_group = $request->secondary_group;
        $a->secondary_group_id = $request->secondary_group_id;
        $a->code_name = $request->code_name;
        $a->type = $request->type;
        $a->unit = $request->unit;
        $a->value = $request->value;
        $a->discount = $request->discount;
        $a->discount_per = $request->discount_per;
        $a->after_discount_amt = $request->after_discount_amt;
        $a->cgst = $request->cgst;
        $a->sgst = $request->sgst;
        $a->igst = $request->igst;
        $a->cgst_per = ($request->taxper/2);
        $a->sgst_per = ($request->taxper/2);
        $a->igst_per = $request->taxper;
        $a->taxper = $request->taxper;
        $a->tax_amt = $request->tax_amt;
        $a->total = $request->total;
        $a->featured = $request->featured;
        $a->active = $request->active;
        $a->save();

        return back();

    }


    public function DatatableApi(){



        $users = ProductVariant::select([
            'id',
            'title',
            'slug',
            'hsn',
            'primary_group',
            'secondary_group',
            'code_name',
            'value',
            'discount_per',
            'tax_amt',
            'total',
        ]);

        //src=".asset('.'/storage/'.$user->sm_img.')"


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class=" btn btn-outline-primary" href="'.route('admin.product.details',$user->id).'"></i> Details</a>';
            })

            ->editColumn('primary_group', function ($user) {
                return '<button class="btn-gradient-info"></i>' .$user->primary_group.'</a>';
            })

            ->editColumn('secondary_group', function ($user) {
                return '<button class="btn-gradient-success"></i>' .$user->secondary_group.'</a>';
            })
            ->rawColumns(['action', 'secondary_group','primary_group'])

            ->make(true);






    }




    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
