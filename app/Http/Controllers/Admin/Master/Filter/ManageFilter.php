<?php

namespace App\Http\Controllers\Admin\Master\Filter;

use App\Model\Filter\FilterType;
use App\Model\Filter\FilterValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageFilter extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }





    public function FilterView(){
        return view('admin.pages.master.filter.createfilter');
    }

    public function FilterSave(Request $request){

        $a = new FilterType();
        $a->title = $request->title;
        $a->description = $request->description;
        $a->img_url = 'na';
        $a->slug = $request->slug;
        $a->save();

        return redirect()->route('admin.filter.all');

    }

    public function FilterDelete($id){

        $a = FilterType::find($id);

        $a->delete();

        return redirect()->route('admin.group.primary.all');


    }


    public function FilterAll(){

        $a = FilterType::paginate(12);

        return view('admin.pages.master.filter.createfilterall')->with([
            'groups' => $a
        ]);
    }

    public function AddValues($id){


        $fts = FilterType::find($id);

        return view('admin.pages.master.filter.addvalues')->with(['filter' => $fts]);



    }


    public function SaveFilterValues(Request $request){
        $a = new FilterValue();
        $a->filter_type_id = $request->filter_type_id;
        $a->value = $request->value;
        $a->filter_types_title = $request->filter_types_title;
        $a->save();

        return redirect()->route('admin.filter.all');
    }
}
