<?php

namespace App\Http\Controllers\Admin\Master\Group;

use App\Model\Group\PrimaryGroup;
use App\Model\Group\SecondaryGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageGroup extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }





    public function PrimaryGroupView(){
        return view('admin.pages.master.group.primary.create');
    }

    public function PrimaryGroupSave(Request $request){

        $a = new PrimaryGroup();
        $a->title = $request->title;
        $a->description = $request->description;
        $a->img_url = 'na';
        $a->color = 'color';
        $a->color = 'color';
        $a->slug = $request->slug;
        $a->featured = $request->featured;
        $a->save();

        return redirect()->route('admin.group.primary.create.view');

    }

    public function EditPrimary($id){
        $a = PrimaryGroup::find($id);
        return view('admin.pages.master.group.primary.edit')->with([
            'group' => $a
        ]);


    }


    public function UpdatePrimary(Request $request){
        $a = PrimaryGroup::find($request->group_id);
        $a->title = $request->title;
        $a->description = $request->description;
        $a->img_url = 'na';
        $a->color = 'color';
        $a->color = 'color';
        $a->slug = $request->slug;
        $a->featured = $request->featured;
        $a->save();

        return back();





    }


    public function PrimaryGroupDelete($id){

        $a = PrimaryGroup::find($id);




        if ($a->secondary_groups->count() > 0   ){

            return 'You cannot Delete because there are secondary Group Attached to it';
        }


        else {

            $a = PrimaryGroup::find($id);

            $a->delete();

            return redirect()->route('admin.group.primary.all');
        }


    }


    public function PrimaryGroupAll(){

        $a = PrimaryGroup::paginate(12);

        return view('admin.pages.master.group.primary.add')->with([
            'groups' => $a
        ]);
    }



    public function SecondaryGroupView(){


        $pc = PrimaryGroup::all();


        return view('admin.pages.master.group.secondary.create')->with([
            'pcs' => $pc
        ]);
    }

    public function SecondaryGroupSave(Request $request){
        $a = new SecondaryGroup();
        $a->title = $request->title;
        $a->primary_group_id = $request->primary_group_id;
        $a->description = $request->description;
        $a->img_url = 'na';
        $a->color = 'color';
        $a->color = 'color';
        $a->slug = $request->slug;
        $a->featured = $request->featured;
        $a->save();

        return redirect()->route('admin.group.secondary.create.view');

    }

    public function SecondaryGroupDelete($id){


        $a = SecondaryGroup::find($id);

        $a->delete();

        return redirect()->route('admin.group.secondary.all');


    }

    public function SecondaryGroupAll(){

        $a = SecondaryGroup::paginate(12);

        return view('admin.pages.master.group.secondary.all')->with([
            'groups' => $a
        ]);

    }


    public function EditSecondary($id){
        $a = SecondaryGroup::find($id);
        $pc = PrimaryGroup::all();
        return view('admin.pages.master.group.secondary.edit')->with([
            'group' => $a,
            'pcs' => $pc
        ]);


    }


    public function UpdateSecondary(Request $request){
        $a = SecondaryGroup::find($request->group_id);
        $a->title = $request->title;
        $a->primary_group_id = $request->primary_group_id;
        $a->description = $request->description;
        $a->img_url = 'na';
        $a->color = 'color';
        $a->color = 'color';
        $a->slug = $request->slug;
        $a->featured = $request->featured;
        $a->save();

        return back();





    }


    public function GetSecondaryGroup($id){

        $pc = PrimaryGroup::find($id);

        $sg = $pc->secondary_groups;

        return $sg;




    }
}
